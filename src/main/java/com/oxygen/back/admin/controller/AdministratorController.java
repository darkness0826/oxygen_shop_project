package com.oxygen.back.admin.controller;

import com.oxygen.back.admin.model.Administrator;
import com.oxygen.back.admin.service.AdministratorService;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;


@Controller
@RequestMapping("/back/admin")
public class AdministratorController {
    @Resource
    private AdministratorService administratorService;

    /**
     * 管理员登录显示页
     *
     * @return 页面显示
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginForm() {
        System.out.println("后台登录页面");
        return "back/admin/admin_login_form";
    }

    /**
     * 管理员管理显示页
     *
     * @return 页面显示
     */
    @RequestMapping(value = "/adminManagement", method = RequestMethod.GET)
    public String adminManagement(Model model) {
        List<Administrator> administratorList = administratorService.getUsers();
        model.addAttribute("adminList",administratorList); //展示全部管理员
        return "back/admin/admin_management";
    }

}
