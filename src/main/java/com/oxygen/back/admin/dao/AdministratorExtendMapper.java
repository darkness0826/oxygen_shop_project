package com.oxygen.back.admin.dao;

import com.oxygen.back.admin.model.Administrator;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface AdministratorExtendMapper extends AdministratorMapper{
    List<Administrator> selectAllToAdminJoinRole();

}
