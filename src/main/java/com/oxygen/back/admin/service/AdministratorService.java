package com.oxygen.back.admin.service;

import com.oxygen.back.admin.model.Administrator;
import com.oxygen.utils.entity.JsonResponse;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;


public interface AdministratorService {
    /**
     * 获得管理员
     *
     * @return 单个用户实体
     */
    Administrator getUser(Administrator user);

    /**
     *
     * @return 一个符合条件的管理员实体list
     */
    List<Administrator> getUsers();

    /**
     * 登录操作
     *
     * @return 登录状态信息
     */
    JsonResponse<Map<String, Object>> login(Administrator user, HttpSession session);


    /**
     * 添加操作
     *
     * @return 受影响行数
     */
    JsonResponse addAdmin(Administrator administrator);

    /**
     * 修改操作
     *
     * @return 受影响行数
     */
    JsonResponse updateAdmin(Administrator administrator);

    /**
     * 删除操作
     *
     * @return 受影响行数
     */
    JsonResponse deleteAdmin(Administrator administrator);
}
