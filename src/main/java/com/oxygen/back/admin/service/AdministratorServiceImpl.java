package com.oxygen.back.admin.service;

import com.oxygen.back.admin.dao.AdministratorExtendMapper;
import com.oxygen.back.admin.model.Administrator;
import com.oxygen.back.admin.model.AdministratorExample;
import com.oxygen.utils.entity.JsonResponse;
import com.oxygen.utils.entity.SessionKey;
import com.oxygen.utils.uuid.UUIDUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AdministratorServiceImpl implements AdministratorService {
    @Resource
    private AdministratorExtendMapper adminMapper;


    /**
     * 登录操作
     *
     * @param admin 管理员实体
     * @return 管理员
     */
    @Override
    public Administrator getUser(Administrator admin) {
        AdministratorExample administratorExample = new AdministratorExample();
        AdministratorExample.Criteria criteria = administratorExample.createCriteria();
        criteria.andAccountEqualTo(admin.getAccount()).andPasswordEqualTo(admin.getPassword());
        List<Administrator> users = adminMapper.selectByExample(administratorExample);
        if (users != null && users.size() > 0) {
            return users.get(0);
        }
        return null;

    }

    @Override
    public List<Administrator> getUsers() {

        return adminMapper.selectAllToAdminJoinRole();
    }

    /**
     * 登录成功存储session
     *
     * @param admin   管理员实体
     * @param session 会话实体
     * @return 状态信息
     */
    @Override
    public JsonResponse<Map<String, Object>> login(Administrator admin, HttpSession session) {
        Administrator theUser = getUser(admin);
        JsonResponse<Map<String, Object>> jsonResponse = new JsonResponse<>();

        if (theUser != null) {
            session.setAttribute(SessionKey.LOGIN_ADMIN_INFO, theUser);
            jsonResponse.setError(false);
            jsonResponse.setMsg("登录成功");
            Map<String, Object> dataMap = new HashMap<>();
            dataMap.put("user", theUser);
            dataMap.put("markUrl", session.getAttribute("markUrl"));
            jsonResponse.setData(dataMap);

        } else {
            jsonResponse.setMsg("登录失败，失败原因：用户名或者密码错误");
        }
        return jsonResponse;
    }

    /**
     * 添加用户
     *
     * @param administrator 管理员实体
     * @return 状态信息
     */
    @Override
    @Transactional
    public JsonResponse addAdmin(Administrator administrator) {
        JsonResponse jsonResponse = new JsonResponse();
        //非空判断
        if (administrator != null && StringUtils.isNotBlank(administrator.getAccount()) && StringUtils.isNotBlank(administrator.getPassword())) {
            AdministratorExample example = new AdministratorExample();
            AdministratorExample.Criteria criteria = example.createCriteria();
            criteria.andAccountEqualTo(administrator.getAccount());
            //如果已有相同名字的admin，则不进行添加，进行报错返回
            if (adminMapper.selectByExample(example).size() == 0) {
                administrator.setId(UUIDUtil.getUUID());
                administrator.setCreateTime(new Date());
                administrator.setUpdateTime(new Date());
                administrator.setRoleId(administrator.getRoleId());
                int res = adminMapper.insertSelective(administrator);
                if (res == 1) {
                    jsonResponse.setError(false);
                    jsonResponse.setMsg("添加成功！");


                } else {
                    jsonResponse.setMsg("添加失败！");
                }
            } else {
                jsonResponse.setMsg("用户名已存在");
            }

        }
        return jsonResponse;
    }

    /**
     * 修改用户
     *
     * @param administrator 管理员实体
     * @return 状态信息
     */
    @Override
    @Transactional
    public JsonResponse updateAdmin(Administrator administrator) {
        JsonResponse jsonResponse = new JsonResponse();
        administrator.setUpdateTime(new Date());
        int res = adminMapper.updateByPrimaryKeySelective(administrator);
        if (res == 1) {
            jsonResponse.setError(false);
            jsonResponse.setMsg("修改成功！");

        } else {
            jsonResponse.setMsg("修改失败！");
        }
        return jsonResponse;
    }

    /**
     * 删除用户
     *
     * @param administrator 管理员实体
     * @return 状态信息
     */
    @Override
    @Transactional
    public JsonResponse deleteAdmin(Administrator administrator) {
        JsonResponse jsonResponse = new JsonResponse();
        administrator.setUpdateTime(new Date());
        int res = adminMapper.updateByPrimaryKeySelective(administrator);
        if (res == 1) {
            jsonResponse.setError(false);
            jsonResponse.setMsg("删除成功！");

        } else {
            jsonResponse.setMsg("删除失败！");
        }
        return jsonResponse;
    }
}
