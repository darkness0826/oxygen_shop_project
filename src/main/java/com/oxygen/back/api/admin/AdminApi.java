package com.oxygen.back.api.admin;

import com.oxygen.back.admin.model.Administrator;
import com.oxygen.back.admin.service.AdministratorService;
import com.oxygen.utils.entity.JsonResponse;
import com.oxygen.utils.entity.SessionKey;
import com.oxygen.utils.uuid.UUIDUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 后台用户API接口
 */
@Controller
@RequestMapping("/back/admin")
public class AdminApi {
    @Resource
    private AdministratorService adminService;

    /**
     * 管理员登录处理逻辑
     *
     * @param administrator 管理员实体
     * @param session session实体
     * @return JsonResponse 返回信息
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse<Map<String, Object>> login(Administrator administrator, HttpSession session) {
        return adminService.login(administrator, session);
    }


    /**
     * 管理员添加处理逻辑
     *
     * @param administrator Administrator 实体
     * @return 返回处理操作
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse add(Administrator administrator) {
        return adminService.addAdmin(administrator);
    }

    /**
     * 管理员修改处理逻辑
     *
     * @param administrator Administrator 实体
     * @return 返回处理操作
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse update(Administrator administrator) {

        return adminService.updateAdmin(administrator);
    }

    /**
     * 管理员删除处理逻辑
     *
     * @param administrator Administrator 实体
     * @return 返回处理操作
     */
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse delete( Administrator administrator) {
        administrator.setIsDel(1);
        return adminService.deleteAdmin(administrator);
    }


    /**
     * 管理员退出处理逻辑
     *
     * @param httpSession session本体
     * @return 返回信息
     */
    @RequestMapping(value = "/quit", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse quit(HttpSession httpSession) {
        JsonResponse jsonResponse = new JsonResponse();
        if (httpSession.getAttribute(SessionKey.LOGIN_USER_INFO) != null) {
            httpSession.removeAttribute(SessionKey.LOGIN_USER_INFO);
        }
        jsonResponse.setError(false);
        jsonResponse.setMsg("退出成功");
        return jsonResponse;

    }

}
