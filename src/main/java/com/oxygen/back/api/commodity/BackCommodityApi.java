package com.oxygen.back.api.commodity;


import com.github.pagehelper.PageInfo;
import com.oxygen.back.admin.model.Administrator;
import com.oxygen.back.commodity.model.BackCommodity;
import com.oxygen.back.commodity.service.BackCommodityService;
import com.oxygen.utils.entity.Datagrid;
import com.oxygen.utils.entity.JsonResponse;
import com.oxygen.utils.entity.JsonResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * 后台商品管理API接口
 */

@Controller
@RequestMapping("/back/commodity")
public class BackCommodityApi {
    @Autowired
    private BackCommodityService backCommodityService;

    /**
     * 商品查询处理逻辑
     *
     * @param
     * @return 返回处理操作
     */
    @RequestMapping(value = "/selectAll", method = RequestMethod.GET)
    @ResponseBody
    public Datagrid selectCommodity(@RequestParam(value = "pageNumber", defaultValue = "0", required = false) Integer pageNum,
                                    @RequestParam(value = "pageSize", defaultValue = "10", required = false) Integer pageSize) {
        PageInfo<BackCommodity> pageInfo = backCommodityService.selectCommodity(pageNum, pageSize);
        return new Datagrid(pageInfo.getTotal(), pageInfo.getList());
    }


    /**
     * 商品添加处理逻辑
     *
     * @param commodity Commodity 实体
     * @return 返回处理操作
     */
    @RequestMapping(value = "/addCommodity", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse addCommodity(@RequestBody BackCommodity commodity) {

        return backCommodityService.addCommodity(commodity);
    }

    /**
     * 商品更新处理逻辑
     *
     * @param commodity Commodity 实体
     * @return 返回处理操作
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse updateCommodity(@RequestBody BackCommodity commodity) {
        int resultCount = backCommodityService.updateCommodity(commodity);
        if (resultCount > 0) {
            return JsonResponseUtil.success();
        }
        return JsonResponseUtil.error();
    }


    /**
     * 商品删除处理逻辑
     *
     * @param
     * @return 返回处理操作
     */
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse delCommodity(BackCommodity commodity) {
        return backCommodityService.delCommodity(commodity);
    }

    /**
     * 商品下架处理逻辑
     *
     * @param
     * @return 返回处理操作
     */
    @RequestMapping(value = "/undercarriage", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse undercarriageCommodity(BackCommodity commodity) {
        return backCommodityService.undercarriageCommodity(commodity);
    }

    /**
     * 商品上架处理逻辑
     *
     * @param
     * @return 返回处理操作
     */
    @RequestMapping(value = "/grounding", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse groundingCommodity(BackCommodity commodity) {
        return backCommodityService.groundingCommodity(commodity);
    }
}
