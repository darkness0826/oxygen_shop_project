package com.oxygen.back.api.jurisdiction;

import com.oxygen.back.jurisdiction.model.Jurisdiction;
import com.oxygen.back.jurisdiction.service.JurisdictionService;
import com.oxygen.utils.entity.JsonResponse;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/back/jurisdiction")
public class JurisdictionApi {
    @Resource
    private JurisdictionService jurisdictionService;

    /**
     * 查询全部
     *
     * @return 结果集
     */
    @GetMapping("/selectAll")
    public JsonResponse<List<Jurisdiction>> selectAll() {
        JsonResponse<List<Jurisdiction>> jsonResponse = new JsonResponse<>();
        jsonResponse.setError(false);
        jsonResponse.setMsg("查询成功");
        jsonResponse.setData(jurisdictionService.selectAll());
        return jsonResponse;
    }

    /**
     * 添加权限操作
     * @param jurisdiction 条件实体
     * @return 返回成功状态信息
     */
    @PostMapping("/add")
    public JsonResponse add(Jurisdiction jurisdiction) {
        JsonResponse jsonResponse = new JsonResponse();
        int count = jurisdictionService.add(jurisdiction);
        if (count == 1) {
            jsonResponse.setError(false);
            jsonResponse.setMsg("插入成功");
        } else {
            jsonResponse.setMsg("插入失败");
        }
        return jsonResponse;
    }

    /**
     * 更新权限操作
     *
     * @param jurisdiction 条件实体
     * @return 返回成功状态信息
     */
    @PostMapping("/update")
    public JsonResponse update(Jurisdiction jurisdiction) {
        JsonResponse jsonResponse = new JsonResponse();
        int count = jurisdictionService.update(jurisdiction);
        if (count == 1) {
            jsonResponse.setError(false);
            jsonResponse.setMsg("更新成功");
        } else {
            jsonResponse.setMsg("更新失败");
        }
        return jsonResponse;
    }

    /**
     * 删除权限操作
     *
     * @param jurisdiction 条件实体
     * @return 返回成功状态信息
     */
    @PostMapping("/delete")
    public JsonResponse delete(Jurisdiction jurisdiction) {
        JsonResponse jsonResponse = new JsonResponse();
        int count = jurisdictionService.delete(jurisdiction);
        if (count == 1) {
            jsonResponse.setError(false);
            jsonResponse.setMsg("删除成功");
        } else {
            jsonResponse.setMsg("删除失败");
        }
        return jsonResponse;
    }
}
