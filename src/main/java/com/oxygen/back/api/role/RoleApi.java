package com.oxygen.back.api.role;

import com.oxygen.back.role.model.Role;
import com.oxygen.back.role.service.RoleService;
import com.oxygen.utils.entity.JsonResponse;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping(value = "/back/role")
public class RoleApi {
    @Resource
    private RoleService roleService;

    /**
     * 搜索角色列表
     *
     * @return 角色列表
     */
    @GetMapping("/selectAll")
    public JsonResponse<List<Role>> selectAll() {
        JsonResponse<List<Role>> jsonResponse = new JsonResponse<>();
        List<Role> roleList = roleService.selectRoleWithJurisdiction();
        jsonResponse.setError(false);
        jsonResponse.setMsg("查询成功");
        jsonResponse.setData(roleList);
        return jsonResponse;
    }

    /**
     * 添加角色操作
     * @param role 条件实体
     * @return 返回成功状态信息
     */
    @PostMapping("/add")
    public JsonResponse add(@RequestBody Role role) {
        JsonResponse jsonResponse = new JsonResponse();
        int count = roleService.add(role);
        if (count > 0) {
            jsonResponse.setError(false);
            jsonResponse.setMsg("插入成功");
        } else {
            jsonResponse.setMsg("插入失败");
        }
        return jsonResponse;
    }

    /**
     * 更新角色操作
     * @param role 条件实体
     * @return 返回成功状态信息
     */
    @PostMapping("/update")
    public JsonResponse update(@RequestBody Role role) {
        JsonResponse jsonResponse = new JsonResponse();
        int count = roleService.update(role);
        if (count > 0) {
            jsonResponse.setError(false);
            jsonResponse.setMsg("更新成功");
        } else {
            jsonResponse.setMsg("更新失败");
        }
        return jsonResponse;
    }

    /**
     * 删除角色操作
     *
     * @param role role 条件实体
     * @return 返回成功状态信息
     */
    @PostMapping("/delete")
    public JsonResponse delete(Role role) {
        JsonResponse jsonResponse = new JsonResponse();
        int count = roleService.delete(role);
        if (count == 1) {
            jsonResponse.setError(false);
            jsonResponse.setMsg("删除成功");
        } else {
            jsonResponse.setMsg("删除失败");
        }
        return jsonResponse;
    }

}
