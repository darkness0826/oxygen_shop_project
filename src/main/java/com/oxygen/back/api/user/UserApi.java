package com.oxygen.back.api.user;


import com.github.pagehelper.PageInfo;
import com.oxygen.back.user.model.BackUser;
import com.oxygen.back.user.service.BackUserService;
import com.oxygen.utils.entity.Datagrid;
import com.oxygen.utils.entity.JsonResponse;
import com.oxygen.utils.entity.PageBean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller("backUserApi")
@RequestMapping("/back/user")
public class UserApi {
    @Resource
    private BackUserService backUserService;

    /**
     * 查询全部的用户
     * @return 用户进行了分页处理的列表
     */
    @RequestMapping(value = "/selectAll",method = RequestMethod.GET)
    @ResponseBody
    public Datagrid selectAll(
            @RequestParam(value="pageNumber",defaultValue="0",required=false)Integer pageNum,
                                                  @RequestParam(value="pageSize",defaultValue="10",required=false)Integer pageSize) {
        PageInfo<BackUser> pageInfo = backUserService.getUsersLimited(pageNum,pageSize);
        Datagrid datagrid = new Datagrid(pageInfo.getTotal(),pageInfo.getList());
        return datagrid;

    }


    /**
     * 用户修改处理逻辑
     *
     * @param backUser 用户实体
     * @return 返回处理操作
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse update(BackUser backUser) {

        return backUserService.updateUser(backUser);
    }

    /**
     * 用户删除处理逻辑
     *
     * @param backUser 用户实体
     * @return 返回处理操作
     */
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse delete( BackUser backUser) {
        backUser.setIsDel(1);
        return backUserService.delUser(backUser);
    }




}
