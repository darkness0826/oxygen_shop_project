package com.oxygen.back.commodity.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 商品控制器
 */
@Controller
@RequestMapping("/back/commodity")
public class BackCommodityController {
    /**
     * 后台功能商品管理
     *
     * @return 页面显示
     */
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String commodity() {
        return "back/commodity/index";
    }
}
