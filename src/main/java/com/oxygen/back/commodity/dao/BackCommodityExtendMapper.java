package com.oxygen.back.commodity.dao;

import com.oxygen.back.commodity.model.BackCommodity;

import java.util.List;

public interface BackCommodityExtendMapper extends BackCommodityMapper {
    /**
     * 查询全部商品（附带类型）
     *
     * @return 商品集合
     */
    List<BackCommodity> selectAll();
}
