package com.oxygen.back.commodity.dao;

import com.oxygen.back.commodity.model.BackCommodity;
import com.oxygen.back.commodity.model.BackCommodityExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface BackCommodityMapper {
    long countByExample(BackCommodityExample example);

    int deleteByExample(BackCommodityExample example);

    int deleteByPrimaryKey(String id);

    int insert(BackCommodity record);

    int insertSelective(BackCommodity record);

    List<BackCommodity> selectByExample(BackCommodityExample example);

    BackCommodity selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") BackCommodity record, @Param("example") BackCommodityExample example);

    int updateByExample(@Param("record") BackCommodity record, @Param("example") BackCommodityExample example);

    int updateByPrimaryKeySelective(BackCommodity record);

    int updateByPrimaryKey(BackCommodity record);
}