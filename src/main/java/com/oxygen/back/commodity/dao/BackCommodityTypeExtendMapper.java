package com.oxygen.back.commodity.dao;

import com.oxygen.back.commodity.model.BackCommodity;
import com.oxygen.back.commodity.model.BackCommodityType;

import java.util.List;

public interface BackCommodityTypeExtendMapper extends BackCommodityTypeMapper {
    /**
     * 批量添加
     *
     * @param params 多个条件参数体
     * @return 受影响行
     */
    int batchAdd(List<BackCommodityType> params);

    /**
     * 批量更新
     *
     * @param params 多个条件参数体
     * @return 受影响行
     */
    int batchUpdate(List<BackCommodityType> params);

    /**
     * 批量删除
     *
     * @return 受影响行
     */
    int batchDelete(List<BackCommodityType> params);
}
