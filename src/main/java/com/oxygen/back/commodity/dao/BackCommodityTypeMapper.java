package com.oxygen.back.commodity.dao;

import com.oxygen.back.commodity.model.BackCommodityType;
import com.oxygen.back.commodity.model.BackCommodityTypeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface BackCommodityTypeMapper {
    long countByExample(BackCommodityTypeExample example);

    int deleteByExample(BackCommodityTypeExample example);

    int deleteByPrimaryKey(String id);

    int insert(BackCommodityType record);

    int insertSelective(BackCommodityType record);

    List<BackCommodityType> selectByExample(BackCommodityTypeExample example);

    BackCommodityType selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") BackCommodityType record, @Param("example") BackCommodityTypeExample example);

    int updateByExample(@Param("record") BackCommodityType record, @Param("example") BackCommodityTypeExample example);

    int updateByPrimaryKeySelective(BackCommodityType record);

    int updateByPrimaryKey(BackCommodityType record);
}