package com.oxygen.back.commodity.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BackCommodity {
    private String id;

    private String name;

    private String frontCommodityThumbnail;

    private BigDecimal price;
    @JsonFormat(pattern = "yyyy年MM月dd日 HH时mm分ss秒", locale = "zh", timezone = "GMT+8")
    private Date putawayTime;
    @JsonFormat(pattern = "yyyy年MM月dd日 HH时mm分ss秒", locale = "zh", timezone = "GMT+8")
    private Date lastAwayTime;

    private Integer state;

    private Integer isDel;

    private Integer commoditySalesNum;
    @JsonFormat(pattern = "yyyy年MM月dd日 HH时mm分ss秒", locale = "zh", timezone = "GMT+8")
    private Date createTime;
    @JsonFormat(pattern = "yyyy年MM月dd日 HH时mm分ss秒", locale = "zh", timezone = "GMT+8")
    private Date updateTime;

    private List<BackCommodityType> backCommodityTypeList = new ArrayList<>();

    public List<BackCommodityType> getBackCommodityTypeList() {
        return backCommodityTypeList;
    }

    public void setBackCommodityTypeList(List<BackCommodityType> backCommodityTypeList) {
        this.backCommodityTypeList = backCommodityTypeList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getFrontCommodityThumbnail() {
        return frontCommodityThumbnail;
    }

    public void setFrontCommodityThumbnail(String frontCommodityThumbnail) {
        this.frontCommodityThumbnail = frontCommodityThumbnail == null ? null : frontCommodityThumbnail.trim();
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Date getPutawayTime() {
        return putawayTime;
    }

    public void setPutawayTime(Date putawayTime) {
        this.putawayTime = putawayTime;
    }

    public Date getLastAwayTime() {
        return lastAwayTime;
    }

    public void setLastAwayTime(Date lastAwayTime) {
        this.lastAwayTime = lastAwayTime;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getIsDel() {
        return isDel;
    }

    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }

    public Integer getCommoditySalesNum() {
        return commoditySalesNum;
    }

    public void setCommoditySalesNum(Integer commoditySalesNum) {
        this.commoditySalesNum = commoditySalesNum;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}