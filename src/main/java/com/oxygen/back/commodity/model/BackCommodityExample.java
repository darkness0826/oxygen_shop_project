package com.oxygen.back.commodity.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BackCommodityExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public BackCommodityExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityThumbnailIsNull() {
            addCriterion("front_commodity_thumbnail is null");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityThumbnailIsNotNull() {
            addCriterion("front_commodity_thumbnail is not null");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityThumbnailEqualTo(String value) {
            addCriterion("front_commodity_thumbnail =", value, "frontCommodityThumbnail");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityThumbnailNotEqualTo(String value) {
            addCriterion("front_commodity_thumbnail <>", value, "frontCommodityThumbnail");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityThumbnailGreaterThan(String value) {
            addCriterion("front_commodity_thumbnail >", value, "frontCommodityThumbnail");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityThumbnailGreaterThanOrEqualTo(String value) {
            addCriterion("front_commodity_thumbnail >=", value, "frontCommodityThumbnail");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityThumbnailLessThan(String value) {
            addCriterion("front_commodity_thumbnail <", value, "frontCommodityThumbnail");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityThumbnailLessThanOrEqualTo(String value) {
            addCriterion("front_commodity_thumbnail <=", value, "frontCommodityThumbnail");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityThumbnailLike(String value) {
            addCriterion("front_commodity_thumbnail like", value, "frontCommodityThumbnail");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityThumbnailNotLike(String value) {
            addCriterion("front_commodity_thumbnail not like", value, "frontCommodityThumbnail");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityThumbnailIn(List<String> values) {
            addCriterion("front_commodity_thumbnail in", values, "frontCommodityThumbnail");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityThumbnailNotIn(List<String> values) {
            addCriterion("front_commodity_thumbnail not in", values, "frontCommodityThumbnail");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityThumbnailBetween(String value1, String value2) {
            addCriterion("front_commodity_thumbnail between", value1, value2, "frontCommodityThumbnail");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityThumbnailNotBetween(String value1, String value2) {
            addCriterion("front_commodity_thumbnail not between", value1, value2, "frontCommodityThumbnail");
            return (Criteria) this;
        }

        public Criteria andPriceIsNull() {
            addCriterion("price is null");
            return (Criteria) this;
        }

        public Criteria andPriceIsNotNull() {
            addCriterion("price is not null");
            return (Criteria) this;
        }

        public Criteria andPriceEqualTo(BigDecimal value) {
            addCriterion("price =", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotEqualTo(BigDecimal value) {
            addCriterion("price <>", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceGreaterThan(BigDecimal value) {
            addCriterion("price >", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("price >=", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceLessThan(BigDecimal value) {
            addCriterion("price <", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("price <=", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceIn(List<BigDecimal> values) {
            addCriterion("price in", values, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotIn(List<BigDecimal> values) {
            addCriterion("price not in", values, "price");
            return (Criteria) this;
        }

        public Criteria andPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("price between", value1, value2, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("price not between", value1, value2, "price");
            return (Criteria) this;
        }

        public Criteria andPutawayTimeIsNull() {
            addCriterion("putaway_time is null");
            return (Criteria) this;
        }

        public Criteria andPutawayTimeIsNotNull() {
            addCriterion("putaway_time is not null");
            return (Criteria) this;
        }

        public Criteria andPutawayTimeEqualTo(Date value) {
            addCriterion("putaway_time =", value, "putawayTime");
            return (Criteria) this;
        }

        public Criteria andPutawayTimeNotEqualTo(Date value) {
            addCriterion("putaway_time <>", value, "putawayTime");
            return (Criteria) this;
        }

        public Criteria andPutawayTimeGreaterThan(Date value) {
            addCriterion("putaway_time >", value, "putawayTime");
            return (Criteria) this;
        }

        public Criteria andPutawayTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("putaway_time >=", value, "putawayTime");
            return (Criteria) this;
        }

        public Criteria andPutawayTimeLessThan(Date value) {
            addCriterion("putaway_time <", value, "putawayTime");
            return (Criteria) this;
        }

        public Criteria andPutawayTimeLessThanOrEqualTo(Date value) {
            addCriterion("putaway_time <=", value, "putawayTime");
            return (Criteria) this;
        }

        public Criteria andPutawayTimeIn(List<Date> values) {
            addCriterion("putaway_time in", values, "putawayTime");
            return (Criteria) this;
        }

        public Criteria andPutawayTimeNotIn(List<Date> values) {
            addCriterion("putaway_time not in", values, "putawayTime");
            return (Criteria) this;
        }

        public Criteria andPutawayTimeBetween(Date value1, Date value2) {
            addCriterion("putaway_time between", value1, value2, "putawayTime");
            return (Criteria) this;
        }

        public Criteria andPutawayTimeNotBetween(Date value1, Date value2) {
            addCriterion("putaway_time not between", value1, value2, "putawayTime");
            return (Criteria) this;
        }

        public Criteria andLastAwayTimeIsNull() {
            addCriterion("last_away_time is null");
            return (Criteria) this;
        }

        public Criteria andLastAwayTimeIsNotNull() {
            addCriterion("last_away_time is not null");
            return (Criteria) this;
        }

        public Criteria andLastAwayTimeEqualTo(Date value) {
            addCriterion("last_away_time =", value, "lastAwayTime");
            return (Criteria) this;
        }

        public Criteria andLastAwayTimeNotEqualTo(Date value) {
            addCriterion("last_away_time <>", value, "lastAwayTime");
            return (Criteria) this;
        }

        public Criteria andLastAwayTimeGreaterThan(Date value) {
            addCriterion("last_away_time >", value, "lastAwayTime");
            return (Criteria) this;
        }

        public Criteria andLastAwayTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("last_away_time >=", value, "lastAwayTime");
            return (Criteria) this;
        }

        public Criteria andLastAwayTimeLessThan(Date value) {
            addCriterion("last_away_time <", value, "lastAwayTime");
            return (Criteria) this;
        }

        public Criteria andLastAwayTimeLessThanOrEqualTo(Date value) {
            addCriterion("last_away_time <=", value, "lastAwayTime");
            return (Criteria) this;
        }

        public Criteria andLastAwayTimeIn(List<Date> values) {
            addCriterion("last_away_time in", values, "lastAwayTime");
            return (Criteria) this;
        }

        public Criteria andLastAwayTimeNotIn(List<Date> values) {
            addCriterion("last_away_time not in", values, "lastAwayTime");
            return (Criteria) this;
        }

        public Criteria andLastAwayTimeBetween(Date value1, Date value2) {
            addCriterion("last_away_time between", value1, value2, "lastAwayTime");
            return (Criteria) this;
        }

        public Criteria andLastAwayTimeNotBetween(Date value1, Date value2) {
            addCriterion("last_away_time not between", value1, value2, "lastAwayTime");
            return (Criteria) this;
        }

        public Criteria andStateIsNull() {
            addCriterion("state is null");
            return (Criteria) this;
        }

        public Criteria andStateIsNotNull() {
            addCriterion("state is not null");
            return (Criteria) this;
        }

        public Criteria andStateEqualTo(Integer value) {
            addCriterion("state =", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotEqualTo(Integer value) {
            addCriterion("state <>", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThan(Integer value) {
            addCriterion("state >", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThanOrEqualTo(Integer value) {
            addCriterion("state >=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThan(Integer value) {
            addCriterion("state <", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThanOrEqualTo(Integer value) {
            addCriterion("state <=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateIn(List<Integer> values) {
            addCriterion("state in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotIn(List<Integer> values) {
            addCriterion("state not in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateBetween(Integer value1, Integer value2) {
            addCriterion("state between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotBetween(Integer value1, Integer value2) {
            addCriterion("state not between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andIsDelIsNull() {
            addCriterion("is_del is null");
            return (Criteria) this;
        }

        public Criteria andIsDelIsNotNull() {
            addCriterion("is_del is not null");
            return (Criteria) this;
        }

        public Criteria andIsDelEqualTo(Integer value) {
            addCriterion("is_del =", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelNotEqualTo(Integer value) {
            addCriterion("is_del <>", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelGreaterThan(Integer value) {
            addCriterion("is_del >", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_del >=", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelLessThan(Integer value) {
            addCriterion("is_del <", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelLessThanOrEqualTo(Integer value) {
            addCriterion("is_del <=", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelIn(List<Integer> values) {
            addCriterion("is_del in", values, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelNotIn(List<Integer> values) {
            addCriterion("is_del not in", values, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelBetween(Integer value1, Integer value2) {
            addCriterion("is_del between", value1, value2, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelNotBetween(Integer value1, Integer value2) {
            addCriterion("is_del not between", value1, value2, "isDel");
            return (Criteria) this;
        }

        public Criteria andCommoditySalesNumIsNull() {
            addCriterion("commodity_sales_num is null");
            return (Criteria) this;
        }

        public Criteria andCommoditySalesNumIsNotNull() {
            addCriterion("commodity_sales_num is not null");
            return (Criteria) this;
        }

        public Criteria andCommoditySalesNumEqualTo(Integer value) {
            addCriterion("commodity_sales_num =", value, "commoditySalesNum");
            return (Criteria) this;
        }

        public Criteria andCommoditySalesNumNotEqualTo(Integer value) {
            addCriterion("commodity_sales_num <>", value, "commoditySalesNum");
            return (Criteria) this;
        }

        public Criteria andCommoditySalesNumGreaterThan(Integer value) {
            addCriterion("commodity_sales_num >", value, "commoditySalesNum");
            return (Criteria) this;
        }

        public Criteria andCommoditySalesNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("commodity_sales_num >=", value, "commoditySalesNum");
            return (Criteria) this;
        }

        public Criteria andCommoditySalesNumLessThan(Integer value) {
            addCriterion("commodity_sales_num <", value, "commoditySalesNum");
            return (Criteria) this;
        }

        public Criteria andCommoditySalesNumLessThanOrEqualTo(Integer value) {
            addCriterion("commodity_sales_num <=", value, "commoditySalesNum");
            return (Criteria) this;
        }

        public Criteria andCommoditySalesNumIn(List<Integer> values) {
            addCriterion("commodity_sales_num in", values, "commoditySalesNum");
            return (Criteria) this;
        }

        public Criteria andCommoditySalesNumNotIn(List<Integer> values) {
            addCriterion("commodity_sales_num not in", values, "commoditySalesNum");
            return (Criteria) this;
        }

        public Criteria andCommoditySalesNumBetween(Integer value1, Integer value2) {
            addCriterion("commodity_sales_num between", value1, value2, "commoditySalesNum");
            return (Criteria) this;
        }

        public Criteria andCommoditySalesNumNotBetween(Integer value1, Integer value2) {
            addCriterion("commodity_sales_num not between", value1, value2, "commoditySalesNum");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}