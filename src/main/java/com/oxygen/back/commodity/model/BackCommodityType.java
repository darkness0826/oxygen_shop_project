package com.oxygen.back.commodity.model;

import java.util.Objects;

public class BackCommodityType {
    private String id;

    private String frontCommodityId;

    private String frontCommodityType;

    private String frontCommodityTypeParameter;

    private Integer isDel;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getFrontCommodityId() {
        return frontCommodityId;
    }

    public void setFrontCommodityId(String frontCommodityId) {
        this.frontCommodityId = frontCommodityId == null ? null : frontCommodityId.trim();
    }

    public String getFrontCommodityType() {
        return frontCommodityType;
    }

    public void setFrontCommodityType(String frontCommodityType) {
        this.frontCommodityType = frontCommodityType == null ? null : frontCommodityType.trim();
    }

    public String getFrontCommodityTypeParameter() {
        return frontCommodityTypeParameter;
    }

    public void setFrontCommodityTypeParameter(String frontCommodityTypeParameter) {
        this.frontCommodityTypeParameter = frontCommodityTypeParameter == null ? null : frontCommodityTypeParameter.trim();
    }

    public Integer getIsDel() {
        return isDel;
    }

    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BackCommodityType)) return false;
        BackCommodityType that = (BackCommodityType) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getFrontCommodityType(), that.getFrontCommodityType()) &&
                Objects.equals(getFrontCommodityTypeParameter(), that.getFrontCommodityTypeParameter());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId(), getFrontCommodityId(), getFrontCommodityType(), getFrontCommodityTypeParameter());
    }
}