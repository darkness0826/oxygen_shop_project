package com.oxygen.back.commodity.model;

import java.util.ArrayList;
import java.util.List;

public class BackCommodityTypeExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public BackCommodityTypeExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityIdIsNull() {
            addCriterion("front_commodity_id is null");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityIdIsNotNull() {
            addCriterion("front_commodity_id is not null");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityIdEqualTo(String value) {
            addCriterion("front_commodity_id =", value, "frontCommodityId");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityIdNotEqualTo(String value) {
            addCriterion("front_commodity_id <>", value, "frontCommodityId");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityIdGreaterThan(String value) {
            addCriterion("front_commodity_id >", value, "frontCommodityId");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityIdGreaterThanOrEqualTo(String value) {
            addCriterion("front_commodity_id >=", value, "frontCommodityId");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityIdLessThan(String value) {
            addCriterion("front_commodity_id <", value, "frontCommodityId");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityIdLessThanOrEqualTo(String value) {
            addCriterion("front_commodity_id <=", value, "frontCommodityId");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityIdLike(String value) {
            addCriterion("front_commodity_id like", value, "frontCommodityId");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityIdNotLike(String value) {
            addCriterion("front_commodity_id not like", value, "frontCommodityId");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityIdIn(List<String> values) {
            addCriterion("front_commodity_id in", values, "frontCommodityId");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityIdNotIn(List<String> values) {
            addCriterion("front_commodity_id not in", values, "frontCommodityId");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityIdBetween(String value1, String value2) {
            addCriterion("front_commodity_id between", value1, value2, "frontCommodityId");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityIdNotBetween(String value1, String value2) {
            addCriterion("front_commodity_id not between", value1, value2, "frontCommodityId");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityTypeIsNull() {
            addCriterion("front_commodity_type is null");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityTypeIsNotNull() {
            addCriterion("front_commodity_type is not null");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityTypeEqualTo(String value) {
            addCriterion("front_commodity_type =", value, "frontCommodityType");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityTypeNotEqualTo(String value) {
            addCriterion("front_commodity_type <>", value, "frontCommodityType");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityTypeGreaterThan(String value) {
            addCriterion("front_commodity_type >", value, "frontCommodityType");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityTypeGreaterThanOrEqualTo(String value) {
            addCriterion("front_commodity_type >=", value, "frontCommodityType");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityTypeLessThan(String value) {
            addCriterion("front_commodity_type <", value, "frontCommodityType");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityTypeLessThanOrEqualTo(String value) {
            addCriterion("front_commodity_type <=", value, "frontCommodityType");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityTypeLike(String value) {
            addCriterion("front_commodity_type like", value, "frontCommodityType");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityTypeNotLike(String value) {
            addCriterion("front_commodity_type not like", value, "frontCommodityType");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityTypeIn(List<String> values) {
            addCriterion("front_commodity_type in", values, "frontCommodityType");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityTypeNotIn(List<String> values) {
            addCriterion("front_commodity_type not in", values, "frontCommodityType");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityTypeBetween(String value1, String value2) {
            addCriterion("front_commodity_type between", value1, value2, "frontCommodityType");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityTypeNotBetween(String value1, String value2) {
            addCriterion("front_commodity_type not between", value1, value2, "frontCommodityType");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityTypeParameterIsNull() {
            addCriterion("front_commodity_type_parameter is null");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityTypeParameterIsNotNull() {
            addCriterion("front_commodity_type_parameter is not null");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityTypeParameterEqualTo(String value) {
            addCriterion("front_commodity_type_parameter =", value, "frontCommodityTypeParameter");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityTypeParameterNotEqualTo(String value) {
            addCriterion("front_commodity_type_parameter <>", value, "frontCommodityTypeParameter");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityTypeParameterGreaterThan(String value) {
            addCriterion("front_commodity_type_parameter >", value, "frontCommodityTypeParameter");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityTypeParameterGreaterThanOrEqualTo(String value) {
            addCriterion("front_commodity_type_parameter >=", value, "frontCommodityTypeParameter");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityTypeParameterLessThan(String value) {
            addCriterion("front_commodity_type_parameter <", value, "frontCommodityTypeParameter");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityTypeParameterLessThanOrEqualTo(String value) {
            addCriterion("front_commodity_type_parameter <=", value, "frontCommodityTypeParameter");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityTypeParameterLike(String value) {
            addCriterion("front_commodity_type_parameter like", value, "frontCommodityTypeParameter");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityTypeParameterNotLike(String value) {
            addCriterion("front_commodity_type_parameter not like", value, "frontCommodityTypeParameter");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityTypeParameterIn(List<String> values) {
            addCriterion("front_commodity_type_parameter in", values, "frontCommodityTypeParameter");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityTypeParameterNotIn(List<String> values) {
            addCriterion("front_commodity_type_parameter not in", values, "frontCommodityTypeParameter");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityTypeParameterBetween(String value1, String value2) {
            addCriterion("front_commodity_type_parameter between", value1, value2, "frontCommodityTypeParameter");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityTypeParameterNotBetween(String value1, String value2) {
            addCriterion("front_commodity_type_parameter not between", value1, value2, "frontCommodityTypeParameter");
            return (Criteria) this;
        }

        public Criteria andIsDelIsNull() {
            addCriterion("is_del is null");
            return (Criteria) this;
        }

        public Criteria andIsDelIsNotNull() {
            addCriterion("is_del is not null");
            return (Criteria) this;
        }

        public Criteria andIsDelEqualTo(Integer value) {
            addCriterion("is_del =", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelNotEqualTo(Integer value) {
            addCriterion("is_del <>", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelGreaterThan(Integer value) {
            addCriterion("is_del >", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_del >=", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelLessThan(Integer value) {
            addCriterion("is_del <", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelLessThanOrEqualTo(Integer value) {
            addCriterion("is_del <=", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelIn(List<Integer> values) {
            addCriterion("is_del in", values, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelNotIn(List<Integer> values) {
            addCriterion("is_del not in", values, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelBetween(Integer value1, Integer value2) {
            addCriterion("is_del between", value1, value2, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelNotBetween(Integer value1, Integer value2) {
            addCriterion("is_del not between", value1, value2, "isDel");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}