package com.oxygen.back.commodity.service;

import com.github.pagehelper.PageInfo;
import com.oxygen.back.commodity.model.BackCommodity;
import com.oxygen.utils.entity.JsonResponse;

public interface BackCommodityService {

    /**
     * 查询商品
     *
     * @return 商品实体
     */
    PageInfo<BackCommodity> selectCommodity(Integer pageNum, Integer pageSize);

    /**
     * 增加商品
     *
     * @return 受影响行数
     */
    JsonResponse addCommodity(BackCommodity commodity);


    int updateCommodity(BackCommodity commodity);

    /**
     * 删除商品
     *
     * @return 受影响行数
     */
    JsonResponse delCommodity(BackCommodity backCommodity);

    /**
     * 下架商品
     *
     * @return 受影响行数
     */
    JsonResponse undercarriageCommodity(BackCommodity backCommodity);


    /**
     * 上架商品
     *
     * @return 受影响行数
     */
    JsonResponse groundingCommodity(BackCommodity backCommodity);


}
