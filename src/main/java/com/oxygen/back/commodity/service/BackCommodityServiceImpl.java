package com.oxygen.back.commodity.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.oxygen.back.commodity.dao.BackCommodityExtendMapper;
import com.oxygen.back.commodity.model.BackCommodity;
import com.oxygen.back.commodity.model.BackCommodityExample;

import com.oxygen.back.commodity.model.BackCommodityType;
import com.oxygen.utils.entity.JsonResponse;
import com.oxygen.utils.uuid.UUIDUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

@Service
public class BackCommodityServiceImpl implements BackCommodityService {
    @Resource
    private BackCommodityExtendMapper backCommodityExtendMapper;
    @Resource
    private BackCommodityTypeService backCommodityTypeService;


    /**
     * 查询商品
     *
     * @param
     * @return JsonResponse
     */
    @Override
    public PageInfo<BackCommodity> selectCommodity(Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<BackCommodity> commodityList = backCommodityExtendMapper.selectAll();
        return new PageInfo<>(commodityList);
    }

    /**
     * 添加商品
     *
     * @param backCommodity 商品实体
     * @return 状态信息
     */
    @Override
    @Transactional
    public JsonResponse addCommodity(BackCommodity backCommodity) {
        JsonResponse<Map<String, Object>> jsonResponse = new JsonResponse<>();
        Date date = new Date();
        backCommodity.setId(UUIDUtil.getUUID());
        backCommodity.setCreateTime(date);
        backCommodity.setUpdateTime(date);

        int res = backCommodityExtendMapper.insertSelective(backCommodity);
        if (res == 1) {
            int count = backCommodityTypeService.addCommodityType(backCommodity);
            if (count > 0){
                Map<String,Object> results = new HashMap<>();
                results.put("AffectedCount:",count);
                jsonResponse.setData(results);
                jsonResponse.setMsg("添加成功！");
                jsonResponse.setError(false);
            }

        } else {
            jsonResponse.setMsg("添加失败！");
        }
        return jsonResponse;
    }

    @Override
    @Transactional
    public int updateCommodity(BackCommodity commodity) {
        //修改商品
        int insertComCount = backCommodityExtendMapper.updateByPrimaryKeySelective(commodity);
        int statusCode = 0; //用于识别是否成功 1成功，0不成功
        if (insertComCount > 0) {
            //先查询出库中已有数据
            List<BackCommodityType> types = backCommodityTypeService.selectAllByComId(commodity.getId()); //库中当前商品已有类型
            List<BackCommodityType> nowTypes = commodity.getBackCommodityTypeList(); //本次的提交类型
            if (types.containsAll(nowTypes) && nowTypes.containsAll(types)) {
                return 1;
            }
            List<BackCommodityType> delTypes = new ArrayList<>(types); //存放待删除类型
            List<BackCommodityType> insertTypes = new ArrayList<>(nowTypes); //存放待删除类型

            delTypes.removeAll(nowTypes);
            insertTypes.removeAll(types);
            if (delTypes.size() == 0 && insertTypes.size() == 0) {
                return 1;
            }
            //删除没选但库中有的类型
            if (delTypes.size() > 0) {
                int delCount = backCommodityTypeService.batchDelete(delTypes);
                if (delCount > 0) {
                    statusCode = 1;
                } else {
                    statusCode = 0;
                }
            }
            //添加选了但是库中没有的类型
            if (insertTypes.size() > 0) {
                for (BackCommodityType one : insertTypes) {
                    one.setId(UUIDUtil.getUUID());
                    one.setFrontCommodityId(commodity.getId());
                }
                int insertCount = backCommodityTypeService.batchAdd(insertTypes);
                if (insertCount > 0) {
                    statusCode = 1;
                } else {
                    statusCode = 0;
                }
            }
            return statusCode;
        } else {
            return statusCode;
        }


    }


    /**
     * 删除商品
     *
     * @param
     * @return JsonResponse
     */
    @Override
    @Transactional
    public JsonResponse delCommodity(BackCommodity backCommodity) {
        backCommodity.setIsDel(1);
        JsonResponse<Map<String, Object>> jsonResponse = new JsonResponse<>();
        BackCommodityExample backCommodityExample = new BackCommodityExample();
        BackCommodityExample.Criteria criteria = backCommodityExample.createCriteria();
        criteria.andIdEqualTo(backCommodity.getId());
        int res = backCommodityExtendMapper.updateByExampleSelective(backCommodity, backCommodityExample);

        if (res == 1) {
            jsonResponse.setError(false);
            jsonResponse.setMsg("删除成功！");
        } else {
            jsonResponse.setMsg("删除失败！");
        }
        return jsonResponse;
    }
    /**
     * 下架商品
     *
     * @param
     * @return JsonResponse
     */
    @Override
    @Transactional
    public JsonResponse undercarriageCommodity(BackCommodity backCommodity) {

        Date date = new Date();
        backCommodity.setLastAwayTime(date);
        backCommodity.setState(1);
        JsonResponse<Map<String, Object>> jsonResponse = new JsonResponse<>();
        BackCommodityExample backCommodityExample = new BackCommodityExample();
        BackCommodityExample.Criteria criteria = backCommodityExample.createCriteria();
        criteria.andIdEqualTo(backCommodity.getId());
        int res = backCommodityExtendMapper.updateByExampleSelective(backCommodity, backCommodityExample);

        if (res == 1) {
            jsonResponse.setError(false);
            jsonResponse.setMsg("下架成功！");
        } else {
            jsonResponse.setMsg("下架失败！");
        }
        return jsonResponse;
    }

    /**
     * 上架商品
     *
     * @param
     * @return JsonResponse
     */
    @Override
    @Transactional
    public JsonResponse groundingCommodity(BackCommodity backCommodity) {
        Date date = new Date();
        backCommodity.setPutawayTime(date);
        backCommodity.setState(0);

        JsonResponse<Map<String, Object>> jsonResponse = new JsonResponse<>();
        BackCommodityExample backCommodityExample = new BackCommodityExample();
        BackCommodityExample.Criteria criteria = backCommodityExample.createCriteria();
        criteria.andIdEqualTo(backCommodity.getId());
        int res = backCommodityExtendMapper.updateByExampleSelective(backCommodity, backCommodityExample);

        if (res == 1) {
            jsonResponse.setError(false);
            jsonResponse.setMsg("上架成功！");
        } else {
            jsonResponse.setMsg("上架失败！");
        }
        return jsonResponse;
    }
}
