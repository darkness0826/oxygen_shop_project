package com.oxygen.back.commodity.service;

import com.oxygen.back.commodity.model.BackCommodity;
import com.oxygen.back.commodity.model.BackCommodityType;

import java.util.List;

public interface BackCommodityTypeService {


    /**
     * 根据商品id，查找商品的类型
     *
     * @return
     */
    List<BackCommodityType> selectAllByComId(String id);


    /**
     * 添加商品属性
     *
     * @param commodity 商品实体
     * @return 状态信息
     */
    int addCommodityType(BackCommodity commodity);


    /**
     * 批量添加
     *
     * @param params 多个条件参数体
     * @return 受影响行
     */
    int batchAdd(List<BackCommodityType> params);

    /**
     * 批量更新
     *
     * @param params 多个条件参数体
     * @return 受影响行
     */
    int batchUpdate(List<BackCommodityType> params);

    /**
     * 批量删除
     *
     * @return 受影响行
     */
    int batchDelete(List<BackCommodityType> params);
}
