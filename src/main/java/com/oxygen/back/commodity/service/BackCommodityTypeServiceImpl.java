package com.oxygen.back.commodity.service;


import com.oxygen.back.commodity.dao.BackCommodityExtendMapper;
import com.oxygen.back.commodity.dao.BackCommodityTypeExtendMapper;
import com.oxygen.back.commodity.dao.BackCommodityTypeMapper;
import com.oxygen.back.commodity.model.BackCommodity;
import com.oxygen.back.commodity.model.BackCommodityType;
import com.oxygen.back.commodity.model.BackCommodityTypeExample;
import com.oxygen.utils.entity.JsonResponse;
import com.oxygen.utils.uuid.UUIDUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
@Service
public class BackCommodityTypeServiceImpl implements BackCommodityTypeService {
    @Resource
    private BackCommodityTypeExtendMapper backCommodityTypeExtendMapper;


    @Override
    public int addCommodityType(BackCommodity backCommodity) {
        List<BackCommodityType> backCommodityType = backCommodity.getBackCommodityTypeList();
        int results = 0;
        for (BackCommodityType backCommodityType1 : backCommodityType) {
            backCommodityType1.setId(UUIDUtil.getUUID());
            backCommodityType1.setFrontCommodityId(backCommodity.getId());
            int res = backCommodityTypeExtendMapper.insertSelective(backCommodityType1);
            System.out.println(res);
            if (res > 0) {
                results++;
            } else {

            }

        }
        return results;
    }

    @Override
    public int batchAdd(List<BackCommodityType> params) {
        return backCommodityTypeExtendMapper.batchAdd(params);
    }

    @Override
    public int batchUpdate(List<BackCommodityType> params) {
        return backCommodityTypeExtendMapper.batchUpdate(params);
    }

    @Override
    public int batchDelete(List<BackCommodityType> params) {
        return backCommodityTypeExtendMapper.batchDelete(params);
    }

    @Override
    public List<BackCommodityType> selectAllByComId(String id) {
        BackCommodityTypeExample example = new BackCommodityTypeExample();
        BackCommodityTypeExample.Criteria criteria = example.createCriteria();
        criteria.andFrontCommodityIdEqualTo(id);
        return backCommodityTypeExtendMapper.selectByExample(example);
    }


//    public List<BackCommodity> selectCommodityType(List<BackCommodity> backCommodities) {
//        for (BackCommodity backCommodity: backCommodities) {
//            BackCommodityTypeExample backCommodityTypeExample = new BackCommodityTypeExample();
//            BackCommodityTypeExample.Criteria criteriaType = backCommodityTypeExample.createCriteria();
//            criteriaType.andFrontCommodityIdEqualTo(backCommodity.getId());
//            List<BackCommodityType> backCommodityTypeList = backCommodityTypeMapper.selectByExample(backCommodityTypeExample);
//            for (BackCommodityType backCommodityType:backCommodityTypeList) {
//                backCommodity.getBackCommodityTypeList().add(backCommodityType);
//            }
//        }
//        return backCommodities;
//    }


}


