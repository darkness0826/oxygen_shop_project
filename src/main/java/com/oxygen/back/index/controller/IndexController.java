package com.oxygen.back.index.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
@Controller("backIndexController")
@RequestMapping("/back/index")
public class IndexController {
    /**
     * 后台首页页面
     *
     * @return 页面显示
     */
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String index() {
        return "back/index/index";
    }

    /**
     * 后台销量统计页面
     *
     * @return 页面显示
     */
    @RequestMapping(value = "/charts", method = RequestMethod.GET)
    public String charts() {
        return "back/sales_volume/charts";
    }

}
