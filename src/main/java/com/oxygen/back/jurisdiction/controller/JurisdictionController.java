package com.oxygen.back.jurisdiction.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 权限控制器
 */
@Controller
@RequestMapping("/back/jurisdiction")
public class JurisdictionController {
    /**
     * 后台功能权限管理
     *
     * @return 页面显示
     */
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String jurisdiction() {
        return "back/jurisdiction/index";
    }
}
