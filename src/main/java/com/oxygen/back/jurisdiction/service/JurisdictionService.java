package com.oxygen.back.jurisdiction.service;

import com.github.pagehelper.PageInfo;
import com.oxygen.back.jurisdiction.model.Jurisdiction;

import java.util.List;

/**
 * 权限的service
 */
public interface JurisdictionService {
    /**
     * 查询全部
     *
     * @return 权限列表
     */
    List<Jurisdiction> selectAll();


    /**
     * 添加权限
     *
     * @return 影响行数
     */
    int add(Jurisdiction role);

    /**
     * 更新权限
     *
     * @param role 条件实体
     * @return 影响行数
     */
    int update(Jurisdiction role);

    /**
     * 删除权限
     *
     * @param role 条件实体
     * @return 影响行数
     */
    int delete(Jurisdiction role);
}
