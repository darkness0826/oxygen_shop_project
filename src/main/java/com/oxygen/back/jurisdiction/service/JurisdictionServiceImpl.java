package com.oxygen.back.jurisdiction.service;

import com.oxygen.back.jurisdiction.dao.JurisdictionMapper;
import com.oxygen.back.jurisdiction.model.Jurisdiction;
import com.oxygen.back.jurisdiction.model.JurisdictionExample;
import com.oxygen.utils.uuid.UUIDUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Service
public class JurisdictionServiceImpl implements JurisdictionService {
    @Resource
    private JurisdictionMapper jurisdictionMapper;

    @Override
    public List<Jurisdiction> selectAll() {
        JurisdictionExample example = new JurisdictionExample();
        JurisdictionExample.Criteria criteria = example.createCriteria();
        criteria.andIsDelNotEqualTo(1);
        return jurisdictionMapper.selectByExample(example);
    }


    @Override
    @Transactional
    public int add(Jurisdiction jurisdiction) {
        if (jurisdiction != null && StringUtils.isNotBlank(jurisdiction.getRule()) &&
                StringUtils.isNotBlank(jurisdiction.getTitle())) {
            jurisdiction.setCreateTime(new Date());
            jurisdiction.setUpdateTime(new Date());
            jurisdiction.setId(UUIDUtil.getUUID());
            return jurisdictionMapper.insertSelective(jurisdiction);
        }
        return 0;
    }

    @Override
    @Transactional
    public int update(Jurisdiction jurisdiction) {
        if (jurisdiction != null && StringUtils.isNotBlank(jurisdiction.getRule()) &&
                StringUtils.isNotBlank(jurisdiction.getTitle())) {
            jurisdiction.setUpdateTime(new Date());
            return jurisdictionMapper.updateByPrimaryKeySelective(jurisdiction);
        }
        return 0;
    }

    @Override
    @Transactional
    public int delete(Jurisdiction jurisdiction) {
            jurisdiction.setIsDel(1);
            jurisdiction.setUpdateTime(new Date());
            return jurisdictionMapper.updateByPrimaryKeySelective(jurisdiction);

    }
}
