package com.oxygen.back.role.controller;

import com.oxygen.back.role.model.Role;
import com.oxygen.back.role.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
@RequestMapping("/back/role")
public class RoleController {
    @Autowired
    RoleService roleService;

    /**
     * 角色分配管理页显示
     *
     * @return 页面返回
     */
    @RequestMapping(value = "/roleManagement", method = RequestMethod.GET)
    public String roleManagement() {

        return "back/role/role_management";
    }
}
