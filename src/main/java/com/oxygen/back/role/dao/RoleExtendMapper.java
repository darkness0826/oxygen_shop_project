package com.oxygen.back.role.dao;

import com.oxygen.back.role.model.Role;

import java.util.List;

public interface RoleExtendMapper extends RoleMapper {
    /**
     * 查询所有角色（及其权限）
     *
     * @return 查询结果集
     */
    List<Role> selectRoleWithJurisdiction();
}
