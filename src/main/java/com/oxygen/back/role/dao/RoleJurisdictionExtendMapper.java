package com.oxygen.back.role.dao;

import com.oxygen.back.role.model.RoleJurisdiction;
import com.oxygen.back.role.model.RoleJurisdictionExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface RoleJurisdictionExtendMapper extends RoleJurisdictionMapper {
    /**
     * 批量添加
     *
     * @param params 多个条件参数体
     * @return 受影响行
     */
    int batchAdd(Map<String, Object> params);

    /**
     * 批量更新
     *
     * @param params 多个条件参数体
     * @return 受影响行
     */
    int batchUpdate(Map<String, Object> params);

    /**
     * 批量删除
     *
     * @return 受影响行
     */
    int batchDelete(Map<String, Object> params);

}