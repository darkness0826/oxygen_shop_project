package com.oxygen.back.role.dao;

import com.oxygen.back.role.model.RoleJurisdiction;
import com.oxygen.back.role.model.RoleJurisdictionExample;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface RoleJurisdictionMapper {
    long countByExample(RoleJurisdictionExample example);

    int deleteByExample(RoleJurisdictionExample example);

    int deleteByPrimaryKey(String id);

    int insert(RoleJurisdiction record);

    int insertSelective(RoleJurisdiction record);

    List<RoleJurisdiction> selectByExample(RoleJurisdictionExample example);

    RoleJurisdiction selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") RoleJurisdiction record, @Param("example") RoleJurisdictionExample example);

    int updateByExample(@Param("record") RoleJurisdiction record, @Param("example") RoleJurisdictionExample example);

    int updateByPrimaryKeySelective(RoleJurisdiction record);

    int updateByPrimaryKey(RoleJurisdiction record);
}