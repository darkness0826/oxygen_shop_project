package com.oxygen.back.role.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.oxygen.back.jurisdiction.model.Jurisdiction;

import java.util.Date;
import java.util.List;

public class Role {
    private String id;

    private String name;
    @JsonFormat(pattern = "yyyy年MM月dd日 HH时mm分ss秒", locale = "zh", timezone = "GMT+8")
    private Date createTime;
    @JsonFormat(pattern = "yyyy年MM月dd日 HH时mm分ss秒", locale = "zh", timezone = "GMT+8")
    private Date updateTime;

    private Integer isDel;
    //    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<Jurisdiction> jurisdictionList;
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getIsDel() {
        return isDel;
    }

    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }

    public List<Jurisdiction> getJurisdictionList() {
        return jurisdictionList;
    }

    public void setJurisdictionList(List<Jurisdiction> jurisdictionList) {
        this.jurisdictionList = jurisdictionList;
    }

    @Override
    public String toString() {
        return "Role{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", isDel=" + isDel +
                ", jurisdictionList=" + jurisdictionList +
                '}';
    }
}