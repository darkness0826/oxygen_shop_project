package com.oxygen.back.role.model;

import java.util.Objects;

public class RoleJurisdiction {
    private String id;

    private String backRoleId;

    private String backJurisdictionId;

    public RoleJurisdiction() {

    }

    public RoleJurisdiction(String id, String backRoleId, String backJurisdictionId) {
        this.id = id;
        this.backRoleId = backRoleId;
        this.backJurisdictionId = backJurisdictionId;
    }

    public RoleJurisdiction(String backRoleId, String backJurisdictionId) {
        this.backRoleId = backRoleId;
        this.backJurisdictionId = backJurisdictionId;
    }

    public RoleJurisdiction(String backRoleId) {
        this.backRoleId = backRoleId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getBackRoleId() {
        return backRoleId;
    }

    public void setBackRoleId(String backRoleId) {
        this.backRoleId = backRoleId == null ? null : backRoleId.trim();
    }

    public String getBackJurisdictionId() {
        return backJurisdictionId;
    }

    public void setBackJurisdictionId(String backJurisdictionId) {
        this.backJurisdictionId = backJurisdictionId == null ? null : backJurisdictionId.trim();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RoleJurisdiction)) return false;
        RoleJurisdiction that = (RoleJurisdiction) o;
        return Objects.equals(getBackRoleId(), that.getBackRoleId()) &&
                Objects.equals(getBackJurisdictionId(), that.getBackJurisdictionId());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId(), getBackRoleId(), getBackJurisdictionId());
    }
}