package com.oxygen.back.role.service;

import com.oxygen.back.role.model.RoleJurisdiction;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface RoleJurisdictionService {


    /**
     * 根据条件进行查询
     *
     * @return 结果集
     */
    List<RoleJurisdiction> selectByExample(RoleJurisdiction roleJurisdiction);

    /**
     * 角色-权限关联表插入
     *
     * @param roleJurisdiction 条件
     * @return 影响行数
     */
    int add(RoleJurisdiction roleJurisdiction);

    /**
     * 批量添加
     *
     * @param list 多个参数体
     * @return 受影响行
     */
    int batchAdd(Map<String, Object> params);

    /**
     * 批量更新
     *
     * @param list 多个参数体
     * @return 受影响行
     */
    int batchUpdate(Map<String, Object> params);

    /**
     * 批量删除
     *
     * @return 受影响行
     */
    int batchDelete(Map<String, Object> params);

}
