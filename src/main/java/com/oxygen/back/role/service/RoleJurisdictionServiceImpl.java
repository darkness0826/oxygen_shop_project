package com.oxygen.back.role.service;

import com.oxygen.back.role.dao.RoleJurisdictionExtendMapper;
import com.oxygen.back.role.model.RoleJurisdiction;
import com.oxygen.back.role.model.RoleJurisdictionExample;
import com.oxygen.utils.uuid.UUIDUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
public class RoleJurisdictionServiceImpl implements RoleJurisdictionService {
    @Resource
    private RoleJurisdictionExtendMapper roleJurisdictionExtendMapper;

    @Override
    public List<RoleJurisdiction> selectByExample(RoleJurisdiction roleJurisdiction) {
        RoleJurisdictionExample example = new RoleJurisdictionExample();
        RoleJurisdictionExample.Criteria criteria = example.createCriteria();
        criteria.andBackRoleIdEqualTo(roleJurisdiction.getBackRoleId());
        return roleJurisdictionExtendMapper.selectByExample(example);
    }

    @Override
    @Transactional
    public int add(RoleJurisdiction roleJurisdiction) {
        if (roleJurisdiction != null && roleJurisdiction.getBackRoleId() != null && roleJurisdiction.getBackJurisdictionId() != null) {
            roleJurisdiction.setId(UUIDUtil.getUUID());
            return roleJurisdictionExtendMapper.insertSelective(roleJurisdiction);
        }
        return 0;
    }

    @Override
    public int batchAdd(Map<String, Object> params) {
        return roleJurisdictionExtendMapper.batchAdd(params);
    }

    @Override
    public int batchUpdate(Map<String, Object> params) {
        return 0;
    }

    @Override
    public int batchDelete(Map<String, Object> params) {
        return roleJurisdictionExtendMapper.batchDelete(params);
    }
}
