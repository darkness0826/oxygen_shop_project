package com.oxygen.back.role.service;

import com.github.pagehelper.PageInfo;
import com.oxygen.back.role.model.Role;
import com.oxygen.utils.entity.JsonResponse;

import java.util.List;
import java.util.Map;

public interface RoleService {
    /**
     * 查询全部
     * @return 角色列表
     */
    List<Role> selectAll();

    /**
     * 查询所有角色（及其权限）
     *
     * @return 结果集
     */
    public List<Role> selectRoleWithJurisdiction();

    /**
     * 添加角色
     * @return 影响行数
     */
    int add(Role role);

    /**
     * 更新角色
     *
     * @param role 条件实体
     * @return 影响行数
     */
    int update(Role role);

    /**
     * 删除角色
     *
     * @param role 条件实体
     * @return 影响行数
     */
    int delete(Role role);
}
