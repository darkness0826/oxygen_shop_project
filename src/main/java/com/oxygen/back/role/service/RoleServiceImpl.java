package com.oxygen.back.role.service;

import com.github.pagehelper.PageInfo;
import com.oxygen.back.jurisdiction.model.Jurisdiction;
import com.oxygen.back.jurisdiction.service.JurisdictionService;
import com.oxygen.back.role.dao.RoleExtendMapper;
import com.oxygen.back.role.dao.RoleMapper;
import com.oxygen.back.role.model.Role;
import com.oxygen.back.role.model.RoleExample;
import com.oxygen.back.role.model.RoleJurisdiction;
import com.oxygen.back.role.model.RoleJurisdictionExample;
import com.oxygen.utils.entity.JsonResponse;
import com.oxygen.utils.uuid.UUIDUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

@Service
public class RoleServiceImpl implements RoleService {
    @Resource
    private RoleExtendMapper roleExtendMapper;
    @Resource
    private RoleJurisdictionService roleJurisdictionService;


    @Override
    public List<Role> selectAll() {
        RoleExample example = new RoleExample();
        RoleExample.Criteria criteria = example.createCriteria();
        criteria.andIsDelNotEqualTo(1);
        return roleExtendMapper.selectByExample(example);
    }

    @Override
    public List<Role> selectRoleWithJurisdiction() {
        return roleExtendMapper.selectRoleWithJurisdiction();
    }

    @Override
    @Transactional
    public int add(Role role) {
        if (role != null && StringUtils.isNotBlank(role.getName()) && role.getJurisdictionList() != null
                && role.getJurisdictionList().size() > 0) {
            role.setId(UUIDUtil.getUUID());
            role.setCreateTime(new Date());
            role.setUpdateTime(new Date());
            int count = roleExtendMapper.insertSelective(role);
            if(count==1){
                int num = 0; //插入条数(影响行数）
                List<Jurisdiction> jurisdictionList = role.getJurisdictionList();
                for (Jurisdiction jurisdiction : jurisdictionList) {
                    RoleJurisdiction roleJurisdiction = new RoleJurisdiction(role.getId(), jurisdiction.getId());
                    roleJurisdictionService.add(roleJurisdiction);
                    num++;
                }
                return num;

            }
        }
        return 0;

    }

    @Override
    @Transactional
    public int update(Role role) {
        if (role != null && StringUtils.isNotBlank(role.getName()) && role.getJurisdictionList() != null
                && role.getJurisdictionList().size() > 0) {
            role.setUpdateTime(new Date());
            int count = roleExtendMapper.updateByPrimaryKeySelective(role);
            RoleJurisdiction roleJurisdiction = new RoleJurisdiction(role.getId());
            List<RoleJurisdiction> roleJurisdictionList = roleJurisdictionService.selectByExample(roleJurisdiction);//获取当前角色已有权限
            List<RoleJurisdiction> nowRoleJurisdictionList = new ArrayList<>();
            List<RoleJurisdiction> delList = new ArrayList<>(roleJurisdictionList); //最终存放待删除
            List<RoleJurisdiction> preAddList = new ArrayList<>(roleJurisdictionList);
            List<RoleJurisdiction> addList = new ArrayList<>(); //最终存放待添加
            for (Jurisdiction jurisdiction : role.getJurisdictionList()) {
                nowRoleJurisdictionList.add(new RoleJurisdiction(role.getId(), jurisdiction.getId()));
                addList.add(new RoleJurisdiction(UUIDUtil.getUUID(), role.getId(), jurisdiction.getId()));
            }
            //如果两者互相包含，则两者相等
            if (roleJurisdictionList.containsAll(nowRoleJurisdictionList) && nowRoleJurisdictionList.containsAll(roleJurisdictionList)) {
                return 1;
            }
            delList.removeAll(nowRoleJurisdictionList);
            addList.removeAll(preAddList);
            if (delList.size() > 0) {
                Map<String, Object> delParams = new HashMap<>();
                delParams.put("jids", delList);
                delParams.put("roleId", role.getId());
                count += roleJurisdictionService.batchDelete(delParams);
            }

            if (addList.size() > 0) {
                Map<String, Object> addParams = new HashMap<>();
                addParams.put("jids", addList);
                addParams.put("roleId", role.getId());
                count += roleJurisdictionService.batchAdd(addParams);
            }
            return count;

        }
        return 0;

    }

    @Override
    @Transactional
    public int delete(Role role) {
        if (role != null && StringUtils.isNotBlank(role.getId())) {
            role.setUpdateTime(new Date());
            role.setIsDel(1);
            return roleExtendMapper.updateByPrimaryKeySelective(role);
        }
        return 0;


    }
}
