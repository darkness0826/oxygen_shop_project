package com.oxygen.back.user.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/back/user")
public class BackUserController {
    /**
     * 后台用户管理页面
     *
     * @return 页面显示
     */
    @RequestMapping(value = "/userManagement", method = RequestMethod.GET)
    public String userManagement() {
        return "back/user/user_management";
    }
}
