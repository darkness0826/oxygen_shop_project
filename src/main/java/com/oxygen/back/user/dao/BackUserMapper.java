package com.oxygen.back.user.dao;

import com.oxygen.back.user.model.BackUser;
import com.oxygen.back.user.model.BackUserExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface BackUserMapper {
    long countByExample(BackUserExample example);

    int deleteByExample(BackUserExample example);

    int deleteByPrimaryKey(String id);

    int insert(BackUser record);

    int insertSelective(BackUser record);

    List<BackUser> selectByExample(BackUserExample example);

    BackUser selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") BackUser record, @Param("example") BackUserExample example);

    int updateByExample(@Param("record") BackUser record, @Param("example") BackUserExample example);

    int updateByPrimaryKeySelective(BackUser record);

    int updateByPrimaryKey(BackUser record);
}