package com.oxygen.back.user.service;

import com.github.pagehelper.PageInfo;
import com.oxygen.back.user.model.BackUser;
import com.oxygen.front.user.model.User;
import com.oxygen.utils.entity.JsonResponse;

import javax.servlet.http.HttpSession;
import java.util.List;

public interface BackUserService {

    /**
     * 获得多个用户
     * @return 多个用户
     */
    List<BackUser> getUsers();


    PageInfo<BackUser> getUsersLimited(Integer pageNum, Integer pageSize);


    /**
     * 修改操作
     *
     * @return 响应信息
     */
    JsonResponse updateUser(BackUser user);
    /**
     * 删除操作
     *
     * @return 响应信息
     */
    JsonResponse delUser(BackUser user);
}
