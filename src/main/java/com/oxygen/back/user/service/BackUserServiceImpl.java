package com.oxygen.back.user.service;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.oxygen.back.user.dao.BackUserMapper;
import com.oxygen.back.user.model.BackUser;
import com.oxygen.back.user.model.BackUserExample;
import com.oxygen.utils.entity.JsonResponse;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;


/**
 * 后台版的前台用户service
 */
@Service
public class BackUserServiceImpl implements BackUserService {
    @Resource
    private BackUserMapper backUserMapper;

    @Override
    public List<BackUser> getUsers() {
        BackUserExample userExample = new BackUserExample();
        BackUserExample.Criteria criteria = userExample.createCriteria();
        criteria.andIsDelNotEqualTo(1);
        return backUserMapper.selectByExample(userExample);
    }

    @Override
    public PageInfo<BackUser> getUsersLimited(Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        BackUserExample userExample = new BackUserExample();
        BackUserExample.Criteria criteria = userExample.createCriteria();
        criteria.andIsDelNotEqualTo(1);
        List<BackUser> backUserList = backUserMapper.selectByExample(userExample);
        PageInfo<BackUser> pageInfo = new PageInfo<>(backUserList);
        return pageInfo;
    }


    @Override
    @Transactional
    public JsonResponse updateUser(BackUser backUser ) {
        JsonResponse jsonResponse = new JsonResponse();
        backUser.setUpdateTime(new Date());
        int res = backUserMapper.updateByPrimaryKeySelective(backUser);
        if (res == 1) {
            jsonResponse.setError(false);
            jsonResponse.setMsg("修改成功！");

        } else {
            jsonResponse.setMsg("修改失败！");
        }
        return jsonResponse;
    }

    @Override
    @Transactional
    public JsonResponse delUser(BackUser backUser) {
        JsonResponse jsonResponse = new JsonResponse();
        backUser.setUpdateTime(new Date());
        int res = backUserMapper.updateByPrimaryKeySelective(backUser);
        if (res == 1) {
            jsonResponse.setError(false);
            jsonResponse.setMsg("删除成功！");

        } else {
            jsonResponse.setMsg("删除失败！");
        }
        return jsonResponse;
    }
}
