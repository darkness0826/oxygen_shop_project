package com.oxygen.front.api.user;

import com.oxygen.front.user.model.User;
import com.oxygen.front.user.service.UserService;
import com.oxygen.utils.entity.JsonResponse;
import com.oxygen.utils.entity.SessionKey;
import com.oxygen.utils.uuid.UUIDUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Date;
import java.util.Map;


/**
 * 前台用户API接口
 */
@Component
@RequestMapping("/user")
public class UserApi {
    /**
     *
     */
    @Autowired
    private UserService userService;

    /**
     * 用户登录处理逻辑
     *
     * @param user user实体
     * @return JsonResponse 返回信息
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse login(User user, HttpSession session) {
        return userService.login(user, session);
    }

    /**
     * 用户注册处理逻辑
     *
     * @param user User 实体
     * @return 返回处理操作
     */
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse register(User user) {
        user.setId(UUIDUtil.getUUID());
        user.setCreateDate(new Date());
        user.setIntegral(0);
        user.setType(0);
        user.setIsDel(0);
        return userService.addUser(user);


    }

    /**
     * 检测用户名是否存在
     */
    @RequestMapping(value = "/test", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse test(User user) {

        String a = userService.select(user);
        System.out.println(a);
        if (a == "a") {
            return userService.error();
        } else {
            JsonResponse<Map<String, Object>> jsonResponse = new JsonResponse<>();
            jsonResponse.setError(false);
            return jsonResponse;
        }

    }
    /**
     * 用户的个人中心
     */




    /**
     * 用户修改处理逻辑
     *
     * @param user User 实体
     * @return 返回处理操作
     */
    @RequestMapping(value = "/alter", method = RequestMethod.POST)
    @ResponseBody
    public JsonResponse alter(User user,HttpSession session) {

        return userService.alterUser(user,session);
    }

    /**
     * 用户退出处理逻辑
     *
     * @param httpSession session本体
     * @return 返回信息
     */
    @RequestMapping(value = "/quit", method = RequestMethod.GET)
    @ResponseBody
    public void quit(HttpSession httpSession, HttpServletResponse response, HttpServletRequest request) throws IOException {
        if (httpSession.getAttribute(SessionKey.LOGIN_USER_INFO) != null) {
            httpSession.removeAttribute(SessionKey.LOGIN_USER_INFO);
        }
        response.sendRedirect(request.getContextPath() + "/user/login");
        return;

    }
}
