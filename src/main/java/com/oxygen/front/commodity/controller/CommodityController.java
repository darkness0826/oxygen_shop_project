package com.oxygen.front.commodity.controller;


import com.oxygen.front.commodity.model.Commodity;
import com.oxygen.front.commodity.model.CommodityType;
import com.oxygen.front.commodity.service.CommodityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller("CommodityController")
@RequestMapping("/commodity")
public class CommodityController {
    @Autowired
    private CommodityService commodityService;

    /**
     * 搜索到的商品列表
     *
     * @return
     */

    @RequestMapping(value = "/search/{name}", method = RequestMethod.GET)
    public String search(Model model, @PathVariable("name") String name) {
        Commodity commodity = new Commodity();
        commodity.setName(name);
        List<Commodity> commoditys = commodityService.selectCommodityAll(commodity);
        //System.out.println(commoditys.size());
        model.addAttribute("commodity", commoditys);
        return "front/search";
    }

    /**
     * 商品的详情页
     *
     * @return
     */
    @RequestMapping(value = "/single/{id}", method = RequestMethod.GET)
    public String single(Model model, @PathVariable("id") String id, HttpServletResponse response) {
        if(id.equals("")||id==null){//id的非空验证
            return "front/404";
        }
        Map<Commodity, List<CommodityType>> commodity = commodityService.getCommodity(id);
        Commodity commd = null;
        List<CommodityType> typelist = new ArrayList<>();
        for(Map.Entry<Commodity, List<CommodityType>> i:commodity.entrySet()){
            commd = i.getKey();
            typelist = i.getValue();
        }
        List<String> typeName = new ArrayList<>();//样式名字列表
        List<List<String>>typePropertyId = new ArrayList<>();//对应的样式的id
        List<List<String>>typeProperty = new ArrayList<>();//对应的样式
        List<String> picture = new ArrayList<>();//详情图片
        for(CommodityType i:typelist){
            if(!typeName.contains(i.getFrontCommodityType())){
                if(i.getFrontCommodityType().equals("picture")){
                    picture.add(i.getFrontCommodityTypeParameter());
                }else{
                    typeName.add(i.getFrontCommodityType());
                    List<String> listId = new ArrayList<>();
                    listId.add(i.getId());
                    typePropertyId.add(listId);

                    List<String> list = new ArrayList<>();
                    list.add(i.getFrontCommodityTypeParameter());
                    typeProperty.add(list);
                }
            }else{
                int t = typeName.size();
                for(int j = 0;j<t;j++){
                    if(i.getFrontCommodityType().equals(typeName.get(j))){
                        typeProperty.get(j).add(i.getFrontCommodityTypeParameter());
                        typePropertyId.get(j).add(i.getFrontCommodityId());
                    }
                }
            }
        }
        model.addAttribute("commodity", commd);//商品
        model.addAttribute("typeName", typeName);//属性列表
        model.addAttribute("typePropertyId", typePropertyId);//属性列表
        model.addAttribute("typeProperty", typeProperty);//属性列表
        model.addAttribute("picture", picture);//详情照片轮播
//        System.out.println(typelist.get(0).getFrontCommodityType());
        return "front/single";
    }

}
