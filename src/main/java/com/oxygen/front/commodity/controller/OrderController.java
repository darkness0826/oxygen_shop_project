package com.oxygen.front.commodity.controller;


import com.oxygen.front.commodity.dao.OrderMapper;
import com.oxygen.front.commodity.model.CommodityTypeExample;
import com.oxygen.front.commodity.model.Order;
import com.oxygen.front.commodity.service.OrderService;
import com.oxygen.front.user.model.User;
import com.oxygen.utils.entity.SessionKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller("OrderController")
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderMapper orderMapper;


    /**
     * 购物车
     */
    @RequestMapping(value = "/single", method = RequestMethod.GET)
    public String single(Model model, HttpSession session) {
        User user = (User) session.getAttribute(SessionKey.LOGIN_USER_INFO);
        if (user != null) {
            String id = user.getId();
            System.out.println(user.getId());
            List<Order> orders = (List<Order>) orderMapper.select(id);
            System.out.println(orders.size());
            model.addAttribute("orders", orders);
           // System.out.println(orders.get(0).getCommodity().getFrontCommodityThumbnail());
            return "/front/cart";
        } else {
            return "/front/user/login";
        }
    }
    /**
     * 确认购物车
     */
    @RequestMapping(value = "/confirm", method = RequestMethod.GET)
    public String confirm(Model model, HttpSession session) {
        User user = (User) session.getAttribute(SessionKey.LOGIN_USER_INFO);
        if (user != null) {
            String id = user.getId();
            System.out.println(user.getId());
            List<Order> orders = (List<Order>) orderMapper.select(id);
            System.out.println(orders.size());
            model.addAttribute("orders", orders);
            // System.out.println(orders.get(0).getCommodity().getFrontCommodityThumbnail());
            return "/front/confirm";
        } else {
            return "/front/user/login";
        }
    }
    /**
     * 结算购物车
     */
    @RequestMapping(value = "/pay", method = RequestMethod.GET)
    public String pay(Model model, HttpSession session) {
        User user = (User) session.getAttribute(SessionKey.LOGIN_USER_INFO);
        if (user != null) {
            String id = user.getId();
            System.out.println(user.getId());
            List<Order> orders = (List<Order>) orderMapper.select(id);
            System.out.println(orders.size());
            model.addAttribute("orders", orders);
            // System.out.println(orders.get(0).getCommodity().getFrontCommodityThumbnail());
            return "/front/pay";
        } else {
            return "/front/user/login";
        }
    }

}
