package com.oxygen.front.commodity.dao;

import com.oxygen.front.commodity.model.Order;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrderMapper {
    List<Order> select(@Param("id") String id);

}
