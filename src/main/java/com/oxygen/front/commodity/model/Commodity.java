package com.oxygen.front.commodity.model;

import java.math.BigDecimal;
import java.util.Date;

public class Commodity  {
    private String id;

    private String name;

    private String frontCommodityThumbnail;

    private BigDecimal price;

    private Date putawayTime;

    private Date lastAwayTime;

    private Integer state;

    private Integer isDel;

    private Integer commoditySalesNum;

    private Date createTime;

    private Date updateTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getFrontCommodityThumbnail() {
        return frontCommodityThumbnail;
    }

    public void setFrontCommodityThumbnail(String frontCommodityThumbnail) {
        this.frontCommodityThumbnail = frontCommodityThumbnail == null ? null : frontCommodityThumbnail.trim();
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Date getPutawayTime() {
        return putawayTime;
    }

    public void setPutawayTime(Date putawayTime) {
        this.putawayTime = putawayTime;
    }

    public Date getLastAwayTime() {
        return lastAwayTime;
    }

    public void setLastAwayTime(Date lastAwayTime) {
        this.lastAwayTime = lastAwayTime;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getIsDel() {
        return isDel;
    }

    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }

    public Integer getCommoditySalesNum() {
        return commoditySalesNum;
    }

    public void setCommoditySalesNum(Integer commoditySalesNum) {
        this.commoditySalesNum = commoditySalesNum;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}