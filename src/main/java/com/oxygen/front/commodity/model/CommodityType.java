package com.oxygen.front.commodity.model;

public class CommodityType {
    private String id;

    private String frontCommodityId;

    private String frontCommodityType;

    private String frontCommodityTypeParameter;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getFrontCommodityId() {
        return frontCommodityId;
    }

    public void setFrontCommodityId(String frontCommodityId) {
        this.frontCommodityId = frontCommodityId == null ? null : frontCommodityId.trim();
    }

    public String getFrontCommodityType() {
        return frontCommodityType;
    }

    public void setFrontCommodityType(String frontCommodityType) {
        this.frontCommodityType = frontCommodityType == null ? null : frontCommodityType.trim();
    }

    public String getFrontCommodityTypeParameter() {
        return frontCommodityTypeParameter;
    }

    public void setFrontCommodityTypeParameter(String frontCommodityTypeParameter) {
        this.frontCommodityTypeParameter = frontCommodityTypeParameter == null ? null : frontCommodityTypeParameter.trim();
    }
}