package com.oxygen.front.commodity.model;

import com.oxygen.front.user.model.User;

import java.math.BigDecimal;
import java.util.Date;

public class Order {
    private String id;

    private User user;

    private Commodity commodity;


    private Address address;

    private String frontAddressId;

    private Integer frontCommodityNum;

    private BigDecimal frontPriceOriginal;

    private BigDecimal frontPriceNum;

    private Date creatTime;

    private Integer state;

    private Integer isDel;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }


    public String getFrontAddressId() {
        return frontAddressId;
    }

    public void setFrontAddressId(String frontAddressId) {
        this.frontAddressId = frontAddressId == null ? null : frontAddressId.trim();
    }

    public Integer getFrontCommodityNum() {
        return frontCommodityNum;
    }

    public void setFrontCommodityNum(Integer frontCommodityNum) {
        this.frontCommodityNum = frontCommodityNum;
    }

    public BigDecimal getFrontPriceOriginal() {
        return frontPriceOriginal;
    }

    public void setFrontPriceOriginal(BigDecimal frontPriceOriginal) {
        this.frontPriceOriginal = frontPriceOriginal;
    }

    public BigDecimal getFrontPriceNum() {
        return frontPriceNum;
    }

    public void setFrontPriceNum(BigDecimal frontPriceNum) {
        this.frontPriceNum = frontPriceNum;
    }

    public Date getCreatTime() {
        return creatTime;
    }

    public void setCreatTime(Date creatTime) {
        this.creatTime = creatTime;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getIsDel() {
        return isDel;
    }

    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Commodity getCommodity() {
        return commodity;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }
}