package com.oxygen.front.commodity.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OrderExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public OrderExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andFrontUserIdIsNull() {
            addCriterion("front_user_id is null");
            return (Criteria) this;
        }

        public Criteria andFrontUserIdIsNotNull() {
            addCriterion("front_user_id is not null");
            return (Criteria) this;
        }

        public Criteria andFrontUserIdEqualTo(String value) {
            addCriterion("front_user_id =", value, "frontUserId");
            return (Criteria) this;
        }

        public Criteria andFrontUserIdNotEqualTo(String value) {
            addCriterion("front_user_id <>", value, "frontUserId");
            return (Criteria) this;
        }

        public Criteria andFrontUserIdGreaterThan(String value) {
            addCriterion("front_user_id >", value, "frontUserId");
            return (Criteria) this;
        }

        public Criteria andFrontUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("front_user_id >=", value, "frontUserId");
            return (Criteria) this;
        }

        public Criteria andFrontUserIdLessThan(String value) {
            addCriterion("front_user_id <", value, "frontUserId");
            return (Criteria) this;
        }

        public Criteria andFrontUserIdLessThanOrEqualTo(String value) {
            addCriterion("front_user_id <=", value, "frontUserId");
            return (Criteria) this;
        }

        public Criteria andFrontUserIdLike(String value) {
            addCriterion("front_user_id like", value, "frontUserId");
            return (Criteria) this;
        }

        public Criteria andFrontUserIdNotLike(String value) {
            addCriterion("front_user_id not like", value, "frontUserId");
            return (Criteria) this;
        }

        public Criteria andFrontUserIdIn(List<String> values) {
            addCriterion("front_user_id in", values, "frontUserId");
            return (Criteria) this;
        }

        public Criteria andFrontUserIdNotIn(List<String> values) {
            addCriterion("front_user_id not in", values, "frontUserId");
            return (Criteria) this;
        }

        public Criteria andFrontUserIdBetween(String value1, String value2) {
            addCriterion("front_user_id between", value1, value2, "frontUserId");
            return (Criteria) this;
        }

        public Criteria andFrontUserIdNotBetween(String value1, String value2) {
            addCriterion("front_user_id not between", value1, value2, "frontUserId");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityIdIsNull() {
            addCriterion("front_commodity_id is null");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityIdIsNotNull() {
            addCriterion("front_commodity_id is not null");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityIdEqualTo(String value) {
            addCriterion("front_commodity_id =", value, "frontCommodityId");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityIdNotEqualTo(String value) {
            addCriterion("front_commodity_id <>", value, "frontCommodityId");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityIdGreaterThan(String value) {
            addCriterion("front_commodity_id >", value, "frontCommodityId");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityIdGreaterThanOrEqualTo(String value) {
            addCriterion("front_commodity_id >=", value, "frontCommodityId");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityIdLessThan(String value) {
            addCriterion("front_commodity_id <", value, "frontCommodityId");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityIdLessThanOrEqualTo(String value) {
            addCriterion("front_commodity_id <=", value, "frontCommodityId");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityIdLike(String value) {
            addCriterion("front_commodity_id like", value, "frontCommodityId");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityIdNotLike(String value) {
            addCriterion("front_commodity_id not like", value, "frontCommodityId");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityIdIn(List<String> values) {
            addCriterion("front_commodity_id in", values, "frontCommodityId");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityIdNotIn(List<String> values) {
            addCriterion("front_commodity_id not in", values, "frontCommodityId");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityIdBetween(String value1, String value2) {
            addCriterion("front_commodity_id between", value1, value2, "frontCommodityId");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityIdNotBetween(String value1, String value2) {
            addCriterion("front_commodity_id not between", value1, value2, "frontCommodityId");
            return (Criteria) this;
        }

        public Criteria andFrontAddressIdIsNull() {
            addCriterion("front_address_id is null");
            return (Criteria) this;
        }

        public Criteria andFrontAddressIdIsNotNull() {
            addCriterion("front_address_id is not null");
            return (Criteria) this;
        }

        public Criteria andFrontAddressIdEqualTo(String value) {
            addCriterion("front_address_id =", value, "frontAddressId");
            return (Criteria) this;
        }

        public Criteria andFrontAddressIdNotEqualTo(String value) {
            addCriterion("front_address_id <>", value, "frontAddressId");
            return (Criteria) this;
        }

        public Criteria andFrontAddressIdGreaterThan(String value) {
            addCriterion("front_address_id >", value, "frontAddressId");
            return (Criteria) this;
        }

        public Criteria andFrontAddressIdGreaterThanOrEqualTo(String value) {
            addCriterion("front_address_id >=", value, "frontAddressId");
            return (Criteria) this;
        }

        public Criteria andFrontAddressIdLessThan(String value) {
            addCriterion("front_address_id <", value, "frontAddressId");
            return (Criteria) this;
        }

        public Criteria andFrontAddressIdLessThanOrEqualTo(String value) {
            addCriterion("front_address_id <=", value, "frontAddressId");
            return (Criteria) this;
        }

        public Criteria andFrontAddressIdLike(String value) {
            addCriterion("front_address_id like", value, "frontAddressId");
            return (Criteria) this;
        }

        public Criteria andFrontAddressIdNotLike(String value) {
            addCriterion("front_address_id not like", value, "frontAddressId");
            return (Criteria) this;
        }

        public Criteria andFrontAddressIdIn(List<String> values) {
            addCriterion("front_address_id in", values, "frontAddressId");
            return (Criteria) this;
        }

        public Criteria andFrontAddressIdNotIn(List<String> values) {
            addCriterion("front_address_id not in", values, "frontAddressId");
            return (Criteria) this;
        }

        public Criteria andFrontAddressIdBetween(String value1, String value2) {
            addCriterion("front_address_id between", value1, value2, "frontAddressId");
            return (Criteria) this;
        }

        public Criteria andFrontAddressIdNotBetween(String value1, String value2) {
            addCriterion("front_address_id not between", value1, value2, "frontAddressId");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityNumIsNull() {
            addCriterion("front_commodity_num is null");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityNumIsNotNull() {
            addCriterion("front_commodity_num is not null");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityNumEqualTo(Integer value) {
            addCriterion("front_commodity_num =", value, "frontCommodityNum");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityNumNotEqualTo(Integer value) {
            addCriterion("front_commodity_num <>", value, "frontCommodityNum");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityNumGreaterThan(Integer value) {
            addCriterion("front_commodity_num >", value, "frontCommodityNum");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("front_commodity_num >=", value, "frontCommodityNum");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityNumLessThan(Integer value) {
            addCriterion("front_commodity_num <", value, "frontCommodityNum");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityNumLessThanOrEqualTo(Integer value) {
            addCriterion("front_commodity_num <=", value, "frontCommodityNum");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityNumIn(List<Integer> values) {
            addCriterion("front_commodity_num in", values, "frontCommodityNum");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityNumNotIn(List<Integer> values) {
            addCriterion("front_commodity_num not in", values, "frontCommodityNum");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityNumBetween(Integer value1, Integer value2) {
            addCriterion("front_commodity_num between", value1, value2, "frontCommodityNum");
            return (Criteria) this;
        }

        public Criteria andFrontCommodityNumNotBetween(Integer value1, Integer value2) {
            addCriterion("front_commodity_num not between", value1, value2, "frontCommodityNum");
            return (Criteria) this;
        }

        public Criteria andFrontPriceOriginalIsNull() {
            addCriterion("front_price_original is null");
            return (Criteria) this;
        }

        public Criteria andFrontPriceOriginalIsNotNull() {
            addCriterion("front_price_original is not null");
            return (Criteria) this;
        }

        public Criteria andFrontPriceOriginalEqualTo(BigDecimal value) {
            addCriterion("front_price_original =", value, "frontPriceOriginal");
            return (Criteria) this;
        }

        public Criteria andFrontPriceOriginalNotEqualTo(BigDecimal value) {
            addCriterion("front_price_original <>", value, "frontPriceOriginal");
            return (Criteria) this;
        }

        public Criteria andFrontPriceOriginalGreaterThan(BigDecimal value) {
            addCriterion("front_price_original >", value, "frontPriceOriginal");
            return (Criteria) this;
        }

        public Criteria andFrontPriceOriginalGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("front_price_original >=", value, "frontPriceOriginal");
            return (Criteria) this;
        }

        public Criteria andFrontPriceOriginalLessThan(BigDecimal value) {
            addCriterion("front_price_original <", value, "frontPriceOriginal");
            return (Criteria) this;
        }

        public Criteria andFrontPriceOriginalLessThanOrEqualTo(BigDecimal value) {
            addCriterion("front_price_original <=", value, "frontPriceOriginal");
            return (Criteria) this;
        }

        public Criteria andFrontPriceOriginalIn(List<BigDecimal> values) {
            addCriterion("front_price_original in", values, "frontPriceOriginal");
            return (Criteria) this;
        }

        public Criteria andFrontPriceOriginalNotIn(List<BigDecimal> values) {
            addCriterion("front_price_original not in", values, "frontPriceOriginal");
            return (Criteria) this;
        }

        public Criteria andFrontPriceOriginalBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("front_price_original between", value1, value2, "frontPriceOriginal");
            return (Criteria) this;
        }

        public Criteria andFrontPriceOriginalNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("front_price_original not between", value1, value2, "frontPriceOriginal");
            return (Criteria) this;
        }

        public Criteria andFrontPriceNumIsNull() {
            addCriterion("front_price_num is null");
            return (Criteria) this;
        }

        public Criteria andFrontPriceNumIsNotNull() {
            addCriterion("front_price_num is not null");
            return (Criteria) this;
        }

        public Criteria andFrontPriceNumEqualTo(BigDecimal value) {
            addCriterion("front_price_num =", value, "frontPriceNum");
            return (Criteria) this;
        }

        public Criteria andFrontPriceNumNotEqualTo(BigDecimal value) {
            addCriterion("front_price_num <>", value, "frontPriceNum");
            return (Criteria) this;
        }

        public Criteria andFrontPriceNumGreaterThan(BigDecimal value) {
            addCriterion("front_price_num >", value, "frontPriceNum");
            return (Criteria) this;
        }

        public Criteria andFrontPriceNumGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("front_price_num >=", value, "frontPriceNum");
            return (Criteria) this;
        }

        public Criteria andFrontPriceNumLessThan(BigDecimal value) {
            addCriterion("front_price_num <", value, "frontPriceNum");
            return (Criteria) this;
        }

        public Criteria andFrontPriceNumLessThanOrEqualTo(BigDecimal value) {
            addCriterion("front_price_num <=", value, "frontPriceNum");
            return (Criteria) this;
        }

        public Criteria andFrontPriceNumIn(List<BigDecimal> values) {
            addCriterion("front_price_num in", values, "frontPriceNum");
            return (Criteria) this;
        }

        public Criteria andFrontPriceNumNotIn(List<BigDecimal> values) {
            addCriterion("front_price_num not in", values, "frontPriceNum");
            return (Criteria) this;
        }

        public Criteria andFrontPriceNumBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("front_price_num between", value1, value2, "frontPriceNum");
            return (Criteria) this;
        }

        public Criteria andFrontPriceNumNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("front_price_num not between", value1, value2, "frontPriceNum");
            return (Criteria) this;
        }

        public Criteria andCreatTimeIsNull() {
            addCriterion("creat_time is null");
            return (Criteria) this;
        }

        public Criteria andCreatTimeIsNotNull() {
            addCriterion("creat_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreatTimeEqualTo(Date value) {
            addCriterion("creat_time =", value, "creatTime");
            return (Criteria) this;
        }

        public Criteria andCreatTimeNotEqualTo(Date value) {
            addCriterion("creat_time <>", value, "creatTime");
            return (Criteria) this;
        }

        public Criteria andCreatTimeGreaterThan(Date value) {
            addCriterion("creat_time >", value, "creatTime");
            return (Criteria) this;
        }

        public Criteria andCreatTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("creat_time >=", value, "creatTime");
            return (Criteria) this;
        }

        public Criteria andCreatTimeLessThan(Date value) {
            addCriterion("creat_time <", value, "creatTime");
            return (Criteria) this;
        }

        public Criteria andCreatTimeLessThanOrEqualTo(Date value) {
            addCriterion("creat_time <=", value, "creatTime");
            return (Criteria) this;
        }

        public Criteria andCreatTimeIn(List<Date> values) {
            addCriterion("creat_time in", values, "creatTime");
            return (Criteria) this;
        }

        public Criteria andCreatTimeNotIn(List<Date> values) {
            addCriterion("creat_time not in", values, "creatTime");
            return (Criteria) this;
        }

        public Criteria andCreatTimeBetween(Date value1, Date value2) {
            addCriterion("creat_time between", value1, value2, "creatTime");
            return (Criteria) this;
        }

        public Criteria andCreatTimeNotBetween(Date value1, Date value2) {
            addCriterion("creat_time not between", value1, value2, "creatTime");
            return (Criteria) this;
        }

        public Criteria andStateIsNull() {
            addCriterion("state is null");
            return (Criteria) this;
        }

        public Criteria andStateIsNotNull() {
            addCriterion("state is not null");
            return (Criteria) this;
        }

        public Criteria andStateEqualTo(Integer value) {
            addCriterion("state =", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotEqualTo(Integer value) {
            addCriterion("state <>", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThan(Integer value) {
            addCriterion("state >", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThanOrEqualTo(Integer value) {
            addCriterion("state >=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThan(Integer value) {
            addCriterion("state <", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThanOrEqualTo(Integer value) {
            addCriterion("state <=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateIn(List<Integer> values) {
            addCriterion("state in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotIn(List<Integer> values) {
            addCriterion("state not in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateBetween(Integer value1, Integer value2) {
            addCriterion("state between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotBetween(Integer value1, Integer value2) {
            addCriterion("state not between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andIsDelIsNull() {
            addCriterion("is_del is null");
            return (Criteria) this;
        }

        public Criteria andIsDelIsNotNull() {
            addCriterion("is_del is not null");
            return (Criteria) this;
        }

        public Criteria andIsDelEqualTo(Integer value) {
            addCriterion("is_del =", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelNotEqualTo(Integer value) {
            addCriterion("is_del <>", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelGreaterThan(Integer value) {
            addCriterion("is_del >", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_del >=", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelLessThan(Integer value) {
            addCriterion("is_del <", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelLessThanOrEqualTo(Integer value) {
            addCriterion("is_del <=", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelIn(List<Integer> values) {
            addCriterion("is_del in", values, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelNotIn(List<Integer> values) {
            addCriterion("is_del not in", values, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelBetween(Integer value1, Integer value2) {
            addCriterion("is_del between", value1, value2, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelNotBetween(Integer value1, Integer value2) {
            addCriterion("is_del not between", value1, value2, "isDel");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}