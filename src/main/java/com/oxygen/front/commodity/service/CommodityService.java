package com.oxygen.front.commodity.service;

import com.oxygen.front.commodity.model.Commodity;
import com.oxygen.front.commodity.model.CommodityType;

import java.util.List;
import java.util.Map;

public interface CommodityService {
    /**
     * 查询某个商品
     *的详情信息
     * @param id
     * @return
     */
    Map<Commodity, List<CommodityType>> getCommodity(String id);

    /**
     * 获得全部商品
     *
     * @return 商品列表
     */
    List<Commodity> selectCommodityAll(Commodity commodity);

}
