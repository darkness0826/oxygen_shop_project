package com.oxygen.front.commodity.service;


import com.oxygen.front.commodity.dao.CommodityMapper;
import com.oxygen.front.commodity.dao.CommodityTypeMapper;
import com.oxygen.front.commodity.model.Commodity;
import com.oxygen.front.commodity.model.CommodityExample;
import com.oxygen.front.commodity.model.CommodityType;
import com.oxygen.front.commodity.model.CommodityTypeExample;
import com.oxygen.utils.entity.JsonResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CommodityServiceImpl implements CommodityService {
    @Autowired
    private CommodityMapper commodityMapper;
    @Autowired
    private CommodityTypeMapper commodityTypeMapper;


    /**
     * 根据id查询某个商品
     *的详情信息
     * @param id
     * @return
     */
    @Override
    public Map<Commodity, List<CommodityType>> getCommodity(String id) {
        Commodity com = commodityMapper.selectByPrimaryKey(id);
        CommodityTypeExample commodityTypeExample = new CommodityTypeExample();
        CommodityTypeExample.Criteria criteria = commodityTypeExample.createCriteria();
        criteria.andFrontCommodityIdEqualTo(id);
        List<CommodityType> comType = commodityTypeMapper.selectByExample(commodityTypeExample);
        Map<Commodity, List<CommodityType>> map = new HashMap<>();
        map.put(com, comType);
        System.out.println(comType.get(0).getFrontCommodityType()+comType.get(0).getFrontCommodityTypeParameter());
        return map;
    }

    /**
     * 根据name查询全部商品
     *
     * @param commodity
     * @return
     */
    @Override
    public List<Commodity> selectCommodityAll(Commodity commodity) {
        CommodityExample commodityExample = new CommodityExample();
        CommodityExample.Criteria criteria = commodityExample.createCriteria();

        criteria.andNameLike("%" + commodity.getName() + "%");

        return commodityMapper.selectByExample(commodityExample);
    }
}
