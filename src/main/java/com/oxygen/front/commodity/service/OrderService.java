package com.oxygen.front.commodity.service;

import com.oxygen.front.commodity.model.Order;

import java.util.List;

public interface OrderService {


    /**
     * 查询购物车里的商品
     */
    List<Order> selectOrder(Order order);
}
