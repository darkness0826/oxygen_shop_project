package com.oxygen.front.commodity.service;

import com.oxygen.front.commodity.dao.OrderMapper;
import com.oxygen.front.commodity.model.CommodityTypeExample;
import com.oxygen.front.commodity.model.Order;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class OrderServicelmpl implements OrderService {
    @Autowired
    private OrderMapper orderMapper;

    @Override
    public List<Order> selectOrder(Order order) {
        String id = order.getUser().getId();
        return orderMapper.select(id);
    }
}
