package com.oxygen.front.company.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller("CompanyController")
@RequestMapping("/company")
public class CompanyController {
    /**
     * 公司简介
     *
     * @return 页面
     */
    @RequestMapping(value = "/about", method = RequestMethod.GET)
    public String about() {

        return "front/about";
    }

    /**
     * careers
     * 公司招聘
     *
     * @return 页面
     */
    @RequestMapping(value = "/careers", method = RequestMethod.GET)
    public String careers() {

        return "front/careers";
    }


    /**
     * contact
     * 公司活动
     *
     * @return 页面
     */
    @RequestMapping(value = "/contact", method = RequestMethod.GET)
    public String contact() {

        return "front/contact";
    }

    /**
     * register
     * 公司注册
     *
     * @return 页面
     */
    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String register() {

        return "front/register";
    }


}
