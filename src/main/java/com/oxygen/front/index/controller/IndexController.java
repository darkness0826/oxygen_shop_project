package com.oxygen.front.index.controller;

import com.oxygen.front.commodity.model.Commodity;
import com.oxygen.front.commodity.service.CommodityService;
import com.oxygen.front.user.model.User;
import com.oxygen.utils.entity.SessionKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import java.text.AttributedCharacterIterator;
import java.util.List;

@Controller("frontIndexController")
@RequestMapping("/index")
public class IndexController {
    @Autowired
    CommodityService commodityService;

    /**
     * 显示首页
     *
     * @return 页面显示
     */
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String index(Model model, Commodity commodity) {
        commodity.setName("");
        List<Commodity> commoditys = commodityService.selectCommodityAll(commodity);
        model.addAttribute("commodity", commoditys);
        return "front/index";
    }

}
