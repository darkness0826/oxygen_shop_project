package com.oxygen.front.user.controller;

import com.oxygen.front.user.model.User;
import com.oxygen.front.user.service.UserService;
import com.oxygen.utils.entity.SessionKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;


@Controller
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    /**
     * 用户登录显示页
     *
     * @return 页面显示
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginForm() {

        return "front/user/login";
    }

    /**
     * 用户注册显示页
     *
     * @return 页面显示
     */
    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String register() {

        return "front/user/register";
    }


    /**
     * 用户的个人中心
     */
    @RequestMapping(value = "/personal", method = RequestMethod.GET)
    public String personal(Model model, HttpSession session) {
        User user = (User) session.getAttribute(SessionKey.LOGIN_USER_INFO);

        User user1 =  userService.getUser(user);
        model.addAttribute("personal", user1);
        return "front/user/uc-account";
    }
    /**
     * 用户的订单详情
     */
    @RequestMapping(value = "/order", method = RequestMethod.GET)
    public String order(Model model, HttpSession session) {
        User user = (User) session.getAttribute(SessionKey.LOGIN_USER_INFO);

        User user1 =  userService.getUser(user);
        model.addAttribute("personal", user1);
        return "front/user/uc-order";
    }

    /**
     * 用户的收藏商品
     */
    @RequestMapping(value = "/fav", method = RequestMethod.GET)
    public String fav(Model model, HttpSession session) {
        User user = (User) session.getAttribute(SessionKey.LOGIN_USER_INFO);

        User user1 =  userService.getUser(user);
        model.addAttribute("personal", user1);
        return "front/user/uc-fav";
    }
    /**
     * 用户的收藏店铺
     */
    @RequestMapping(value = "/favshop", method = RequestMethod.GET)
    public String favshop(Model model, HttpSession session) {
        User user = (User) session.getAttribute(SessionKey.LOGIN_USER_INFO);

        User user1 =  userService.getUser(user);
        model.addAttribute("personal", user1);
        return "front/user/uc-fav-shop";
    }
    /**
     * 用户的浏览足迹
     */
    @RequestMapping(value = "/footprint", method = RequestMethod.GET)
    public String footprint(Model model, HttpSession session) {
        User user = (User) session.getAttribute(SessionKey.LOGIN_USER_INFO);

        User user1 =  userService.getUser(user);
        model.addAttribute("personal", user1);
        return "front/user/uc-footprint";
    }
    /**
     * 用户的退货
     */
    @RequestMapping(value = "/refund", method = RequestMethod.GET)
    public String refund(Model model, HttpSession session) {
        User user = (User) session.getAttribute(SessionKey.LOGIN_USER_INFO);

        User user1 =  userService.getUser(user);
        model.addAttribute("personal", user1);
        return "front/user/uc-refund";
    }
    /**
     * 用户賬戶安全
     */
    @RequestMapping(value = "/safe", method = RequestMethod.GET)
    public String safe(Model model, HttpSession session) {
        User user = (User) session.getAttribute(SessionKey.LOGIN_USER_INFO);

        User user1 =  userService.getUser(user);
        model.addAttribute("personal", user1);
        return "front/user/uc-safe";
    }
    /**
     * 用户收货地址
     */
    @RequestMapping(value = "/address", method = RequestMethod.GET)
    public String address(Model model, HttpSession session) {
        User user = (User) session.getAttribute(SessionKey.LOGIN_USER_INFO);

        User user1 =  userService.getUser(user);
        model.addAttribute("personal", user1);
        return "front/user/uc-address";
    }
    /**
     * 用户的商品评价
     */
    @RequestMapping(value = "/evaluate", method = RequestMethod.GET)
    public String evaluate(Model model, HttpSession session) {
        User user = (User) session.getAttribute(SessionKey.LOGIN_USER_INFO);

        User user1 =  userService.getUser(user);
        model.addAttribute("personal", user1);
        return "front/user/uc-evaluate";
    }
    /**
     * 用户的站内信息
     */
    @RequestMapping(value = "/msg", method = RequestMethod.GET)
    public String msg(Model model, HttpSession session) {
        User user = (User) session.getAttribute(SessionKey.LOGIN_USER_INFO);

        User user1 =  userService.getUser(user);
        model.addAttribute("personal", user1);
        return "front/user/uc-msg";
    }

}
