package com.oxygen.front.user.service;

import com.oxygen.front.user.model.User;
import com.oxygen.utils.entity.JsonResponse;

import javax.servlet.http.HttpSession;


public interface UserService {
    /**
     * 获得用户
     *
     * @return 单个用户实体
     */
    User getUser(User user);

    /**
     * 查询用户
     *
     * @return 单个用户实体
     */
    String select(User user);

    /**
     * 登录操作
     *
     * @return 登录状态信息
     */
    JsonResponse login(User user, HttpSession session);

    /**
     * 添加操作
     *
     * @return 受影响行数
     */
    JsonResponse addUser(User user);

    /**
     * 修改操作
     *
     * @return 受影响行数
     */
    JsonResponse alterUser(User user,HttpSession session);

    JsonResponse error();
}
