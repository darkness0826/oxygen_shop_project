package com.oxygen.front.user.service;


import com.oxygen.front.user.dao.UserMapper;
import com.oxygen.front.user.model.User;
import com.oxygen.front.user.model.UserExample;
import com.oxygen.utils.entity.JsonResponse;
import com.oxygen.utils.entity.SessionKey;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserMapper userMapper;


    /**
     * 登录操作
     *
     * @param user user 用户实体
     * @return 返回信息
     */
    @Override
    public User getUser(User user) {

        UserExample userExample = new UserExample();
        UserExample.Criteria criteria = userExample.createCriteria();
        criteria.andAccountEqualTo(user.getAccount()).andPasswordEqualTo(user.getPassword());
        List<User> users = userMapper.selectByExample(userExample);
        if (users != null && users.size() != 0) {
            return users.get(0);
        }
        return null;


    }

    @Override
    public String select(User user) {
        UserExample userExample = new UserExample();
        UserExample.Criteria criteria = userExample.createCriteria();
        criteria.andAccountEqualTo(user.getAccount());
        List<User> users = userMapper.selectByExample(userExample);
        if (users.size() > 0) {
            return "a";//用户名已存在
        }
        return "b";
    }

    /**
     * 登录成功存储session
     *
     * @param user
     * @param session
     * @return
     */
    @Override
    public JsonResponse login(User user, HttpSession session) {
        JsonResponse<Map<String, Object>> jsonResponse = new JsonResponse<>();
        User theUser = getUser(user);
        if (theUser != null) {
            session.setAttribute(SessionKey.LOGIN_USER_INFO, theUser);
            jsonResponse.setError(false);
            jsonResponse.setMsg("登录成功");
            Map<String, Object> dataMap = new HashMap<>();
            dataMap.put("user", theUser);
            dataMap.put("markUrl", session.getAttribute("markUrl"));
            jsonResponse.setData(dataMap);

        } else {
            jsonResponse.setMsg("登录失败，失败原因：用户名或者密码错误");
        }
        return jsonResponse;
    }

    /**
     * 添加用户
     *
     * @param user
     * @return
     */
    @Override
    public JsonResponse addUser(User user) {
        JsonResponse<Map<String, Object>> jsonResponse = new JsonResponse<>();


        int res = userMapper.insert(user);
        if (res == 1) {
            jsonResponse.setError(false);
            jsonResponse.setMsg("注册成功！");

        } else {
            jsonResponse.setMsg("注册失败！");
        }
        return jsonResponse;


    }

    /**
     * 修改用户资料
     *
     * @param user
     * @return
     */
    @Override
    public JsonResponse alterUser(User user, HttpSession session) {
        JsonResponse<Map<String, Object>> jsonResponse = new JsonResponse<>();
        int res = userMapper.updateByPrimaryKeySelective(user);


        if (res == 1) {
            jsonResponse.setError(false);
            jsonResponse.setMsg("修改成功！");

        } else {
            jsonResponse.setMsg("修改失败！");
        }
        return jsonResponse;

    }

    @Override
    public JsonResponse error() {
        JsonResponse<Map<String, Object>> jsonResponse = new JsonResponse<>();
        jsonResponse.setError(true);
        jsonResponse.setMsg("注册失败");
        return jsonResponse;
    }

}
