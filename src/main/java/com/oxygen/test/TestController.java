package com.oxygen.test;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 用于后台测试接口使用
 */
@Controller
@RequestMapping("/test")
public class TestController {
    @RequestMapping("/page")
    public String test(){
        return "test/page";
    }


    @RequestMapping("/commodity")
    public String testCommodity(){
        return "test/commodity";
    }
}
