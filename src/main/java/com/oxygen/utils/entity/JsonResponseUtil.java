package com.oxygen.utils.entity;


/**
 * 处理最终的返回json的统一格式工具类
 */
public class JsonResponseUtil {
    public static <T> JsonResponse<T> success() {
        return success(null);
    }

    public static <T> JsonResponse<T> success(T data) {
        return success("操作成功", data);
    }

    public static <T> JsonResponse<T> success(String msg, T data) {
        JsonResponse<T> jsonResponse = new JsonResponse<>();
        jsonResponse.setError(false);
        jsonResponse.setMsg(msg);
        jsonResponse.setData(data);
        return jsonResponse;
    }

    public static <T> JsonResponse<T> error(String msg) {
        JsonResponse<T> jsonResponse = new JsonResponse<>();
        jsonResponse.setError(true);
        jsonResponse.setMsg(msg);
        return jsonResponse;
    }

    public static <T> JsonResponse<T> error() {
        return error("操作失败");
    }
}
