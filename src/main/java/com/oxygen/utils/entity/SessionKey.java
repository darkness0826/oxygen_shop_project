package com.oxygen.utils.entity;

/**
 * session存储中的一些常量
 */
public class SessionKey {
    public static final String LOGIN_USER_INFO = "login_user_info"; //用户登录信息字段
    public static final String LOGIN_ADMIN_INFO = "login_admin_info"; //管理员登录信息字段
}
