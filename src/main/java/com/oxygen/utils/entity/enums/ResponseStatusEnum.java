package com.oxygen.utils.entity.enums;

public enum ResponseStatusEnum {
    UNKONW_ERROR(-1, "未知错误"),
    SUCCESS(0, "成功"),
    INSERT_ERROR(101, "插入错误"),
    UPDATE_ERROR(102, "更新错误"),
    DELETE_ERROR(103, "删除错误"),;
    private Integer statusCode;
    private String msg;

    ResponseStatusEnum(Integer statusCode, String msg) {
        this.msg = msg;
        this.statusCode = statusCode;
    }

    public String getMsg() {
        return msg;
    }

    public Integer getStatusCode() {
        return statusCode;
    }
}
