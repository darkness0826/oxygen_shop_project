package com.oxygen.utils.interceptor;

import com.oxygen.utils.entity.SessionKey;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * 后台登录权限过滤器
 */
public class BackLoginInterceptor extends HandlerInterceptorAdapter {
    /**
     * 待过滤URL
     */
    private List<String> exceptUrls;

    public List<String> getExceptUrls() {
        return exceptUrls;
    }

    public void setExceptUrls(List<String> exceptUrls) {
        this.exceptUrls = exceptUrls;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        System.out.println(System.getProperty("webapp.root"));
//        System.out.println(System.getProperty("user.dir"));
        HttpSession session = request.getSession();
        String url = request.getRequestURI();
        String loginUrl = "/back/admin/login";
        String indexUrl = "/back/index/index";
        //如果为前台页面，则进行放行
        if (!url.contains("back")) {
            return true;
        }
        //放行exceptUrls中配置的url
        if (exceptUrls != null) {
            for (String oneUrl : exceptUrls) {
                if (url.endsWith("/**")) {
                    if (url.startsWith(oneUrl.substring(0, url.length() - 3))) {
                        return true;
                    }
                } else if (url.startsWith(oneUrl)) {
                    return true;
                }
            }
        }

        if (session.getAttribute(SessionKey.LOGIN_ADMIN_INFO) == null) {
            if (url.equals(request.getContextPath() + loginUrl)) {
                return true;
            } else {
                request.getSession().setAttribute("markUrl", url);
                response.sendRedirect(request.getContextPath() + loginUrl);
                return false;
            }

        } else {
            if (url.equals(loginUrl)) {
                System.out.println(loginUrl);
                response.sendRedirect(indexUrl);
                return false;
            } else {
                return true;
            }
        }

    }
}

