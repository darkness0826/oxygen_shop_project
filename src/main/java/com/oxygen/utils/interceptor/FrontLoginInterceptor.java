package com.oxygen.utils.interceptor;

import com.oxygen.utils.entity.SessionKey;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * 前台登录权限拦截器
 */
public class FrontLoginInterceptor extends HandlerInterceptorAdapter {
    /**
     * 待过滤URL
     */
    private List<String> exceptUrls;

    public List<String> getExceptUrls() {
        return exceptUrls;
    }

    public void setExceptUrls(List<String> exceptUrls) {
        this.exceptUrls = exceptUrls;
    }


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession();
        String url = request.getRequestURI();
        String loginUrl = "/user/login";
        String indexUrl = "/user/index";
        //如果为后台页面，则进行放行
        if (url.contains("back")) {
            return true;
        }
        //放行exceptUrls中配置的url
        if (exceptUrls != null) {
            for (String oneUrl : exceptUrls) {
                if (url.endsWith("/**")) {
                    if (url.startsWith(oneUrl.substring(0, url.length() - 3))) {
                        return true;
                    }
                } else if (url.startsWith(oneUrl)) {
                    return true;
                }
            }
        }

        if (session.getAttribute(SessionKey.LOGIN_USER_INFO) == null) {
            if (url.equals(request.getContextPath() + loginUrl)) {
                return true;
            } else {
                request.getSession().setAttribute("markUrl", url);
                response.sendRedirect(request.getContextPath() + loginUrl);
                return false;
            }

        } else {
            if (url.equals(loginUrl)) {
                System.out.println(loginUrl);
                response.sendRedirect(indexUrl);
                return false;
            } else {
                return true;
            }
        }

    }


}
