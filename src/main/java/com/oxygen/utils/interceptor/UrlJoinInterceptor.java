package com.oxygen.utils.interceptor;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 用于动态拼接URL的拦截器
 */
public class UrlJoinInterceptor extends HandlerInterceptorAdapter {
    /**
     * 用于动态拼接URL地址
     * @param request 请求
     * @param response 相应
     * @param handler 处理器
     * @param modelAndView 模型和视图
     * @throws Exception 各种异常
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        String virtualPath = request.getContextPath(); //虚拟路径
        String rootPath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + virtualPath; //根路径
        if(modelAndView!=null){
            modelAndView.addObject("ROOT_PATH",rootPath);
        }


    }
}
