<%--
  Created by IntelliJ IDEA.
  User: WangShiPeng
  Date: 2018/6/27
  Time: 21:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>OXYGEN后台登陆页面</title>
    <jsp:include page="/static/back/common/links.jsp" flush="true"/>
</head>

<body class="bg-dark">
<div class="container">
    <div class="card card-login mx-auto mt-5">
        <div class="card-header">OXYGEN后台登录</div>
        <div class="card-body">
            <form>
                <div class="form-group">
                    <label for="username">用户名</label>
                    <input class="form-control" id="username" type="email" name="account" aria-describedby="emailHelp"
                           placeholder="Enter email">
                </div>
                <div class="form-group">
                    <label for="password">密码</label>
                    <input class="form-control" id="password" type="password" name="password" placeholder="Password">
                </div>
                <%--<div class="form-group">--%>
                <%--<div class="form-check">--%>
                <%--<label class="form-check-label">--%>
                <%--<input class="form-check-input" type="checkbox"> 记住密码</label>--%>
                <%--</div>--%>
                <%--</div>--%>
                <a class="btn btn-primary btn-block" href="javascript:;" id="loginBtn">登陆</a>
            </form>
        </div>
    </div>
</div>
<jsp:include page="/static/back/common/scripts.jsp" flush="true"/>
</body>
</html>
<script>
    $(function () {
        $("#loginBtn").click(function () {
            //登录操作
            let url = "${ROOT_PATH}" + "/back/admin/login";
            let data = {
                "account": $("#username").val(),
                "password": $("#password").val()
            };
            if ($("#username").val().length === 0) {
                alert("用户名不能为空");
                return;

            } else if ($("#password").val().length === 0) {
                alert("密码不能为空");
                return;
            }
            $.post(url, data, function (res) {
                if (res.error === false) {
                    alert("登录成功~");
                    if (res.data.markUrl) {
                        location.href = res.data.markUrl;
                    } else {
                        location.href = "${ROOT_PATH}" + "/back/index/index";
                    }
                } else {
                    alert("登录失败");
                }
            }, "json");


        });
    });
</script>
