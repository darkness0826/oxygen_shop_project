<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>管理员管理页</title>
    <jsp:include page="/static/back/common/links.jsp" flush="true"/>
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
<!-- Navigation-->
<jsp:include page="/static/back/common/nav.jsp" flush="true"/>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">OxyGen</a>
            </li>
            <li class="breadcrumb-item active">管理员管理</li>

        </ol>
        <!-- Example DataTables Card-->
        <div class="card mb-3">
            <div class="card-header">
                <i class="fa fa-table"></i> 管理员管理
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <button class="btn btn-secondary" style="margin: 0px 15px 16px;" data-toggle="modal" id="add_btn">
                        新增
                    </button>
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>id</th>
                            <th>用户名</th>
                            <th>密码</th>
                            <th>手机号</th>
                            <th>昵称</th>
                            <th>年龄</th>
                            <th>描述</th>
                            <th>角色</th>
                            <th>创建时间</th>
                            <th>更新时间</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                        <tbody>

                        <c:forEach items="${adminList}" var="item">
                            <tr>
                                <td data-attr="id">${item.id}</td>
                                <td data-attr="account">${item.account}</td>
                                <td data-attr="password">${item.password}</td>
                                <td data-attr="phone">${item.phone}</td>
                                <td data-attr="name">${item.name}</td>
                                <td data-attr="age">${item.age}</td>
                                <td data-attr="description">${item.description}</td>
                                <td data-attr="role">${item.role.name}</td>
                                <td><fmt:formatDate value="${item.createTime}" pattern="yyyy/MM/dd  HH:mm:ss"/></td>
                                <td><fmt:formatDate value="${item.updateTime}" pattern="yyyy/MM/dd  HH:mm:ss"/></td>
                                <td>
                                    <a href="javascript:;" class="btn btn-primary update_btn">修改</a>
                                    <a href="javascript:;" class="btn btn-danger del_btn">删除</a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>

                    </table>
                </div>
            </div>
            <div class="card-footer small text-muted">Updated By OxyGen</div>
        </div>
    </div>
    <jsp:include page="/static/back/common/scripts.jsp" flush="true"/>
    <jsp:include page="/static/back/common/footer.jsp" flush="true"/>
</div>
<!-- 添加模态框（Modal） -->
<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="formModalLabel">新增管理员</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <form id="the_form">
                    <div class="form-group row">
                        <label for="account" class="col-sm-2 col-form-label">用户名</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" id="account" name="account"
                                   placeholder="用户名">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-sm-2 col-form-label">密码</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" id="password" name="password"
                                   placeholder="密码">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="phone" class="col-sm-2 col-form-label">手机号</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" id="phone" name="phone" placeholder="手机号">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="age" class="col-sm-2 col-form-label">年龄</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" id="age" name="age" placeholder="年龄">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label">昵称</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="name" name="name" placeholder="昵称">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="desc" class="col-sm-2 col-form-label">描述</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="desc" name="description" placeholder="描述">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="role" class="col-sm-2 col-form-label">角色</label>
                        <div class="col-sm-10">
                            <select class="custom-select my-1 mr-sm-2" id="role" name="roleId">

                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="add_form_btn">提交</button>
                <button type="button" class="btn btn-primary" id="update_form_btn">保存</button>
        </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>

</body>

</html>
<script>
    $(function () {
        let is_first = true; //第一次才进行ajax加载角色选择
        //打开添加界面
        $("#add_btn").click(function () {
            $('#formModal').modal('show');
            getRoleList();
            $("#id").remove();
            $("#account").val("");
            $("#password").val("");
            $("#phone").val("");
            $("#name").val("");
            $("#age").val("");
            $("#desc").val("");
            $("#add_form_btn").show();
            $("#update_form_btn").hide();
            $("#formModalLabel").html("新增管理员");
            $("#role option").each(function (i,e) {
                if(i===0){
                    $(this).prop("selected",true);
                }
                $(this).prop("selected",false);

            });

        });
        //请求角色列表
        function getRoleList(){
            if (is_first) {
                let url = "${ROOT_PATH}" + "/back/role/selectAll";
                $.get(url, function (res) {
                    if (res.error === false) {
                        res.data.forEach(function (e, i) {
                            $("#role").append(`<option value="\${e.id}">\${e.name}</option>`);
                        });
                    } else {
                        console.log("数据请求失败");
                    }
                });
                is_first = false;
            }
        }
        //提交添加操作
        $("#add_form_btn").click(function () {
            if(checkForm()===-1){
                return;
            }
            let url = "${ROOT_PATH}" + "/back/admin/add";
            $.post(url, $("#the_form").serialize(), function (res) {
                if(res.error===false){
                    alert("添加成功！");
                    $('#the_form').modal('hide');
                    history.go();
                }
            });
        });



        //打开更新界面
        $(".update_btn").click(function () {
            getRoleList();
            $('#formModal').modal('show');
            if($("#id").length===0){
                let id = $(this).parent().siblings("[data-attr='id']").html();
                $("#the_form").append(`<input name="id" value="\${id}" id="id" type="hidden" />`);
            }
            $("#account").val($(this).parent().siblings("[data-attr='account']").html());
            $("#password").val($(this).parent().siblings("[data-attr='password']").html());
            $("#phone").val($(this).parent().siblings("[data-attr='phone']").html());
            $("#name").val($(this).parent().siblings("[data-attr='name']").html());
            $("#age").val($(this).parent().siblings("[data-attr='age']").html());
            $("#desc").val($(this).parent().siblings("[data-attr='description']").html());
            $("#add_form_btn").hide();
            $("#update_form_btn").show();
            $("#formModalLabel").html("修改管理员");
            let self = $(this);
            $("#role option").each(function (i,e) {
                if($(this).html()===self.parent().siblings("[data-attr=role]").html()){
                    $(this).prop("selected",true);
                }
            });

        });
        //提交更新操作
        $("#update_form_btn").click(function () {
            if(checkForm()===-1){
                return;
            }
            let url = "${ROOT_PATH}" + "/back/admin/update";
            $.post(url, $("#the_form").serialize(), function (res) {
                if(res.error===false){
                    alert("修改成功！");
                    $('#the_form').modal('hide');
                    history.go();
                }
            });
        });
        //删除操作
        $(".del_btn").click(function () {
            let id = $(this).parent().siblings("[data-attr='id']").html();
            let url = "${ROOT_PATH}" + "/back/admin/delete";
            $.post(url,{id:id},function (res) {
                if(res.error===false){
                    alert("删除成功！");
                    $('#the_form').modal('hide');
                    history.go();
                }
            });
        });


        //检查表单
        function checkForm() {
            let account = $("#account").val();
            let password = $("#password").val();
            let phone = $("#phone").val();
            let age = $("#age").val();
            if (account.length === 0 || password.length === 0) {
                alert("用户名或者密码不能为空");
                return -1;
            }
            if (phone.length !== 11) {
                alert("手机号为11位");
                return -1;
            }
            if (isNaN(parseInt(age)) || parseInt(age) < 0||parseInt(age)>120) {
                alert("年龄不符合规则");
                return -1;
            }
        }

    });
</script>
