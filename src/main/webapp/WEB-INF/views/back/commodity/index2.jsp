<%--商品管理页--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>商品管理页</title>
    <jsp:include page="/static/back/common/links.jsp" flush="true"/>
    <style>
        .input-group-text {
            background-color: #FFFFFF;
        }

        .upload {
            hight: 500px;
        !important width: 200 px;
        !important
        }
    </style>
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
<!-- Navigation-->
<jsp:include page="/static/back/common/nav.jsp" flush="true"/>

<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">OxyGen</a>
            </li>
            <li class="breadcrumb-item active">商品管理</li>
        </ol>
        <!-- Example DataTables Card-->
        <div class="card mb-3">
            <div class="card-header">
                <i class="fa fa-table"></i> 商品信息
                <button style="margin-left: 84%" type="button" class="btn btn-outline-primary" data-toggle="modal"
                        data-target="#myModal">新增商品
                </button>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>商品ID</th>
                            <th>商品名称</th>
                            <th>商品属性与值</th>
                            <th>商品单价</th>
                            <th>销量</th>
                            <th>上下架状态</th>
                            <th>编辑</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>商品ID</th>
                            <th>商品名称</th>
                            <th>商品属性与值</th>
                            <th>商品单价</th>
                            <th>销量</th>
                            <th>上下架状态</th>
                            <th>编辑</th>
                        </tr>
                        </tfoot>
                        <tbody id="commodity_val">

                        </tbody>
                    </table>
                    <%--新增的模态框--%>
                    <!-- 模态框 -->
                    <div class="modal fade" id="myModal">
                        <div class="modal-dialog">
                            <div class="modal-content">

                                <!-- 模态框头部 -->
                                <div class="modal-header">
                                    <h4 class="modal-title">商品新增</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>

                                <!-- 模态框主体 -->
                                <div class="modal-body">
                                    <form id="my_form">
                                        <div class="upload">
                                            <label for="file" style="color: #585686">
                                                图片上传
                                            </label>
                                            <input name="file" type="file" id="file" style="display: none"
                                                   multiple="multiple">
                                        </div>

                                        <div class="form-group">
                                            <div class="input-group mb-3">
                                                <label for="name" class="col-sm-3 control-label">商品名</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" id="name" placeholder="商品名">
                                                </div>
                                            </div>
                                            <div class="input-group mb-3">
                                                <label for="price" class="col-sm-3 control-label">单价</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" id="price"
                                                           placeholder="单价"/>
                                                </div>
                                            </div>
                                            <div class="input-group mb-3 commodity">
                                                <div class="input-group mb-3">
                          <span class="input-group-text col-sm-12">商品属性
                            <div style="margin-left: 64%;">

                                <button id="addBtn" data-toggle="tooltip" data-placement="top" title="新增商品的属性、样式"
                                        type="button" class="btn btn-outline-primary" id="addBtn">
                                  <span class="fa fa-fw fa-plus"></span>
                                </button>
                                <button id="delBtn" data-toggle="tooltip" data-placement="top" title="删除刚增加的属性、样式"
                                        type="button" class="btn btn-outline-danger" id="delBtn">
                                  <span class="fa fa-fw fa-minus"></span>
                                </button>

                            </div>
                          </span>
                                                </div>
                                                <div class="input-group mb-3 commodity_commons">
                                                    <%--<div class="input-group-prepend">--%>

                                                    <%--</div>--%>
                                                    <input type="text" class="form-control commodity_type"
                                                           placeholder="商品属性">
                                                    <input type="text" class="form-control commodity_type_parameter"
                                                           placeholder="商品属性值">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <!-- 模态框底部 -->
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-outline-primary" id="submit_btn">提交</button>
                                    <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">关闭
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>
    </div>
    <jsp:include page="/static/back/common/scripts.jsp" flush="true"/>
    <jsp:include page="/static/back/common/footer.jsp" flush="true"/>
</div>
</body>
<script>
    $(function () {

        /*
        *查询操作
        * */
        $.ajax({
            url: "${ROOT_PATH}/back/commodity/selectCommodity",
            dataType: "json",
            type: "POST",
            contentType: "application/json",
            success: function (res) {
                var str = "";
                if (res["data"] != null && res != "") {
                    res["data"]["backCommodities"].forEach(function (v, c) {
                        str += "<tr>\n" +
                            "                  <td>" + v.id + "</td>\n" +
                            "                  <td>" + v.name + "</td>\n" +
                            "\"                  <td>  ";

                        v["backCommodityTypeList"].forEach(function (e, i) {
                            str +=
                                e.frontCommodityType + "：" + e.frontCommodityTypeParameter + "<br/>";
                        });
                        str += " </td> " +
                            "                 <td>" + v.price + "</td>\n" +
                            "                  <td>" + v.commoditySalesNum + "</td>\n" +
                            "                  <td>" + v.state + "</td>\n" +
                            "<td>\n" +
                            "                  <button type=\"button\" class=\"btn btn-outline-primary\">修改</button>\n" +
                            "                  <button type=\"button\" class=\"btn btn-outline-danger del\">删除</button>\n" +
                            "                </td>" +
                            "                </tr>";
                    });
                    $("#commodity_val").html(str);
                }
                console.log(res);
                console.log(res["msg"]);
                console.log(res["data"]);
                // alert(res);

            }

        });
        /*
        * 删除商品
        * */
        $(document).on("click", ".del", function () {
            var id = $(this).parents('tr').find('td').eq(0).text();//点击的这条id
            var commodity = {};//商品的对象
            commodity.id = id;
            commodity.isDel = 1;
            var dele = confirm("是否删除？");
            if (dele) {
                $.ajax({
                    url: "${ROOT_PATH}/back/commodity/delCommodity",
                    data: JSON.stringify(commodity),
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json",
                    success: function (res) {
                        if (res["error"] === false) {
                            alert(res["msg"]);
                            history.go();
                        }
                    }

                });
            }

        });


        /*
          上传图片
         */
        var imgUrl = "";
        $("#file").change(function () {
            var formData = new FormData();
            // var file = $('#file');
            formData.append('file', $('#file')[0].files[0]);

            // console.log(file);
            // alert(file);
            $.ajax({
                url: "${ROOT_PATH}/back/commodity/upload",
                data: formData,
                type: "POST",
                contentType: false,
                processData: false,
                success: function (res) {
                    // console.log(res["error"]);
                    // console.log(res["msg"]);
                    // console.log(res["data"]);
                    if (res["error"] === false) {
                        $.each(res, function (i, item) {
                            imgUrl = item.imgUrl;
                            // console.log(imgUrl);

                        });
                        alert(res["msg"]);
                    }

                }

            });
        });

        /*
        添加商品的属性与值
         */
        $("#addBtn").click(function () {
            // alert(1);
            var str = "<div class=\"input-group mb-3 commodity_commons\">\n" +
                "                            <input type=\"text\" class=\"form-control commodity_type\" placeholder=\"商品所拥有的属性\">\n" +
                "                            <input type=\"text\" class=\"form-control commodity_type_parameter\" placeholder=\"商品属性值\">\n" +
                "                          </div>";

            $(".commodity").append(str);
        });
        /*
         删除商品的属性与值
        */
        $("#delBtn").click(function () {
            // alert(2);
            var len = $(".commodity_commons").length;
            if (len > 1) {
                $(".commodity").children("div:last-child").remove();
            } else {
                alert("至少要有一个属性");
            }

        });

        $("#submit_btn").click(function () {

            // alert($("#name").val() + $(".commodity_type_parameter").val() + $(".commodity_type").val() + $("#price").val());
            if (($("#name").val() && $(".commodity_type_parameter").val() && $(".commodity_type").val() && $("#price").val() && imgUrl) != null && ($("#name").val() && $(".commodity_type_parameter").val() && $(".commodity_type").val() && $("#price").val() && imgUrl) != "") {
                var commodity = {};//商品的对象
                var commodityTypeArr = [];//商品属性与值存放的数组

                var btns = $('.commodity_commons');//遍历这个div的class
                btns.each(function (i, e) {
                    var commodityType = {};//商品样式的对象
                    commodityType.frontCommodityTypeParameter = $(this).find(".commodity_type_parameter").val();//商品属性值
                    commodityType.frontCommodityType = $(this).find(".commodity_type").val();//商品所拥有的属性
                    commodityTypeArr.push(commodityType);//把每次获取的值，放到这个数组里

                }, false);

                commodity.frontCommodityThumbnail = imgUrl;//商品图片的url
                commodity.name = $("#name").val();//商品的名字
                commodity.price = $("#price").val();//商品的单间
                commodity.backCommodityTypeList = commodityTypeArr;//把商品属性与值存放的数组放到这个对象

                // alert(commodity);

                $.ajax({
                    url: "${ROOT_PATH}/back/commodity/addCommodity",
                    data: JSON.stringify(commodity),
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json",
                    success: function (res) {
                        // console.log(res["error"]);
                        // console.log(res["msg"]);
                        // console.log(res["data"]);
                        if (res["error"] === false) {
                            alert(res["msg"]);
                            history.go();
                        }

                    }

                });
            } else {
                alert("内容或上传图片都不能为空！");
            }
        });

        $('[data-toggle="tooltip"]').tooltip();
    });


</script>
</html>
