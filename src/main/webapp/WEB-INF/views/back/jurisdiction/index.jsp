<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>权限管理页</title>
    <jsp:include page="/static/back/common/links.jsp" flush="true"/>
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
<!-- Navigation-->
<jsp:include page="/static/back/common/nav.jsp" flush="true"/>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">OxyGen</a>
            </li>
            <li class="breadcrumb-item active">权限管理</li>
        </ol>
        <!-- Example DataTables Card-->
        <div class="card mb-3">
            <div class="card-header">
                <i class="fa fa-table"></i> 权限管理
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <button class="btn btn-secondary" id="add_btn">新增</button>
                </div>
                <table id="table" class="table-bordered">
                </table>
            </div>
            <div class="card-footer small text-muted">Updated By OxyGen</div>
        </div>
    </div>
    <jsp:include page="/static/back/common/scripts.jsp" flush="true"/>
    <jsp:include page="/static/back/common/footer.jsp" flush="true"/>
</div>
<!-- 模态框（Modal） -->
<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="formModalLabel">修改权限</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <form id="the_form">
                    <div class="form-group row">
                        <label for="title" class="col-sm-2 col-form-label">名称</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="title" name="title"
                                   placeholder="权限名">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="rule" class="col-sm-2 col-form-label">规则</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="rule" name="rule"
                                   placeholder="权限规则">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="description" class="col-sm-2 col-form-label">描述</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="description" name="description"
                                   placeholder="权限描述">
                        </div>
                    </div>


                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="add_form_btn">提交</button>
                        <button type="button" class="btn btn-primary" id="update_form_btn">保存</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal -->

    </div>

</body>

</html>
<script>
    $(function () {
        //表格生成
        let url = "${ROOT_PATH}" + "/back/jurisdiction/selectAll";

        //初始化数据
        $('#table').bootstrapTable({
            url: url,
            search: true,
            striped: true,
            clickToSelect: true,
            uniqueId: "id",
            sortable: true,
            sortOrder: "asc",
            sidePagination: "client",
            showAllColumns: true,
            showRefresh: true,
            pagination: true,
            pageNumber: 1,
            pageSize: 10,
            queryParamsType: "",
            columns: [
                {"field": "id", "title": "id"},
                {"field": "rule", "title": "权限规则"},
                {"field": "title", "title": "权限名称"},
                {"field": "description", "title": "权限描述"},
                {"field": "createTime", "title": "创建时间"},
                {"field": "updateTime", "title": "更新时间"},
                {
                    "field": "id",
                    "title": "操作",
                    width: 120,
                    align: "center",
                    valign: "middle",
                    formatter: actionFormatter
                }
            ]
        });


        //操作按钮格式化
        function actionFormatter(value, row, index) {
            let id = value;
            let result = "";
            result += `<a href='javascript:;' class='btn btn-xs blue update_btn'  title='编辑' data-id="\${id}"><span class='glyphicon glyphicon-pencil'></span></a>`;
            result += `<a href='javascript:;' class='btn btn-xs red del_btn'  title='删除'   data-id="\${id}"><span class='glyphicon glyphicon-remove'></span></a>`;
            return result;
        }

        //打开添加界面
        $("#add_btn").click(function () {
            $('#formModal').modal('show');
            $("#id").remove();
            $("#title").val("");
            $("#rule").val("");
            $("#description").val("");
            $("#add_form_btn").show();
            $("#update_form_btn").hide();
            $("#formModalLabel").html("新增权限");

        });

        //提交添加操作
        $("#add_form_btn").click(function () {
            if ($("#title").val().length === 0) {
                alert("名字不能为空啊");
                return;
            }
            if ($("#rule").val().length === 0) {
                alert("规则不能为空啊");
                return;
            }
            if ($("#description").val().length === 0) {
                alert("描述不能为空啊");
                return;
            }

            let url = "${ROOT_PATH}" + "/back/jurisdiction/add";
            $.ajax({
                type: "POST",
                url: url,
                data: $("#the_form").serialize(),
                dataType: "json",
                success: function (res) {
                    if (res.error === false) {
                        alert("添加成功");
                        $('#formModal').modal('hide');
                        $("#table").bootstrapTable('refresh', {silent: true});
                    } else {
                        console.log(res);
                    }
                }
            });
        });


        //点击更新按钮，触发打开更新界面
        $(document).on("click", ".update_btn", function () {
            $("#formModalLabel").html("修改权限");
            $("#add_form_btn").hide();
            $("#update_form_btn").show();
            let id = $(this).attr("data-id");
            $("#id").val(id);
            if ($("#id").length === 0) {
                $("#the_form").append(`<input name="id" value="\${id}" id="id" type="hidden" />`);
            }
            let row = $("#table").bootstrapTable('getRowByUniqueId', id);
            $('#formModal').modal('show');
            $("#title").val(row.title);
            $("#rule").val(row.rule);
            $("#description").val(row.description);

        });

        //提交更新操作
        $("#update_form_btn").click(function () {
            if ($("#id").val() === undefined || $("#id").val().length === 0) {
                console.log("表单id为空");
                return;
            }
            if ($("#title").val().length === 0) {
                alert("名字不能为空啊");
                return;
            }
            if ($("#rule").val().length === 0) {
                alert("规则不能为空啊");
                return;
            }
            if ($("#description").val().length === 0) {
                alert("描述不能为空啊");
                return;
            }

            let url = "${ROOT_PATH}" + "/back/jurisdiction/update";
            $.ajax({
                type: "POST",
                url: url,
                data: $("#the_form").serialize(),
                dataType: "json",
                success: function (res) {
                    if (res.error === false) {
                        alert("更新成功");
                        $('#formModal').modal('hide');
                        $("#table").bootstrapTable('refresh', {silent: true});
                    } else {
                        console.log(res);
                    }
                }
            });
        });


        //删除操作
        $(document).on("click", ".del_btn", function () {

            let id = $(this).attr("data-id");
            let url = "${ROOT_PATH}" + "/back/jurisdiction/delete";
            $.post(url, {id: id}, function (res) {
                if (res.error === false) {
                    alert("删除成功！");
                    $("#table").bootstrapTable('refresh', {silent: true});
                }
            });
        });

    });
</script>
