<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>管理员管理页</title>
    <jsp:include page="/static/back/common/links.jsp" flush="true"/>
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
<!-- Navigation-->
<jsp:include page="/static/back/common/nav.jsp" flush="true"/>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">OxyGen</a>
            </li>
            <li class="breadcrumb-item active">角色管理</li>
        </ol>
        <!-- Example DataTables Card-->
        <div class="card mb-3">
            <div class="card-header">
                <i class="fa fa-table"></i> 角色管理
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <button class="btn btn-secondary" id="add_btn">新增</button>
                </div>
                <table id="table" class="table-bordered">
                </table>
            </div>
            <div class="card-footer small text-muted">Updated By OxyGen</div>
        </div>
    </div>
    <jsp:include page="/static/back/common/scripts.jsp" flush="true"/>
    <jsp:include page="/static/back/common/footer.jsp" flush="true"/>
</div>
<!-- 模态框（Modal） -->
<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="formModalLabel">修改角色</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <form id="the_form">
                    <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label">角色名</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" id="name" name="name"
                                   placeholder="角色名">
                        </div>
                    </div>
                    <div class="form-group row" id="jurisdiction">

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="add_form_btn">提交</button>
                        <button type="button" class="btn btn-primary" id="update_form_btn">保存</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal -->

    </div>

</body>

</html>
<script>
    $(function () {
        //表格生成
        let url = "${ROOT_PATH}" + "/back/role/selectAll";

        //初始化数据
        $('#table').bootstrapTable({
            url: url,
            search: true,
            striped: true,
            clickToSelect: true,
            uniqueId: "id",
            sortable: true,
            sortOrder: "asc",
            sidePagination: "client",
            showAllColumns: true,
            showRefresh: true,
            pagination: true,
            pageNumber: 1,
            pageSize: 10,
            queryParamsType: "",
            columns: [
                {"field": "id", "title": "id"},
                {"field": "name", "title": "角色名称"},
                {"field": "jurisdictionList", "title": "拥有权限", formatter: jurisdictionFormatter},
                {"field": "createTime", "title": "创建时间"},
                {"field": "updateTime", "title": "更新时间"},
                {
                    "field": "id",
                    "title": "操作",
                    width: 120,
                    align: "center",
                    valign: "middle",
                    formatter: actionFormatter
                }
            ]
        });

        //拥有权限格式化
        function jurisdictionFormatter(value, row, index) {
            let str = "";
            if (value != null && value.length > 0) {
                value.forEach(function (e, i) {
                    if (i !== value.length - 1) {
                        str += e.title + ",";
                    } else {
                        str += e.title;
                    }

                });

            }

            return str;
        }


        //操作按钮格式化
        function actionFormatter(value, row, index) {
            let id = value;
            let result = "";
            result += `<a href='javascript:;' class='btn btn-xs blue update_btn'  title='编辑' data-id="\${id}"><span class='glyphicon glyphicon-pencil'></span></a>`;
            result += `<a href='javascript:;' class='btn btn-xs red del_btn'  title='删除'   data-id="\${id}"><span class='glyphicon glyphicon-remove'></span></a>`;
            return result;
        }

        let is_first = true; //第一次才进行ajax加载权限选择
        //打开添加界面
        $("#add_btn").click(function () {
            $("#jurisdiction :checkbox:checked").prop("checked", false); //取消选中
            getJurisdictionList();
            $('#formModal').modal('show');
            $("#id").remove();
            $("#name").val("");
            $("#add_form_btn").show();
            $("#update_form_btn").hide();
            $("#formModalLabel").html("新增权限");

        });

        //提交添加操作
        $("#add_form_btn").click(function () {
            if ($("#name").val().length === 0) {
                alert("名字不能为空啊");
                return;
            }
            if ($("#jurisdiction :checkbox:checked").length === 0) {
                alert("起码选一个权限吧，给点面子？");
                return;
            }
            let role = {"name": $("#name").val()};
            role.jurisdictionList = [];
            $("#jurisdiction :checkbox:checked").each(function () {
                let jurisdiction = {};
                jurisdiction.id = $(this).val();
                role.jurisdictionList.push(jurisdiction);
            });
            let url = "${ROOT_PATH}" + "/back/role/add";
            $.ajax({
                type: "POST",
                url: url,
                data: JSON.stringify(role),
                dataType: "json",
                contentType: "application/json",
                success: function (res) {
                    if (res.error === false) {
                        alert("添加成功");
                        $('#formModal').modal('hide');
                        $("#table").bootstrapTable('refresh', {silent: true});
                    } else {
                        console.log(res);
                    }
                }
            });
        });

        //请求权限列表
        function getJurisdictionList(isUpdate) {
            if (is_first) {
                let url = "${ROOT_PATH}" + "/back/jurisdiction/selectAll";
                if (isUpdate === true) { //如果传参了（一定是点击的更新），此时将this赋值，因为如果没传this过来，this默认是window
                    self = this;
                }
                $.get(url, function (res) {
                    if (res.error === false) {
                        $("#jurisdiction").append(`<label class="col-sm-2 col-form-label">权限</label>
                        <div class="col-sm-10" id="jurisdiction_col" style="padding-top: 8px;"></div>`);
                        res.data.forEach(function (e, i) {
                            $("#jurisdiction_col").append(`
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="\${e.id}" value="\${e.id}" name="jurisdictionList">
                    <label class="form-check-label" for="\${e.id}">\${e.title}</label>
                </div>`);
                        });
                        if (isUpdate === true) {
                            showUpdateView.apply(self); //说明是更新按钮在第一次派发过来的，将其this（上面保存为self，避免this转变）传过来进行调用
                        }
                        is_first = false;
                    } else {
                        console.log("数据请求失败");
                    }

                });

            }
        }


        //点击更新按钮，触发打开更新界面
        $(document).on("click", ".update_btn", function () {

            if (is_first) {
                getJurisdictionList.apply(this, [true]); //第一次防止在ajax加载还没好就去处理多选失败，将对显示更新的函数交给ajax回调后去调用
            } else {
                showUpdateView.apply(this); //之后正常调用
            }
        });


        //显示更新页面
        function showUpdateView() {
            $("#jurisdiction :checkbox:checked").prop("checked", false); //取消选中
            $("#formModalLabel").html("修改角色");
            $("#add_form_btn").hide();
            $("#update_form_btn").show();
            let id = $(this).attr("data-id");

            if ($("#id").length === 0) {
                $("#the_form").append(`<input name="id" value="\${id}" id="id" type="hidden" />`);
            } else {
                $("#id").val(id);
            }
            let row = $("#table").bootstrapTable('getRowByUniqueId', id);
            $('#formModal').modal('show');
            $("#name").val(row.name);
            let jurisdictionList = row.jurisdictionList;
            // console.log(jurisdictionList);

            if (jurisdictionList !== null && jurisdictionList.length > 0) {
                // console.log($("#jurisdiction"));
                $("#jurisdiction :checkbox").each(function () {
                    let self = $(this);
                    jurisdictionList.forEach(function (e, i) {
                        if (self.val() === e.id) {
                            self.prop("checked", true);
                        }
                    });
                });
            }
        }

        //提交更新操作
        $("#update_form_btn").click(function () {
            if ($("#id").val() === undefined || $("#id").val().length === 0) {
                console.log("表单id为空");
                return;
            }
            if ($("#name").val().length === 0) {
                alert("名字不能为空啊");
                return;
            }
            if ($("#jurisdiction :checkbox:checked").length === 0) {
                alert("起码选一个权限吧，给点面子？");
                return;
            }
            let role = {"name": $("#name").val(), "id": $("#id").val()};
            role.jurisdictionList = [];
            $("#jurisdiction :checkbox:checked").each(function () {
                let jurisdiction = {};
                jurisdiction.id = $(this).val();
                role.jurisdictionList.push(jurisdiction);
            });
            let url = "${ROOT_PATH}" + "/back/role/update";
            $.ajax({
                type: "POST",
                url: url,
                data: JSON.stringify(role),
                dataType: "json",
                contentType: "application/json",
                success: function (res) {
                    if (res.error === false) {
                        alert("更新成功");
                        $('#formModal').modal('hide');
                        $("#table").bootstrapTable('refresh', {silent: true});
                    } else {
                        console.log(res);
                    }
                }
            });
        });


        //删除操作
        $(document).on("click", ".del_btn", function () {

            let id = $(this).attr("data-id");
            let url = "${ROOT_PATH}" + "/back/role/delete";
            $.post(url, {id: id}, function (res) {
                if (res.error === false) {
                    alert("删除成功！");
                    $("#table").bootstrapTable('refresh', {silent: true});
                }
            });
        });

    });
</script>
