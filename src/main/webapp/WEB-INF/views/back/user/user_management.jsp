<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>管理员管理页</title>
    <jsp:include page="/static/back/common/links.jsp" flush="true"/>
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
<!-- Navigation-->
<jsp:include page="/static/back/common/nav.jsp" flush="true"/>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">OxyGen</a>
            </li>
            <li class="breadcrumb-item active">用户管理</li>

        </ol>
        <!-- Example DataTables Card-->
        <div class="card mb-3">
            <div class="card-header">
                <i class="fa fa-table"></i> 用户管理
            </div>
            <div class="card-body">
                <div class="table-responsive">
                </div>
                <table id="table" class="table-bordered">
                </table>
            </div>
            <div class="card-footer small text-muted">Updated By OxyGen</div>
        </div>
    </div>
    <jsp:include page="/static/back/common/scripts.jsp" flush="true"/>
    <jsp:include page="/static/back/common/footer.jsp" flush="true"/>
</div>
<!-- 模态框（Modal） -->
<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="formModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="formModalLabel">修改用户</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <form id="the_form">
                    <input type="hidden" name="id" id="user_id">
                    <div class="form-group row">
                        <label for="account" class="col-sm-2 col-form-label">用户名</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" id="account" name="account"
                                   placeholder="用户名">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-sm-2 col-form-label">密码</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" id="password" name="password"
                                   placeholder="密码">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="phone" class="col-sm-2 col-form-label">手机号</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" id="phone" name="phone" placeholder="手机号">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="age" class="col-sm-2 col-form-label">年龄</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" id="age" name="age" placeholder="年龄">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label">昵称</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="name" name="name" placeholder="昵称">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="desc" class="col-sm-2 col-form-label">描述</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="desc" name="description" placeholder="描述">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="face" class="col-sm-2 col-form-label">头像路径</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="face" name="face" placeholder="头像路径">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="integral" class="col-sm-2 col-form-label">积分</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" id="integral" name="integral" placeholder="积分">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="update_form_btn">保存</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal -->
</div>

</body>

</html>
<script>
    $(function () {
        //表格生成
        let url = "${ROOT_PATH}" + "/back/user/selectAll";

        //初始化数据
        $('#table').bootstrapTable({
            url: url,
            search:true,
            striped: true,
            strictSearch: true,
            clickToSelect: true,
            uniqueId: "id",
            sortable: true,
            sortOrder:"asc",
            sidePagination: "server",
            showAllColumns:true,
            showRefresh:true,
            pagination:true,
            pageNumber:1,
            pageSize:10,
            queryParamsType : "",
            queryParams: function (params) { // 请求服务器数据时发送的参数，可以在这里添加额外的查询参数，返回false则终止请求
                return {
                    pageSize : params.pageSize,
                    pageNumber : params.pageNumber,
                    sort: params.sort, // 要排序的字段
                    sortOrder: params.order, // 排序规则
                }
            },
            columns: [
                {"field": "id", "title":"id"},
                {"field": "account","title":"账号"},
                {"field": "password","title":"密码"},
                {"field": "name","title":"昵称"},
                {"field": "phone","title":"手机号"},
                {"field": "description","title":"简介"},
                {"field": "integral","title":"积分"},
                {"field": "age","title":"年龄"},
                {"field": "face","title":"头像"},
                {"field": "type","title":"会员级别"},
                {"field": "createDate","title":"创建时间"},
                {"field": "updateTime","title":"更新时间"},
                {"field": "lastLoginTime","title":"最后登录时间"},
                {"field":"id","title":"操作",width:120,align:"center",valign:"middle",formatter:actionFormatter}
                ]
        });
        //操作按钮格式化
        function actionFormatter(value, row, index) {
            let id = value;
            let result = "";
            result += `<a href='javascript:;' class='btn btn-xs blue update_btn'  title='编辑' data-id="\${id}"><span class='glyphicon glyphicon-pencil'></span></a>`;
            result += `<a href='javascript:;' class='btn btn-xs red del_btn'  title='删除'   data-id="\${id}"><span class='glyphicon glyphicon-remove'></span></a>`;
            return result;
        }
        //删除操作
        $(document).on("click",".del_btn",function () {

            let id = $(this).attr("data-id");
            let url = "${ROOT_PATH}" + "/back/user/delete";
            $.post(url, {id: id}, function (res) {
                if (res.error === false) {
                    alert(res["msg"]);
                    $("#table").bootstrapTable('refresh',{silent: true});
                }
            });
        });

        //打开更新界面
        $(document).on("click",".update_btn",function () {
            let row = $("#table").bootstrapTable('getRowByUniqueId', $(this).attr("data-id"));
            $('#formModal').modal('show');
            $("#user_id").val(row.id);
            $("#account").val(row.account);
            $("#password").val(row.password);
            $("#phone").val(row.phone);
            $("#name").val(row.name);
            $("#age").val(row.age);
            $("#desc").val(row.description);
            $("#face").val(row.face);
            $("#integral").val(row.integral);
        });
        //提交更新操作
        $("#update_form_btn").click(function () {
            if($("#user_id").val()===undefined||$("#user_id").val().length===0){
                console.log("表单id为空");
                return;
            }
            if (checkForm() === -1) {
                return;
            }
            let url = "${ROOT_PATH}" + "/back/user/update";
            $.post(url, $("#the_form").serialize(), function (res) {
                if (res.error === false) {
                    alert("修改成功！");
                    $('#formModal').modal('hide');
                    $("#table").bootstrapTable('refresh',{silent: true});
                }
            });
        });



        //检查表单
        function checkForm() {
            let account = $("#account").val();
            let password = $("#password").val();
            let phone = $("#phone").val();
            let age = $("#age").val();
            if (account.length === 0 || password.length === 0) {
                alert("用户名或者密码不能为空");
                return -1;
            }
            if (phone.length !== 11) {
                alert("手机号为11位");
                return -1;
            }
            if (isNaN(parseInt(age)) || parseInt(age) < 0 || parseInt(age) > 120) {
                alert("年龄不符合规则");
                return -1;
            }
        }

    });
</script>
