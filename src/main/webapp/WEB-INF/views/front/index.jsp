<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home</title>
    <jsp:include page="/static/front/common/links.jsp" flush="true"/>
</head>

<body>

<jsp:include page="/static/front/common/header.jsp" flush="true"/>
<div class="slider">
    <div class="callbacks_container">
        <ul class="rslides" id="slider">
            <li><img src="${ROOT_PATH}/static/front/images/banner1.jpg" class="img-responsive" alt=""/>
                <div class="banner_desc">
                    <h1>We Provide Worlds top fashion for less fashionpress.</h1>
                    <h2>FashionPress the name of the of hi class fashion Web FreePsd.</h2>
                </div>
            </li>
            <li><img src="${ROOT_PATH}/static/front/images/banner2.jpg" class="img-responsive" alt=""/>
                <div class="banner_desc">
                    <h1>Duis autem vel eum iriure dolor in hendrerit.</h1>
                    <h2>Claritas est etiam processus dynamicus, qui sequitur .</h2>
                </div>
            </li>
            <li><img src="${ROOT_PATH}/static/front/images/banner3.jpg" class="img-responsive" alt=""/>
                <div class="banner_desc">
                    <h1>Ut wisi enim ad minim veniam, quis nostrud.</h1>
                    <h2>Mirum est notare quam littera gothica, quam nunc putamus.</h2>
                </div>
            </li>
        </ul>
    </div>
</div>
<jsp:include page="/static/front/common/search.jsp" flush="true"/>
<div class="main">
    <div class="content_top">
        <div class="container">
            <jsp:include page="/static/front/common/contaiber.jsp" flush="true"/>
            <div class="col-md-9 content_right">
                <div class="top_grid1">
                    <div class="col-md-4 box_2">
                        <div class="grid_1">
                            <a href="single.html">
                                <div class="b-link-stroke b-animate-go  thickbox">
                                    <img src="${ROOT_PATH}/static/front/images/p1.jpg" class="img-responsive" alt=""/>
                                </div>
                                <div class="grid_2">
                                    <p>There are many variations of passages</p>
                                    <ul class="grid_2-bottom">
                                        <li class="grid_2-left">
                                            <p>$99
                                                <small>.33</small>
                                            </p>
                                        </li>
                                        <li class="grid_2-right">
                                            <div class="btn btn-primary btn-normal btn-inline " target="_self"
                                                 title="Get It">Get It
                                            </div>
                                        </li>
                                        <div class="clearfix"></div>
                                    </ul>
                                </div>
                        </div>
                        </a>
                    </div>
                    <div class="col-md-8 box_1">
                        <a href="single.html">
                            <div class="grid_1">
                                <div class="b-link-stroke b-animate-go  thickbox">
                                    <img src="${ROOT_PATH}/static/front/images/p2.jpg" class="img-responsive" alt=""/>
                                </div>
                                <div class="grid_2">
                                    <p>There are many variations of passages</p>
                                    <ul class="grid_2-bottom">
                                        <li class="grid_2-left">
                                            <p>$99
                                                <small>.33</small>
                                            </p>
                                        </li>
                                        <li class="grid_2-right">
                                            <div class="btn btn-primary btn-normal btn-inline " target="_self"
                                                 title="Get It">Get It
                                            </div>
                                        </li>
                                        <div class="clearfix"></div>
                                    </ul>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="top_grid2">
                    <div class="col-md-4 top_grid1-box1">
                        <a href="single.html">
                            <div class="grid_1">
                                <div class="b-link-stroke b-animate-go  thickbox">
                                    <img src="${ROOT_PATH}/static/front/images/p3.jpg" class="img-responsive" alt=""/>
                                </div>
                                <div class="grid_2">
                                    <p>There are many variations of passages</p>
                                    <ul class="grid_2-bottom">
                                        <li class="grid_2-left">
                                            <p>$99
                                                <small>.33</small>
                                            </p>
                                        </li>
                                        <li class="grid_2-right">
                                            <div class="btn btn-primary btn-normal btn-inline " target="_self"
                                                 title="Get It">Get It
                                            </div>
                                        </li>
                                        <div class="clearfix"></div>
                                    </ul>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4 top_grid1-box1">
                        <a href="single.html">
                            <div class="grid_1">
                                <div class="b-link-stroke b-animate-go  thickbox">
                                    <img src="${ROOT_PATH}/static/front/images/p4.jpg" class="img-responsive" alt=""/>
                                </div>
                                <div class="grid_2">
                                    <p>There are many variations of passages</p>
                                    <ul class="grid_2-bottom">
                                        <li class="grid_2-left">
                                            <p>$99
                                                <small>.33</small>
                                            </p>
                                        </li>
                                        <li class="grid_2-right">
                                            <div class="btn btn-primary btn-normal btn-inline " target="_self"
                                                 title="Get It">Get It
                                            </div>
                                        </li>
                                        <div class="clearfix"></div>
                                    </ul>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4 top_grid1-box1">
                        <a href="single.html">
                            <div class="grid_1">
                                <div class="b-link-stroke b-animate-go  thickbox">
                                    <img src="${ROOT_PATH}/static/front/images/p5.jpg" class="img-responsive" alt=""/>
                                </div>
                                <div class="grid_2">
                                    <p>There are many variations of passages</p>
                                    <ul class="grid_2-bottom">
                                        <li class="grid_2-left">
                                            <p>$99
                                                <small>.33</small>
                                            </p>
                                        </li>
                                        <li class="grid_2-right">
                                            <div class="btn btn-primary btn-normal btn-inline " target="_self"
                                                 title="Get It">Get It
                                            </div>
                                        </li>
                                        <div class="clearfix"></div>
                                    </ul>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="top_grid2">
                    <div class="col-md-4 top_grid1-box1">
                        <a href="single.html">
                            <div class="grid_1">
                                <div class="b-link-stroke b-animate-go  thickbox">
                                    <img src="${ROOT_PATH}/static/front/images/p6.jpg" class="img-responsive" alt=""/>
                                </div>
                                <div class="grid_2">
                                    <p>There are many variations of passages</p>
                                    <ul class="grid_2-bottom">
                                        <li class="grid_2-left">
                                            <p>$99
                                                <small>.33</small>
                                            </p>
                                        </li>
                                        <li class="grid_2-right">
                                            <div class="btn btn-primary btn-normal btn-inline " target="_self"
                                                 title="Get It">Get It
                                            </div>
                                        </li>
                                        <div class="clearfix"></div>
                                    </ul>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4 top_grid1-box1">
                        <a href="single.html">
                            <div class="grid_1">
                                <div class="b-link-stroke b-animate-go  thickbox">
                                    <img src="${ROOT_PATH}/static/front/images/p7.jpg" class="img-responsive" alt=""/>
                                </div>
                                <div class="grid_2">
                                    <p>There are many variations of passages</p>
                                    <ul class="grid_2-bottom">
                                        <li class="grid_2-left">
                                            <p>$99
                                                <small>.33</small>
                                            </p>
                                        </li>
                                        <li class="grid_2-right">
                                            <div class="btn btn-primary btn-normal btn-inline " target="_self"
                                                 title="Get It">Get It
                                            </div>
                                        </li>
                                        <div class="clearfix"></div>
                                    </ul>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4 top_grid1-box1">
                        <a href="single.html">
                            <div class="grid_1">
                                <div class="b-link-stroke b-animate-go  thickbox">
                                    <img src="${ROOT_PATH}/static/front/images/p8.jpg" class="img-responsive" alt=""/>
                                </div>
                                <div class="grid_2">
                                    <p>There are many variations of passages</p>
                                    <ul class="grid_2-bottom">
                                        <li class="grid_2-left">
                                            <p>$99
                                                <small>.33</small>
                                            </p>
                                        </li>
                                        <li class="grid_2-right">
                                            <div class="btn btn-primary btn-normal btn-inline " target="_self"
                                                 title="Get It">Get It
                                            </div>
                                        </li>
                                        <div class="clearfix"></div>
                                    </ul>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <h4 class="head"><span class="m_2">Popular</span> Products Now</h4>
                <!--商品部分循环-->
                <div class="top_grid2">
                    <c:forEach  items="${commodity}" var="status">
                        <div class="col-md-4 top_grid1-box1" style="margin-bottom: 50px;">
                            <a href="${ROOT_PATH}/commodity/single/<c:out value="${status.id}"/>">
                                <div class="grid_1">
                                    <div class="b-link-stroke b-animate-go  thickbox">
                                        <img src="${ROOT_PATH}/static/front/images/commodtiy/<c:out value="${status.frontCommodityThumbnail}"/>" class="img-responsive" alt=""/>
                                    </div>
                                     <div class="grid_2">
                                        <p><c:out value="${status.name}"/></p>
                                        <ul class="grid_2-bottom">
                                            <li class="grid_2-left">
                                                <p>
                                                    <c:out value="${status.price}"/>
                                                    <small>.33</small>

                                                </p>
                                            </li>
                                            <li class="grid_2-right">
                                                <div class="btn btn-primary btn-normal btn-inline " target="_self"
                                                     title="Get It">购买
                                                </div>
                                            </li>
                                            <div class="clearfix"></div>
                                        </ul>
                                    </div>
                               </div>
                            </a>
                         </div>
                    </c:forEach>
                </div>
            </div>
        </div>
    </div>
</div>
<jsp:include page="/static/front/common/footer.jsp" flush="true"/>
<jsp:include page="/static/front/common/scripts.jsp" flush="true"/>
</body>

</html>