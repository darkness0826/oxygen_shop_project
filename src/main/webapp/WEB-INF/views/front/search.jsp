<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Register</title>
    <jsp:include page="/static/front/common/links.jsp" flush="true" />
</head>

<body>

<jsp:include page="/static/front/common/header.jsp" flush="true" />
<jsp:include page="/static/front/common/search.jsp" flush="true" />
<div class="about">
    <div class="container">
       <h1>搜索到的商品</h1>
    </div>
</div>
<jsp:include page="/static/front/common/footer.jsp" flush="true" />
<jsp:include page="/static/front/common/scripts.jsp" flush="true" />
</body>

</html>