<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>商品详情</title>
        <jsp:include page="/static/front/common/links.jsp" flush="true" />
    </head>

    <body>

        <jsp:include page="/static/front/common/header.jsp" flush="true" />
        <jsp:include page="/static/front/common/search.jsp" flush="true" />
        <div class="main">
            <div class="content_top">
                <div class="container">
                    <jsp:include page="/static/front/common/contaiber.jsp" flush="true"/>
                    <div class="col-md-9 single_right">
                        <div class="single_top">
                            <div class="single_grid">
                                <div class="grid images_3_of_2">
                                    <ul id="etalage">
                                        <c:forEach items="${picture}" var="star">
                                            <li>
                                                <a href="optionallink.html">
                                                    <img class="etalage_thumb_image" src="${ROOT_PATH}/static/front/images/commodtiy/<c:out value="${star}"/>" class="img-responsive" />
                                                    <img class="etalage_source_image" src="${ROOT_PATH}/static/front/images/commodtiy/<c:out value="${star}"/>" class="img-responsive" title="" />
                                                </a>
                                            </li>
                                        </c:forEach>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="desc1 span_3_of_2">
                                    <h1><c:out value="${commodity.name}"/></h1>
                                    <p class="availability">Availability: <span class="color">In stock</span></p>
                                    <div class="price_single">
                                        <span class="reducedfrom">$ ${commodity.price}</span>
                                        <span class="actual">$ ${commodity.price}</span><a href="#">click for offer</a>
                                    </div>
                                    <c:forEach items="${typeName}" var="sta" varStatus="loop">
                                        <ul class="size">
                                            <h3>${sta}</h3>
                                            <c:forEach items="${typeProperty[loop.count-1]}" var="lei" varStatus="loopid">
                                                    <li><a href="javascript:void(0) " class="ttype" typeid="${typePropertyId[loop.count-1][loopid.count-1]}">${lei}</a></li>
                                            </c:forEach>
                                        </ul>
                                    </c:forEach>

                                    <div class="quantity_box">
                                        <ul class="product-qty">
                                            <span>数量:</span>
                                            <div class="quantity">
                                                <input type="number" class="num" value="1">
                                            </div>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <a href="reservation.html" title="Online Reservation" class="btn bt1 btn-primary btn-normal btn-inline " target="_self">Buy</a>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="sap_tabs">
                            <div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
                                <ul class="resp-tabs-list">
                                    <li class="resp-tab-item" aria-controls="tab_item-0" role="tab"><span>产品描述</span></li>
                                    <li class="resp-tab-item" aria-controls="tab_item-1" role="tab"><span>附加信息</span></li>
                                    <li class="resp-tab-item" aria-controls="tab_item-2" role="tab"><span>评测</span></li>
                                    <div class="clear"></div>
                                </ul>
                                <div class="resp-tabs-container">
                                    <div class="tab-1 resp-tab-content" aria-labelledby="tab_item-0">
                                        <div class="facts">
                                            <ul class="tab_list">
                                                <li><a href="#">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat</a></li>
                                                <li><a href="#">augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigatione</a></li>
                                                <li><a href="#">claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica</a></li>
                                                <li><a href="#">Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="tab-1 resp-tab-content" aria-labelledby="tab_item-1">
                                        <div class="facts">
                                            <ul class="tab_list">
                                                <li><a href="#">待开发.</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="tab-1 resp-tab-content" aria-labelledby="tab_item-2">
                                        <div class="facts">
                                            <ul class="tab_list">
                                                <li><a href="#">待开发.</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h3 class="single_head">相关推荐</h3>
                        <div class="related_products">
                            <div class="col-md-4 top_grid1-box1 top_grid2-box2">
                                <a href="single.html">
                                    <div class="grid_1">
                                        <div class="b-link-stroke b-animate-go  thickbox">
                                            <img src="${ROOT_PATH}/static/front/images/p12.jpg" class="img-responsive" alt="" /> </div>
                                        <div class="grid_2">
                                            <p>There are many variations of passages</p>
                                            <ul class="grid_2-bottom">
                                                <li class="grid_2-left">
                                                    <p>$99<small>.33</small></p>
                                                </li>
                                                <li class="grid_2-right"><a href="single.html" title="Get It" class="btn btn-primary btn-normal btn-inline " target="_self">Get It</a></li>
                                                <div class="clearfix"> </div>
                                            </ul>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-4 top_grid1-box1">
                                <a href="single.html">
                                    <div class="grid_1">
                                        <div class="b-link-stroke b-animate-go  thickbox">
                                            <img src="${ROOT_PATH}/static/front/images/p13.jpg" class="img-responsive" alt="" /> </div>
                                        <div class="grid_2">
                                            <p>There are many variations of passages</p>
                                            <ul class="grid_2-bottom">
                                                <li class="grid_2-left">
                                                    <p>$99<small>.33</small></p>
                                                </li>
                                                <li class="grid_2-right"><a href="single.html" title="Get It" class="btn btn-primary btn-normal btn-inline " target="_self">Get It</a></li>
                                                <div class="clearfix"> </div>
                                            </ul>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-4 top_grid1-box1">
                                <a href="single.html">
                                    <div class="grid_1">
                                        <div class="b-link-stroke b-animate-go  thickbox">
                                            <img src="${ROOT_PATH}/static/front/images/p14.jpg" class="img-responsive" alt="" /> </div>
                                        <div class="grid_2">
                                            <p>There are many variations of passages</p>
                                            <ul class="grid_2-bottom">
                                                <li class="grid_2-left">
                                                    <p>$99<small>.33</small></p>
                                                </li>
                                                <li class="grid_2-right"><a href="single.html" title="Get It" class="btn btn-primary btn-normal btn-inline " target="_self">Get It</a></li>
                                                <div class="clearfix"> </div>
                                            </ul>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                        <div class="top_grid2">
                            <div class="col-md-4 top_grid1-box1 top_grid2-box2">
                                <a href="single.html">
                                    <div class="grid_1">
                                        <div class="b-link-stroke b-animate-go  thickbox">
                                            <img src="${ROOT_PATH}/static/front/images/p9.jpg" class="img-responsive" alt="" /> </div>
                                        <div class="grid_2">
                                            <p>There are many variations of passages</p>
                                            <ul class="grid_2-bottom">
                                                <li class="grid_2-left">
                                                    <p>$99<small>.33</small></p>
                                                </li>
                                                <li class="grid_2-right"><a href="single.html" title="Get It" class="btn btn-primary btn-normal btn-inline " target="_self">Get It</a></li>
                                                <div class="clearfix"> </div>
                                            </ul>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-4 top_grid1-box1">
                                <a href="single.html">
                                    <div class="grid_1">
                                        <div class="b-link-stroke b-animate-go  thickbox">
                                            <img src="${ROOT_PATH}/static/front/images/p10.jpg" class="img-responsive" alt="" /> </div>
                                        <div class="grid_2">
                                            <p>There are many variations of passages</p>
                                            <ul class="grid_2-bottom">
                                                <li class="grid_2-left">
                                                    <p>$99<small>.33</small></p>
                                                </li>
                                                <li class="grid_2-right"><a href="single.html" title="Get It" class="btn btn-primary btn-normal btn-inline " target="_self">Get It</a></li>
                                                <div class="clearfix"> </div>
                                            </ul>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-4 top_grid1-box1">
                                <a href="single.html">
                                    <div class="grid_1">
                                        <div class="b-link-stroke b-animate-go  thickbox">
                                            <img src="${ROOT_PATH}/static/front/images/p11.jpg" class="img-responsive" alt="" /> </div>
                                        <div class="grid_2">
                                            <p>There are many variations of passages</p>
                                            <ul class="grid_2-bottom">
                                                <li class="grid_2-left">
                                                    <p>$99<small>.33</small></p>
                                                </li>
                                                <li class="grid_2-right"><a href="single.html" title="Get It" class="btn btn-primary btn-normal btn-inline " target="_self">Get It</a></li>
                                                <div class="clearfix"> </div>
                                            </ul>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <jsp:include page="/static/front/common/footer.jsp" flush="true" />
        <jsp:include page="/static/front/common/scripts.jsp" flush="true" />

    </body>

    </html>