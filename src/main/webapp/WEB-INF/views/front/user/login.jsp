<%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>登录</title>
        <jsp:include page="/static/front/common/links.jsp" flush="true" />
    </head>

    <body>

        <jsp:include page="/static/front/common/header.jsp" flush="true" />
        <jsp:include page="/static/front/common/search.jsp" flush="true" />

        <div class="about">
            <div class="container">
                <div class="register">
                    <div class="col-md-6 login-left">
                        <h3>NEW CUSTOMERS</h3>
                        <p>By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.</p>
                        <a class="acount-btn" href="${ROOT_PATH}/user/register">Create an Account</a>
                    </div>
                    <div class="col-md-6 login-right">
                        <h3>REGISTERED CUSTOMERS</h3>
                        <p>If you have an account with us, please log in.</p>
                        <form>
                            <div>
                                <span>Email Address<label for="account">*</label></span>
                                <input class="form-control" id="account" type="email" name="account" aria-describedby="emailHelp"
                                       placeholder="Enter email">
                            </div>
                            <div>
                                <span>Password<label for="password">*</label></span>
                                <input class="form-control" id="password" type="password" name="password" placeholder="Password">
                            </div>
                            <a class="btn btn-primary btn-block" href="javascript:;" id="loginBtn">LOGIN</a>
                        </form>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>
        <jsp:include page="/static/front/common/footer.jsp" flush="true" />
        <jsp:include page="/static/front/common/scripts.jsp" flush="true" />

    </body>

    </html>
<script>
    $(function () {
        $("#loginBtn").click(function () {
            //登录操作
            let url = "${ROOT_PATH}" + "/user/login";
            let data = {
                "account": $("#account").val(),
                "password": $("#password").val()
            };
            if ($("#account").val().length === 0) {
                alert("用户名不能为空");
                return;

            } else if ($("#password").val().length === 0) {
                alert("密码不能为空");
                return;
            }
            $.post(url, data, function (res) {
                if (res.error === false) {
                    alert("登录成功~");
                    if (res.data.markUrl) {
                        location.href = res.data.markUrl;
                    } else {
                        location.href = "${ROOT_PATH}" + "/index/index";
                    }
                } else {
                    alert("登录失败");
                }
            }, "json");


        });
    });
</script>