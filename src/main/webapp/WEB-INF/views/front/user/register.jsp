<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>注册</title>
    <jsp:include page="/static/front/common/links.jsp" flush="true"/>
</head>

<body>

<jsp:include page="/static/front/common/header.jsp" flush="true"/>
<jsp:include page="/static/front/common/search.jsp" flush="true"/>
<div class="about">
    <div class="container">
        <div class="register">
            <form>
                <div class="register-top-grid">
                    <h3>个人信息</h3>
                    <div>
                        <span for="account">账号<label>*</label></span>
                        <input type="email" class="form-control" id="account" name="account"
                               placeholder="账号">
                        <div id="test" class="color">

                        </div>
                    </div>
                    <div>
                        <span>密码<label>*</label></span>
                        <input type="password" class="form-control" id="password" name="password"
                               placeholder="密码">
                        <div id="pss" class="color">

                        </div>
                    </div>
                    <div>
                        <span>确认密码<label>*</label></span>
                        <input type="password" class="form-control" id="passwords" name="password"
                               placeholder="密码">
                        <div id="pass" class="color">

                        </div>
                    </div>


                    <div>
                        <span>姓名<label>*</label></span>
                        <input type="text" class="form-control" id="name" name="name" placeholder="昵称">

                    </div>
                    <div>
                        <span>手机号码<label>*</label></span>
                        <input type="number" class="form-control" id="phone" name="phone" placeholder="手机号">

                    </div>
                    <div>
                        <span>年龄<label></label></span>
                        <input type="number" class="form-control" id="age" name="age" placeholder="年龄">

                    </div>
                    <div class="register-box">
                        <label class="other_label">验 证 码
                            <input id="coder" maxlength="20" type="text" placeholder="请输入验证码"/>
                        </label>
                        <span id="code"></span>
                        <div id="hint" class="color">

                        </div>
                    </div>

                </div>

            </form>
            <div class="register-but">

                <button type="button" class="btn btn-primary" id="add_form_btn">提交</button>

            </div>
        </div>
    </div>
</div>

<jsp:include page="/static/front/common/footer.jsp" flush="true"/>
<jsp:include page="/static/front/common/scripts.jsp" flush="true"/>
</body>

</html>
<style>
    #code {
        display: inline-block;
        width: 100px;
        height: 46px;
        vertical-align: middle;
        background-color: black;
        font-size: 25px;
        color: #fff;
        text-align: center;
        line-height: 46px;
    }
</style>
<script>
    $(function () {
        //检测账号是否存在
        $("#account").blur(function () {
            var str = $("#account").val();
            var url = "${ROOT_PATH}" + "/user/test";
            var data = {
                "account": $("#account").val()
            };

            if (str == null || str == "") {
                $("#test").css("color", 'red');
                $("#test").text("请输入帐号");
            } else {
                var reg = /^[a-zA-Z]{1}([a-zA-Z0-9]|[._]){4,19}$/;
                isok = reg.test(str);
                if (!isok) {
                    $("#test").css("color", 'red');
                    $("#test").text("只能输入5-20个以字母开头、可带数字");
                } else {
                    $.post(url, data, function (res) {
                        if (res.error === true) {
                            $("#test").css("color", 'red');
                            $("#test").text("用户名重复");
                            code();

                        } else {
                            $("#test").css("color", 'green');
                            $("#test").text("用户名可用");
                        }

                    });
                }


            }

        });
        //密码
        $("#password").blur(function () {
            var pass = $("#password").val();
            var reg = /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,16}$/;
            if (!reg.test(pass)) {
                $("#pss").css("color", 'red');
                $("#pss").text("密码不符合规则(必须6-16位并且必须含有大写,小写,数字)");
            } else {
                $("#pss").css("color", 'green');
                $("#pss").text("密码符合规则");
            }
        });


        // 	验证码
        $("#coder").focus(function () {
            if ($(this).val().length == 0) {
                $("#hint").text("看不清？点击图片更换验证码");
            }
        })

        //	确认密码
        $("#passwords").blur(function () {
            if ($(this).val().length == 0) {
                $("#pass").text("");
                $("#pass").css("color", '#ccc');
            } else if ($(this).val() != $("#password").val()) {
                $("#pass").text("两次密码不匹配");
                $("#pass").css("color", 'red');
            } else {
                $("#pass").text("两次密码匹配");
                $("#pass").css("color", 'green');
            }
        });


//	 验证码刷新
        function code() {
            var str = "qwertyuiopasdfghjklzxcvbnm1234567890QWERTYUIOPLKJHGFDSAZXCVBNM";
            var str1 = 0;
            for (var i = 0; i < 4; i++) {
                str1 += str.charAt(Math.floor(Math.random() * 62))
            }
            str1 = str1.substring(1)
            $("#code").text(str1);
        }

        code();
        $("#code").click(code);
        //	验证码验证
        $("#coder").blur(function () {
            if ($(this).val().length == 0) {
                $(this).next("div").text("");
                $(this).next("div").css("color", '#ccc');
            } else if ($(this).val().toUpperCase() != $("#code").text().toUpperCase()) {
                $("#hint").text("验证码不正确");
                $("#hint").css("color", 'red');
                code();
            } else {
                $("#hint").text("验证码正确");
                $("#hint").css("color", 'green');
            }
        })


        $("#add_form_btn").click(function () {
            if (checkForm() === -1) {
                return;
            }

            var url = "${ROOT_PATH}" + "/user/register";
            var data = {
                "account": $("#account").val(),
                "password": $("#password").val(),
                "name": $("#name").val(),
                "phone": $("#phone").val(),
                "age": $("#age").val()
            }
            console.log(data);
            if ($("#test").css("color") == 'rgb(0, 128, 0)' && $("#pass").css("color") == 'rgb(0, 128, 0)'
                && $("#hint").css("color") == 'rgb(0, 128, 0)' && $("#pss").css("color") == 'rgb(0, 128, 0)') {
                $.post(url, data, function (res) {
                    if (res.error === false) {
                        alert("注册成功！");
                        window.location.href = "${ROOT_PATH}" + "/user/login";
                    } else {
                        alert("注册失败,请重新注册");
                        window.location.href = "${ROOT_PATH}" + "/user/register";
                    }
                }, "json");
            } else {
                alert("请检查格式是否正确")


            }

        });

        //检查表单
        function checkForm() {
            let account = $("#account").val();
            let password = $("#password").val();
            let phone = $("#phone").val();
            let name = $("#name").val();
            let age = $("#age").val();
            if (account.length === 0 || password.length === 0) {
                alert("用户名或者密码不能为空");
                return -1;
            }


            if (name = null) {
                alert("姓名不能为空");
                return -1;
            }
            if (isNaN(parseInt(age)) || parseInt(age) < 0 || parseInt(age) > 120) {
                alert("年龄不符合规则");
                return -1;
            }


        }
    });

</script>