<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta charset="utf-8">
    <meta http-equiv="Cache-Control" content="no-siteapp">
    <title>个人信息</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <jsp:include page="/static/front/common/links.jsp" flush="true"/>
</head>
<body>
    <jsp:include page="/static/front/common/header.jsp" flush="true"/>
    <div class="wrapper uc-router">
        <ul>
            <li><a href="index.html">首页</a></li>
            <li><span class="divider"></span></li>
            <li><span>个人中心</span></li>
        </ul>
    </div>

    <div class="wrapper">
        <div class="uc-main clearfix">
            <jsp:include page="/static/front/common/leftheader.jsp" flush="true"/>
            <div class="uc-content">
                <div class="uc-panel">
                    <div class="uc-bigtit">个人信息</div>
                    <div class="uc-panel-bd">
                        <div class="account-info clearfix">

                            <div class="col-userinfo">
                                <div class="col-headpic" style="margin: 0px 201px;">

                                    <div class="pic-wrap">
                                        <label>
                                        <input type="file" id="file" name="file" style="display:none">
                                        <div class="picbox">
                                            <img src="${ROOT_PATH}/static/front/images/face/<c:out value="${personal.face}"/>" alt=""width="123px"height="123px" />
                                        </div>
                                        </label>
                                    </div>

                                </div>
                                <div style="display: none;">
                                    <input id="id" value="<c:out value="${personal.id}"/>">

                                </div>
                                <div class="control-group">
                                    <div class="hd">帐号：</div>
                                    <div class="bd"><input class="ui-txtin" type="text" disabled="disabled"value="<c:out value="${personal.account}"/>"></div>
                                </div>
                                <div class="control-group">
                                    <div class="hd">昵称：</div>
                                    <div class="bd"><input class="ui-txtin" id="name" type="text" name="" value="<c:out value="${personal.name}"/>" /></div>
                                </div>
                                <div class="control-group">
                                    <div class="hd">性别：</div>
                                    <div class="bd">
                                        <label class="check"><input type="radio" name="sex" />男</label>
                                        <label class="check ml25"><input type="radio" name="sex" />女</label>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="hd">电话：</div>
                                    <div class="bd"><input class="ui-txtin" id="phone" type="text" name="" value="<c:out value="${personal.phone}"/>" />
                                        </div>
                                    <div id="dd" ></div>
                                </div>

                                <div class="control-submit">
                                    <input class="ui-btn-theme submit"id="submit" type="submit" value="确认" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <jsp:include page="/static/front/common/footer.jsp" flush="true"/>
    <jsp:include page="/static/front/common/scripts.jsp" flush="true"/>
</body>

<script>
$(function () {
    var imgUrl = "";
    $("#file").change(function () {
        alert("修好");
        var formData = new FormData();
        // var file = $('#file');
        formData.append('file',$('#file')[0].files[0]);

        // console.log(file);
        // alert(file);
        $.ajax({
            url: "${ROOT_PATH}/back/commodity/upload",
            data: formData,
            type: "POST",
            contentType: false,
            processData : false,
            success: function(res) {
                console.log(res["error"]);
                console.log(res["msg"]);
                console.log(res["data"]);
                $.each(res, function(i, item) {
                    imgUrl = item.imgUrl;
                    console.log(imgUrl);

                });
                alert(res["msg"]);
            }

        });
    });
    $("#phone").blur(function () {
        //alert("123");

        var phone= $("#phone").val();
        var myreg=/^[1][3,4,5,7,8][0-9]{9}$/;
        if (!myreg.test(phone)) {
            $("#dd").text("手机号码格式错误");
            $("#dd").css("color", 'red');
            $("#phone").css("color", 'red');
            return;
        }else {
            $("#dd").text("手机号码格式正确");
            $("#dd").css("color", 'green');
            $("#phone").css("color", 'green');
        }




    });
    $("#submit").click(function () {

        //登录操作
        var  url = "${ROOT_PATH}" + "/user/alter";
        var data = {
            "id":$("#id").val(),
            "name": $("#name").val(),
            "phone": $("#phone").val()
        };
        console.log(data);

            $.post(url, data, function (res) {
                if (res.error === false) {
                    alert("修改成功~");
                    location.reload();
                } else {
                    alert("修改失败");
                }
            }, "json");




    });

});
</script>
</html>