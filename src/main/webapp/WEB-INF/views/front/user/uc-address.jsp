<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta charset="utf-8">
    <meta http-equiv="Cache-Control" content="no-siteapp">
    <title>收货地址</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <jsp:include page="/static/front/common/links.jsp" flush="true"/>
</head>
<body>
<jsp:include page="/static/front/common/header.jsp" flush="true"/>
    <!--头部-->

    <div class="wrapper uc-router">
        <ul>
            <li><a href="index.html">首页</a></li>
            <li><span class="divider"></span></li>
            <li><span>个人中心</span></li>
        </ul>
    </div>

    <div class="wrapper">
        <div class="uc-main clearfix">
            <jsp:include page="/static/front/common/leftheader.jsp" flush="true"/>
            <div class="uc-content">
                <div class="uc-panel">
                    <div class="uc-bigtit">收货地址</div>
                    <div class="uc-panel-bd">

                            <div class="ui-msg-info ui-msg-block">您已创建 2 个收货地址，最多可创建 5 个</div>

                            <div class="address-list">
                                <div class="col col-4">
                                    <a class="item va-m-box ta-c add">
                                        <div class="add-new">
                                            <span class="ico"><i class="iconfont icon-tianjia"></i></span>
                                            <div class="label">添加收货地址</div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col col-4">
                                    <div class="item">
                                        <div class="action">
                                            <div class="fl"><a class="edit" href="javascript:;">修改</a><a class="del" href="javascript:;">删除</a></div>
                                            <div class="fr"><a class="setdft" href="javascript:;">设为默认</a></div>

                                        </div>
                                        <div class="info">
                                            <div class="info-item name ellipsis">安徽合肥（小柚子 收）</div>
                                            <div class="info-item address">瑶海区东方商城</div>
                                            <div class="info-item tel ellipsis">13666666666</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col col-4">
                                    <div class="item active">
                                        <div class="action">
                                            <div class="fl"><a class="edit" href="javascript:;">修改</a><a class="del" href="javascript:;">删除</a></div>
                                            <div class="fr"><a class="setdft" href="javascript:;">设为默认</a></div>
                                        </div>
                                        <div class="info">
                                            <div class="info-item name ellipsis">安徽合肥（小柚子 收）</div>
                                            <div class="info-item address">瑶海区东方商城</div>
                                            <div class="info-item tel ellipsis">13666666666</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--脚部-->
<jsp:include page="/static/front/common/footer.jsp" flush="true"/>
<jsp:include page="/static/front/common/scripts.jsp" flush="true"/>
    <!--脚部-->
</body>
<script>

</script>
</html>