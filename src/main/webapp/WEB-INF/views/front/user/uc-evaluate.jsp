

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta charset="utf-8">
    <meta http-equiv="Cache-Control" content="no-siteapp">
    <title>商品评价</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <jsp:include page="/static/front/common/links.jsp" flush="true"/>
</head>
<body>
<jsp:include page="/static/front/common/header.jsp" flush="true"/>
<!--头部-->

<div class="wrapper uc-router">
    <ul>
        <li><a href="index.html">首页</a></li>
        <li><span class="divider"></span></li>
        <li><span>个人中心</span></li>
    </ul>
</div>

<div class="wrapper">
    <div class="uc-main clearfix">
<jsp:include page="/static/front/common/leftheader.jsp" flush="true"/>
            <div class="uc-content">
                <div class="uc-panel">
                    <div class="uc-bigtit">商品评价</div>
                    <div class="uc-panel-bd">
                        <table class="evalute-table">
                            <tr class="hd">
                                <th width="360">
                                    <select name=""class="ui-txtin" style="width:120px;"><option value="">评价</option><option value="">好价</option><option value="">中价</option><option value="">评价</option></select>
                                    <select name=""class="ui-txtin ml10" style="width:120px;"><option value="">评论</option><option value="">有评论内容</option><option value="">无评论内容</option></select>
                                </th>
                                <th>被评价人</th>
                                <th>宝贝信息</th>
                                <th class="align-center">操作</th>
                            </tr>
                            <tr class="item">
                                <td>
                                    <div class="cont">宝贝真心不错，质量很棒，下次还来<div class="time">[2016-05-29 16:02] </div></div>
                                </td>
                                <td>商家：小米旗舰店</td>
                                <td>小米短袖T恤 五彩换<div class="text-theme">499.00元</div></td>
                                <td class="align-center"><a class="ui-btn-theme uc-btn-md" href="uc-evaluate.jsp">回复</a></td>
                            </tr>
                            <tr class="item">
                                <td>
                                    <div class="cont good">宝贝真心不错，质量很棒，下次还来<div class="time">[2016-05-29 16:02] </div></div>
                                </td>
                                <td>商家：小米旗舰店</td>
                                <td>小米短袖T恤 五彩换<div class="text-theme">499.00元</div></td>
                                <td class="align-center"><a class="ui-btn-theme uc-btn-md" href="uc-evaluate.jsp">回复</a></td>
                            </tr>
                            <tr class="item">
                                <td>
                                    <div class="cont bad">宝贝真心不错，质量很棒，下次还来<div class="time">[2016-05-29 16:02] </div></div>
                                </td>
                                <td>商家：小米旗舰店</td>
                                <td>小米短袖T恤 五彩换<div class="text-theme">499.00元</div></td>
                                <td class="align-center"><a class="ui-btn-theme uc-btn-md" href="uc-evaluate.jsp">回复</a></td>
                            </tr>
                        </table>

                        <div class="pages">
                            <a class="page prev" href="uc-evaluate.jsp">上一页</a>
                            <a class="page" href="uc-evaluate.jsp">1</a>
                            <span class="cur-page">2</span>
                            <a class="page" href="uc-evaluate.jsp">3</a>
                            <a class="page" href="uc-evaluate.jsp">4</a>
                            <i class="page-split">...</i>
                            <a class="page" href="uc-evaluate.jsp">71</a>
                            <a class="page next" href="uc-evaluate.jsp">下一页</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


 <!--脚部-->
 <jsp:include page="/static/front/common/footer.jsp" flush="true"/>
 <jsp:include page="/static/front/common/scripts.jsp" flush="true"/>
 <!--脚部-->
 </body>
 <script>

 </script>
 </html>