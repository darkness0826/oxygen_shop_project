<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta charset="utf-8">
    <meta http-equiv="Cache-Control" content="no-siteapp">
    <title>我的收藏</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <jsp:include page="/static/front/common/links.jsp" flush="true"/>
</head>
<body>
<jsp:include page="/static/front/common/header.jsp" flush="true"/>
    <div class="wrapper uc-router">
        <ul>
            <li><a href="index.html">首页</a></li>
            <li><span class="divider"></span></li>
            <li><span>个人中心</span></li>
        </ul>
    </div>

    <div class="wrapper">
        <div class="uc-main clearfix">
            <jsp:include page="/static/front/common/leftheader.jsp" flush="true"/>
            <div class="uc-content">
                <div class="uc-panel">
                    <div class="uc-bigtit">我的收藏</div>
                    <div class="uc-panel-bd">
                        <div class="uc-tabs">
                            <a class="item active" href="${ROOT_PATH}/user/fav">我收藏的商品</a>
                            <a class="item" href="${ROOT_PATH}/user/favshop">我收藏的店铺</a>
                        </div>
                        <div class="uc-bar">
                            <label class="check"><input type="checkbox" /><span class="ml10 mr10">全选</span></label><a href="uc-fav-shop.jsp" class="del iconfont icon-shanchu"></a>
                        </div>
                        <div class="fav-shop">
                            <div class="row">
                                <div class="s-info-col">
                                    <label class="check"><input type="checkbox" /></label>
                                    <div class="s-info">
                                        <a href="uc-fav-shop.jsp"><img class="s-logo" src="uploads/9.jpg" alt="" /></a>
                                        <div class="name"><a href="uc-fav-shop.jsp">韩都衣舍蔚然专</a></div>
                                        <a class="text-danger" href="uc-fav-shop.jsp">删除</a>
                                    </div>
                                </div>
                                <div class="s-goods">
                                    <div class="item">
                                        <a href="uc-fav-shop.jsp"><img src="uploads/2.jpg" alt="" /></a>
                                        <div class="info"><span class="price">¥75.00</span><span class="sale">销量0</span></div>
                                    </div>
                                    <div class="item">
                                        <a href="uc-fav-shop.jsp"><img src="uploads/2.jpg" alt="" /></a>
                                        <div class="info"><span class="price">¥75.00</span><span class="sale">销量0</span></div>
                                    </div>
                                    <div class="item">
                                        <a href="uc-fav-shop.jsp"><img src="uploads/2.jpg" alt="" /></a>
                                        <div class="info"><span class="price">¥75.00</span><span class="sale">销量0</span></div>
                                    </div>
                                    <div class="item">
                                        <a href="uc-fav-shop.jsp"><img src="uploads/2.jpg" alt="" /></a>
                                        <div class="info"><span class="price">¥75.00</span><span class="sale">销量0</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="s-info-col">
                                    <label class="check"><input type="checkbox" /></label>
                                    <div class="s-info">
                                        <a href="uc-fav-shop.jsp"><img class="s-logo" src="uploads/9.jpg" alt="" /></a>
                                        <div class="name"><a href="uc-fav-shop.jsp">韩都衣舍蔚然专</a></div>
                                        <a class="text-danger" href="uc-fav-shop.jsp">删除</a>
                                    </div>
                                </div>
                                <div class="s-goods">
                                    <div class="item">
                                        <a href="uc-fav-shop.jsp"><img src="uploads/2.jpg" alt="" /></a>
                                        <div class="info"><span class="price">¥75.00</span><span class="sale">销量0</span></div>
                                    </div>
                                    <div class="item">
                                        <a href="uc-fav-shop.jsp"><img src="uploads/2.jpg" alt="" /></a>
                                        <div class="info"><span class="price">¥75.00</span><span class="sale">销量0</span></div>
                                    </div>
                                    <div class="item">
                                        <a href="uc-fav-shop.jsp"><img src="uploads/2.jpg" alt="" /></a>
                                        <div class="info"><span class="price">¥75.00</span><span class="sale">销量0</span></div>
                                    </div>
                                    <div class="item">
                                        <a href="uc-fav-shop.jsp"><img src="uploads/2.jpg" alt="" /></a>
                                        <div class="info"><span class="price">¥75.00</span><span class="sale">销量0</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="s-info-col">
                                    <label class="check"><input type="checkbox" /></label>
                                    <div class="s-info">
                                        <a href="uc-fav-shop.jsp"><img class="s-logo" src="uploads/9.jpg" alt="" /></a>
                                        <div class="name"><a href="uc-fav-shop.jsp">韩都衣舍蔚然专</a></div>
                                        <a class="text-danger" href="uc-fav-shop.jsp">删除</a>
                                    </div>
                                </div>
                                <div class="s-goods">
                                    <div class="item">
                                        <a href="uc-fav-shop.jsp"><img src="uploads/2.jpg" alt="" /></a>
                                        <div class="info"><span class="price">¥75.00</span><span class="sale">销量0</span></div>
                                    </div>
                                    <div class="item">
                                        <a href="uc-fav-shop.jsp"><img src="uploads/2.jpg" alt="" /></a>
                                        <div class="info"><span class="price">¥75.00</span><span class="sale">销量0</span></div>
                                    </div>
                                    <div class="item">
                                        <a href="uc-fav-shop.jsp"><img src="uploads/2.jpg" alt="" /></a>
                                        <div class="info"><span class="price">¥75.00</span><span class="sale">销量0</span></div>
                                    </div>
                                    <div class="item">
                                        <a href="uc-fav-shop.jsp"><img src="uploads/2.jpg" alt="" /></a>
                                        <div class="info"><span class="price">¥75.00</span><span class="sale">销量0</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="pages">
                            <a class="page prev" href="uc-fav-shop.jsp">上一页</a>
                            <a class="page" href="uc-fav-shop.jsp">1</a>
                            <span class="cur-page">2</span>
                            <a class="page" href="uc-fav-shop.jsp">3</a>
                            <a class="page" href="uc-fav-shop.jsp">4</a>
                            <i class="page-split">...</i>
                            <a class="page" href="uc-fav-shop.jsp">71</a>
                            <a class="page next" href="uc-fav-shop.jsp">下一页</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!--脚部-->
<jsp:include page="/static/front/common/footer.jsp" flush="true"/>
<jsp:include page="/static/front/common/scripts.jsp" flush="true"/>
<!--脚部-->
</body>
<script>

</script>

<script>
$(function () {
    $('.check input').iCheck({
            checkboxClass: 'sty1-checkbox'
        });
});
</script>
</html>