<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta charset="utf-8">
    <meta http-equiv="Cache-Control" content="no-siteapp">
    <title>我的收藏</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <jsp:include page="/static/front/common/links.jsp" flush="true"/>
</head>
<body>
<jsp:include page="/static/front/common/header.jsp" flush="true"/>
    <!--头部-->


    <div class="wrapper uc-router">
        <ul>
            <li><a href="index.html">首页</a></li>
            <li><span class="divider"></span></li>
            <li><span>个人中心</span></li>
        </ul>
    </div>

    <div class="wrapper">
        <div class="uc-main clearfix">
            <jsp:include page="/static/front/common/leftheader.jsp" flush="true"/>
            <div class="uc-content">
                <div class="uc-panel">
                    <div class="uc-bigtit">我的收藏</div>
                    <div class="uc-panel-bd">
                        <div class="uc-tabs">
                            <a class="item active" href="${ROOT_PATH}/user/fav">我收藏的商品</a>
                            <a class="item" href="${ROOT_PATH}/user/favshop">我收藏的店铺</a>
                        </div>
                        <table class="uc-table">
                            <tr class="hd">
                                <td width="16" class="check"><input type="checkbox" /></td>
                                <td widtd="360" class="align-left" >
                                    商家名称
                                </td>
                                <td width="100">价格</td>
                                <td width="100">库存情况</td>
                                <td width="120">操作</td>
                                <td><a href="uc-fav.jsp" class="del iconfont icon-shanchu"></a></td>
                            </tr>
                            <tr class="fav-goods">
                                <td class="check"><input type="checkbox" /></td>
                                <td class="item">
                                    <img src="uc-fav.jsp" alt="" /><span class="name">小米短袖T恤 五彩换</span>
                                </td>
                                <td><span class="text-theme fwb">298元</span></td>
                                <td>现货</td>
                                <td><a href="uc-fav.jsp" class="ui-btn-theme uc-btn-md mb5">加入购物车</a><br /><a class="text-muted" href="uc-fav.jsp">取消收藏</a></td>
                                <td><a href="uc-fav.jsp" class="text-warning">删除</a></td>
                            </tr>


                        </table>
                        <div class="pages">
                            <a class="page prev" href="uc-fav.jsp">上一页</a>
                            <a class="page" href="uc-fav.jsp">1</a>
                            <span class="cur-page">2</span>
                            <a class="page" href="uc-fav.jsp">3</a>
                            <a class="page" href="uc-fav.jsp">4</a>
                            <i class="page-split">...</i>
                            <a class="page" href="uc-fav.jsp">71</a>
                            <a class="page next" href="uc-fav.jsp">下一页</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!--脚部-->
<jsp:include page="/static/front/common/footer.jsp" flush="true"/>
<jsp:include page="/static/front/common/scripts.jsp" flush="true"/>
<!--脚部-->
</body>
<script>

</script>
<script>
$(function () {
    $('.check input').iCheck({
            checkboxClass: 'sty1-checkbox'
        });
});
</script>
</html>