<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta charset="utf-8">
    <meta http-equiv="Cache-Control" content="no-siteapp">
    <title>我的足迹</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <jsp:include page="/static/front/common/links.jsp" flush="true"/>
</head>
<body>
<jsp:include page="/static/front/common/header.jsp" flush="true"/>
<div class="wrapper uc-router">
    <ul>
        <li><a href="index.html">首页</a></li>
        <li><span class="divider"></span></li>
        <li><span>个人中心</span></li>
    </ul>
</div>

<div class="wrapper">
    <div class="uc-main clearfix">
        <jsp:include page="/static/front/common/leftheader.jsp" flush="true"/>
            <div class="uc-content">
                <div class="uc-panel">
                    <div class="uc-bigtit">我的足迹</div>
                    <div class="uc-panel-bd">
                        <div class="uc-bar mt10">
                            <div class="fl">以下是您最近30天的商品浏览记录</div>
                            <div class="fr">
                                <label class="check"><input type="checkbox" name="" id="" /><span class="ml10 mr10">全选</span></label><a href="uc-footprint.jsp" class="del iconfont icon-shanchu"></a>
                            </div>
                        </div>

                        <div class="footprint-box">
                            <div class="footprint-item">
                                <div class="f-info">
                                    <span class="f-date">今天</span><span class="f-time">2016-06-15</span><a class="f-del" href="uc-footprint.jsp">删除</a>
                                </div>
                                <div class="f-goods-list clearfix">
                                    <div class="item">
                                        <a href="uc-footprint.jsp" class="g-del"><i class="iconfont icon-shanchu"></i></a>
                                        <a href="uc-footprint.jsp"><img class="figure" src="uploads/34.jpg" alt="" /></a>
                                        <div class="cont">
                                            <div class="name"><a href="uc-footprint.jsp">小米4k曲面显示器</a></div>
                                            <div class="price">
                                                <span class="n">￥699</span><span class="s">1998</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footprint-item">
                                <div class="f-info">
                                    <span class="f-date">昨天</span><span class="f-time">2016-06-14</span><a class="f-del" href="uc-footprint.jsp">删除</a>
                                </div>
                                <div class="f-goods-list clearfix">
                                    <div class="item">
                                        <a href="uc-footprint.jsp" class="g-del"><i class="iconfont icon-shanchu"></i></a>
                                        <a href="uc-footprint.jsp"><img class="figure" src="uploads/34.jpg" alt="" /></a>
                                        <div class="cont">
                                            <div class="name"><a href="uc-footprint.jsp">小米4k曲面显示器</a></div>
                                            <div class="price">
                                                <span class="n">￥699</span><span class="s">1998</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <a href="uc-footprint.jsp" class="g-del"><i class="iconfont icon-shanchu"></i></a>
                                        <a href="uc-footprint.jsp"><img class="figure" src="uploads/34.jpg" alt="" /></a>
                                        <div class="cont">
                                            <div class="name"><a href="uc-footprint.jsp">小米4k曲面显示器</a></div>
                                            <div class="price">
                                                <span class="n">￥699</span><span class="s">1998</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <a href="uc-footprint.jsp" class="g-del"><i class="iconfont icon-shanchu"></i></a>
                                        <a href="uc-footprint.jsp"><img class="figure" src="uploads/34.jpg" alt="" /></a>
                                        <div class="cont">
                                            <div class="name"><a href="uc-footprint.jsp">小米4k曲面显示器</a></div>
                                            <div class="price">
                                                <span class="n">￥699</span><span class="s">1998</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <a href="uc-footprint.jsp" class="g-del"><i class="iconfont icon-shanchu"></i></a>
                                        <a href="uc-footprint.jsp"><img class="figure" src="uploads/34.jpg" alt="" /></a>
                                        <div class="cont">
                                            <div class="name"><a href="uc-footprint.jsp">小米4k曲面显示器</a></div>
                                            <div class="price">
                                                <span class="n">￥699</span><span class="s">1998</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="footprint-item end">
                                <div class="nomore">没有更多了哦</div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--脚部-->
    <jsp:include page="/static/front/common/footer.jsp" flush="true"/>
    <jsp:include page="/static/front/common/scripts.jsp" flush="true"/>
    <!--脚部-->
</body>

<script>
    $(function () {
        $('.check input').iCheck({
            checkboxClass: 'sty1-checkbox'
        });
    });
</script>

</html>