<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta charset="utf-8">
    <meta http-equiv="Cache-Control" content="no-siteapp">
    <title>我的私信</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <jsp:include page="/static/front/common/links.jsp" flush="true"/>
</head>
<body>
<jsp:include page="/static/front/common/header.jsp" flush="true"/>
<div class="wrapper uc-router">
    <ul>
        <li><a href="index.html">首页</a></li>
        <li><span class="divider"></span></li>
        <li><span>个人中心</span></li>
    </ul>
</div>

<div class="wrapper">
    <div class="uc-main clearfix">
        <jsp:include page="/static/front/common/leftheader.jsp" flush="true"/>
            <div class="uc-content">
                <div class="uc-panel">
                    <div class="uc-bigtit">我的私信</div>
                    <div class="uc-tabs">
                        <a class="item active" href="javascript:;">系统私信</a>
                    </div>
                    <div class="uc-bar">
                        <div class="fl">
                            <span class="va-m-ib mr5">共有<span class="text-danger">1</span>个联系人</span><input class="ui-txtin" type="text" name="" id="" /><input class="ui-btn-theme ml5" type="submit" value="搜索" />
                        </div>
                        <div class="fr">
                            <a href="uc-msg.jsp" class="ui-btn-warn">清空所有私信</a>
                            <a href="uc-msg.jsp" class="ui-btn-theme ml5">发送私信</a>
                        </div>
                    </div>
                    <div class="mc-list">
                        <div class="item">
                            <div class="desc">
                                私信内容私信内容私信内容私信内容私信内容私信内容私信内容私信内容私信内容私信内容私信内容私信内容私信内容私信内容私信内容私信内容私信内容
                            </div>
                            <div class="bar">
                                <div class="date">5天前</div>
                                <div class="op">
                                    <a href="uc-msg.jsp">回复</a>
                                    <a href="uc-msg.jsp">删除</a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="desc">
                                私信内容私信内容私信内容私信内容私信内容私信内容私信内容私信内容私信内容私信内容私信内容私信内容私信内容私信内容私信内容私信内容私信内容
                            </div>
                            <div class="bar">
                                <div class="date">5天前</div>
                                <div class="op">
                                    <a href="uc-msg.jsp">回复</a>
                                    <a href="uc-msg.jsp">删除</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!--脚部-->
<jsp:include page="/static/front/common/footer.jsp" flush="true"/>
<jsp:include page="/static/front/common/scripts.jsp" flush="true"/>
<!--脚部-->
</body>
</html>