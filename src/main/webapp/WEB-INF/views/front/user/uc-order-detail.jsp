<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta charset="utf-8">
    <meta http-equiv="Cache-Control" content="no-siteapp">
    <title>订单详情</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <jsp:include page="/static/front/common/links.jsp" flush="true"/>
</head>
<body>
<jsp:include page="/static/front/common/header.jsp" flush="true"/>
<div class="wrapper uc-router">
    <ul>
        <li><a href="index.html">首页</a></li>
        <li><span class="divider"></span></li>
        <li><span>个人中心</span></li>
    </ul>
</div>

<div class="wrapper">
    <div class="uc-main clearfix">
        <jsp:include page="/static/front/common/leftheader.jsp" flush="true"/>
            <div class="uc-content">
                <div class="uc-panel">
                    <div class="uc-bigtit">订单详情<a class="extra" href="uc-order-detail.jsp">请谨防钓鱼链接或诈骗后台，了解更多></a></div>
                    <div class="uc-panel-bd">
                        <div class="order-detail">
                            <div class="od-hd">
                                <div class="fl">
                                    <span class="tit">订单号：</span><span>1344643134736</span>
                                </div>
                                <div class="fr">
                                    <a href="uc-order-detail.jsp" class="ui-btn-low ui-btn-hollow uc-btn-md">取消订单</a>
                                    <a href="uc-order-detail.jsp" class="ui-btn-theme uc-btn-md">立即支付</a>
                                </div>
                            </div>
                            <div class="od-status">
                                <div class="tit">等待付款</div>1小时35后订单将被关闭
                            </div>
                            <div class="od-percent">
                                <div class="col"><div class="inner">下单<span class="time">05月30日 11:06</span></div></div>
                                <div class="col col2 active"><div class="inner">付款</div></div>
                                <div class="col col3"><div class="inner">发货</div></div>
                                <div class="col col4"><div class="inner">交易成功</div></div>
                            </div>
                            <div class="od-pdt">
                                <div class="item">
                                    <img src="uploads/11.jpg" class="figure" />
                                    <div class="pname">小米短袖T恤 五彩换 黑色 S</div>
                                    <div class="price">499元×1</div>
                                </div>
                                <div class="item">
                                    <img src="uploads/11.jpg" class="figure" />
                                    <div class="pname">小米短袖T恤 五彩换 黑色 S</div>
                                    <div class="price">499元×1</div>
                                </div>
                                <div class="item">
                                    <img src="uploads/11.jpg" class="figure" />
                                    <div class="pname">小米短袖T恤 五彩换 黑色 S</div>
                                    <div class="price">499元×1</div>
                                </div>
                                <div class="item">
                                    <img src="uploads/11.jpg" class="figure" />
                                    <div class="pname">小米短袖T恤 五彩换 黑色 S</div>
                                    <div class="price">499元×1</div>
                                </div>
                                <div class="item">
                                    <img src="uploads/11.jpg" class="figure" />
                                    <div class="pname">小米短袖T恤 五彩换 黑色 S</div>
                                    <div class="price">499元×1</div>
                                </div>
                            </div>
                            <div class="od-info">
                                <div class="item">
                                    <div class="tit">
                                        收货信息<a href="uc-order-detail.jsp" class="ui-btn-low ui-btn-hollow uc-btn-md fr">修改</a>
                                    </div>
                                    <div class="meta">
                                        <div>姓&emsp;&emsp;名：</div>
                                        <div>联系电话：</div>
                                        <div>收货地址：</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="tit">
                                        支付方式及送货时间<a href="uc-order-detail.jsp" class="ui-btn-low ui-btn-hollow uc-btn-md fr">修改</a>
                                    </div>
                                    <div class="meta">
                                        <div>支付方式：</div>
                                        <div>送货时间：</div>
                                        <div>送达时间：</div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="tit">
                                        发票信息
                                    </div>
                                    <div class="meta">
                                        <div>发票类型：</div>
                                        <div>发票内容：</div>
                                        <div>发票抬头：</div>
                                    </div>
                                </div>
                            </div>
                            <div class="od-count">
                                <div class="inner">
                                    <div class="item">
                                        <div class="tit">商品总价：</div>
                                        <div class="val">0元</div>
                                    </div>
                                    <div class="item">
                                        <div class="tit">运&emsp;&emsp;费：</div>
                                        <div class="val">0元</div>
                                    </div>
                                    <div class="item">
                                        <div class="tit">订单详情：</div>
                                        <div class="val">0元</div>
                                    </div>
                                    <div class="item last">
                                        <div class="tit">实付金额：</div>
                                        <div class="val"><span class="strong">0</span>元</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!--脚部-->
<jsp:include page="/static/front/common/footer.jsp" flush="true"/>
<jsp:include page="/static/front/common/scripts.jsp" flush="true"/>
<!--脚部-->
</body>

<script>
    $(function () {
        $('.check input').iCheck({
            checkboxClass: 'sty1-checkbox'
        });
    });
</script>

</html>