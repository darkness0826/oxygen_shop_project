<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta charset="utf-8">
    <meta http-equiv="Cache-Control" content="no-siteapp">
    <title>退款/退货</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <jsp:include page="/static/front/common/links.jsp" flush="true"/>
</head>
<body>
<jsp:include page="/static/front/common/header.jsp" flush="true"/>
<div class="wrapper uc-router">
    <ul>
        <li><a href="index.html">首页</a></li>
        <li><span class="divider"></span></li>
        <li><span>个人中心</span></li>
    </ul>
</div>

<div class="wrapper">
    <div class="uc-main clearfix">
        <jsp:include page="/static/front/common/leftheader.jsp" flush="true"/>
            <div class="uc-content">
                <div class="uc-panel">
                    <div class="uc-bigtit">退款/退货</div>
                    <div class="uc-panel-bd">
                        <div class="uc-sort">
                            <div class="uc-tabs">
                                <a class="item active" href="uc-refund.jsp">退款申请</a>
                                <a class="item" href="uc-refund.jsp">退货申请</a>
                            </div>
                            <div class="uc-search">
                                <form action="uc-refund.jsp">
                                    <input type="text" class="sch-input" placeholder="输入商品名称,订单号，商品编号" />
                                    <button class="sch-btn"><i class="iconfont icon-search"></i></button>
                                </form>
                            </div>
                        </div>
                        <table class="refund-returns-list">
                            <tr class="head bd-t">
                                <td width="370" class="first">商品信息</td>
                                <td>退款金额（元）</td>
                                <td>审核状态</td>
                                <td>平台确认</td>
                                <td width="140">操作</td>
                            </tr>
                            <tr class="item-head">
                                <td colspan="5">
                                    <div class="fl">
                                        退款编号: 1947584672162364 <span class="ml15">商家：<a class="sname" href="uc-refund.jsp">小米旗舰店</a></span>
                                    </div>
                                    <div class="fr">申请时间：2016-05-29 16:02   </div>
                                </td>
                            </tr>
                            <tr class="item">
                                <td class="first">
                                    <div class="good-desc">
                                        <img class="gimg" src="uploads/9.jpg" alt="" />
                                        <div class="ginfo">
                                            小米短袖T恤 五彩换<br />订单编号：16978514363344786
                                        </div>
                                    </div>
                                </td>
                                <td><span class="text-danger">499.00元</span></td>
                                <td>待审核</td>
                                <td>无</td>
                                <td class="last"><a href="uc-refund.jsp" class="ui-btn-theme uc-btn-md">查看</a></td>
                            </tr>
                            <tr class="blank"></tr>
                            <tr class="item-head">
                                <td colspan="5">
                                    <div class="fl">
                                        退款编号: 1947584672162364 <span class="ml15">商家：<a class="sname" href="uc-refund.jsp">小米旗舰店</a></span>
                                    </div>
                                    <div class="fr">申请时间：2016-05-29 16:02   </div>
                                </td>
                            </tr>
                            <tr class="item">
                                <td class="first">
                                    <div class="good-desc">
                                        <img class="gimg" src="uploads/9.jpg" alt="" />
                                        <div class="ginfo">
                                            小米短袖T恤 五彩换<br />订单编号：16978514363344786
                                        </div>
                                    </div>
                                </td>
                                <td><span class="text-danger">499.00元</span></td>
                                <td>待审核</td>
                                <td>无</td>
                                <td class="last"><a href="uc-refund.jsp" class="ui-btn-theme uc-btn-md">查看</a></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!--脚部-->
<jsp:include page="/static/front/common/footer.jsp" flush="true"/>
<jsp:include page="/static/front/common/scripts.jsp" flush="true"/>
<!--脚部-->
</body>

<script>
    $(function () {
        $('.check input').iCheck({
            checkboxClass: 'sty1-checkbox'
        });
    });
</script>

</html>