<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta charset="utf-8">
    <meta http-equiv="Cache-Control" content="no-siteapp">
    <title>账号安全</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <jsp:include page="/static/front/common/links.jsp" flush="true"/>
</head>
<body>
<jsp:include page="/static/front/common/header.jsp" flush="true"/>
<div class="wrapper uc-router">
    <ul>
        <li><a href="index.html">首页</a></li>
        <li><span class="divider"></span></li>
        <li><span>个人中心</span></li>
    </ul>
</div>

<div class="wrapper">
    <div class="uc-main clearfix">
        <jsp:include page="/static/front/common/leftheader.jsp" flush="true"/>
            <div class="uc-content">
                <div class="uc-panel">
                    <div class="uc-bigtit bd-b">账号安全</div>
                    <div class="uc-panel-bd">

                        <div class="safe-list">
                            <div class="list-item">
                                <a href="uc-verify.jsp" class="edit ui-btn-low ui-btn-hollow uc-btn-md">修改</a>
                                <div class="cont">
                                    <img src="img/uc/pwd.jpg" class="ico" />
                                    <div class="tit">帐号密码</div>
                                    <div class="desc">用于保护帐号信息和登录安全</div>
                                </div>
                            </div>
                            <div class="list-item">
                                <a href="uc-verify.jsp" class="edit ui-btn-low ui-btn-hollow uc-btn-md">修改</a>
                                <div class="cont">
                                    <img src="img/uc/mail.jpg" class="ico" />
                                    <div class="tit">安全邮箱 25******1@q*.com<span class="safe-status"><i class="iconfont icon-yanzheng text-success"></i>已绑定</span></div>
                                    <div class="desc">安全邮箱可以用于登录小米帐号，重置密码或其他安全验证</div>
                                </div>
                            </div>
                            <div class="list-item">
                                <a href="uc-verify.jsp" class="edit ui-btn-low ui-btn-hollow uc-btn-md">修改</a>
                                <div class="cont">
                                    <img src="img/uc/phone.jpg" class="ico" />
                                    <div class="tit">安全手机<span class="safe-status"><i class="iconfont icon-warn text-danger"></i>未绑定</span></div>
                                    <div class="desc">安全手机将可用于登录小米帐号和重置密码，建议立即设置</div>
                                </div>
                            </div>
                            <div class="list-item">
                                <a href="uc-verify.jsp" class="edit ui-btn-low ui-btn-hollow uc-btn-md">修改</a>
                                <div class="cont">
                                    <img src="img/uc/safe.jpg" class="ico" />
                                    <div class="tit">密保问题<span class="safe-status"><i class="iconfont icon-yanzheng text-success"></i>已设置</span></div>
                                    <div class="desc">密保问题用于安全验证，建议立即设置</div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--脚部-->
    <jsp:include page="/static/front/common/footer.jsp" flush="true"/>
    <jsp:include page="/static/front/common/scripts.jsp" flush="true"/>
    <!--脚部-->
</body>

<script>
    $(function () {
        $('.check input').iCheck({
            checkboxClass: 'sty1-checkbox'
        });
    });
</script>

</html>