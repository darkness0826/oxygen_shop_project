<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta charset="utf-8">
    <meta http-equiv="Cache-Control" content="no-siteapp">
    <title>账号安全</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <jsp:include page="/static/front/common/links.jsp" flush="true"/>
</head>
<body>
<jsp:include page="/static/front/common/header.jsp" flush="true"/>
<div class="wrapper uc-router">
    <ul>
        <li><a href="index.html">首页</a></li>
        <li><span class="divider"></span></li>
        <li><span>个人中心</span></li>
    </ul>
</div>

<div class="wrapper">
    <div class="uc-main clearfix">
        <jsp:include page="/static/front/common/leftheader.jsp" flush="true"/>
            <div class="uc-content">
                <div class="uc-panel">
                    <div class="uc-bigtit bd-b">账号安全</div>
                    <div class="uc-panel-bd">

                        <div class="verify-step mt20">
                            <div class="item item-f">
                                <i></i>1.验证身份
                            </div>
                            <div class="item item-m active">
                                <i></i>2.设置新密码
                            </div>
                            <div class="item item-l">
                                3.完成
                            </div>
                        </div>

                        <div class="verify-panel mt10">
                            <div class="inner-box">

                                <div class="control-group">
                                    <div class="hd">新登录密码：</div>
                                    <div class="bd">
                                        <input style="width: 220px;" class="ui-txtin" type="password" name="" />
                                    </div>
                                </div>
                                <div class="control-group mt30">
                                    <div class="hd">确认新密码：</div>
                                    <div class="bd">
                                        <input style="width: 220px;" class="ui-txtin" type="password" name="" />
                                    </div>
                                </div>
                                <div class="control-group mt30">
                                    <div class="hd">验证码：</div>
                                    <div class="bd">
                                        <input style="width: 80px;" class="ui-txtin" type="text" name="" />
                                        <img class="yzm" src="uc-verify2.jsp" alt="" />
                                        <div class="yzm-tip">
                                            看不清？<br /><a href="uc-verify2.jsp">换一张</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="submit-group mt30">
                                    <a href="uc-verify3.jsp" class="ui-btn-theme uc-btn-md">下一步</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

<!--脚部-->
<jsp:include page="/static/front/common/footer.jsp" flush="true"/>
<jsp:include page="/static/front/common/scripts.jsp" flush="true"/>
<!--脚部-->
</body>

<script>
    $(function () {
        $('.check input').iCheck({
            checkboxClass: 'sty1-checkbox'
        });
    });
</script>

</html>