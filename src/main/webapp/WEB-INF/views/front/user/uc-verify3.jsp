<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta charset="utf-8">
    <meta http-equiv="Cache-Control" content="no-siteapp">
    <title>账号安全</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <jsp:include page="/static/front/common/links.jsp" flush="true"/>
</head>
<body>
<jsp:include page="/static/front/common/header.jsp" flush="true"/>
<div class="wrapper uc-router">
    <ul>
        <li><a href="index.html">首页</a></li>
        <li><span class="divider"></span></li>
        <li><span>个人中心</span></li>
    </ul>
</div>

<div class="wrapper">
    <div class="uc-main clearfix">
        <jsp:include page="/static/front/common/leftheader.jsp" flush="true"/>
        <div class="uc-content">
            <div class="uc-panel">
                <div class="uc-bigtit bd-b">账号安全</div>
                <div class="uc-panel-bd">

                    <div class="verify-step mt20">
                        <div class="item item-f">
                            <i></i>1.验证身份
                        </div>
                        <div class="item item-m">
                            <i></i>2.设置新密码
                        </div>
                        <div class="item item-l active">
                            3.完成
                        </div>
                    </div>

                    <div class="verify-panel mt10">
                        <div class="notice">
                            <div class="notice-inner">
                                <i class="ico iconfont icon-check01"></i>
                                新密码设置成功！<br/>
                                请牢记您新设置的密码 <a class="text-info ml10" href="uc-verify3.jsp">返回登录</a>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!--脚部-->
<jsp:include page="/static/front/common/footer.jsp" flush="true"/>
<jsp:include page="/static/front/common/scripts.jsp" flush="true"/>
<!--脚部-->
</body>

<script>
    $(function () {
        $('.check input').iCheck({
            checkboxClass: 'sty1-checkbox'
        });
    });
</script>

</html>