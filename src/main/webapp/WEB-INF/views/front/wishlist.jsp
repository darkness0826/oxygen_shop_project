<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="${ROOT_PATH}/static/front/css/car.css" rel='stylesheet' type='text/css'/>
    <title>Wishlist</title>
    <jsp:include page="/static/front/common/links.jsp" flush="true"/>
</head>

<body>

<jsp:include page="/static/front/common/header.jsp" flush="true"/>
<jsp:include page="/static/front/common/search.jsp" flush="true"/>
<div class="about">
    <div class="container">

        <%--空的购物车--%>
        <%--<div class="register">--%>
        <%--<h4 class="title">Shopping cart is empty</h4>--%>
        <%--<p class="cart">You have no items in your shopping cart.<br>Click<a href="${ROOT_PATH}/index/index">--%>
        <%--here</a> to continue shopping</p>--%>
        <%--</div>--%>

        <%--购物车--%>
        <div class="car">
            <ul class="breadcrumb">
                <li>
                    <a href="{:url('index\\index')}">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li>
                    <a href="">购物车内的商品</a>
                </li>
            </ul>
            <div class="row">
                <div id="content" class="col-sm-12">
                    <h1>购物车内的商品 &nbsp;</h1>
                    <form action="" method="post" enctype="multipart/form-data">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>

                                    <td class="text-center">图片</td>
                                    <td class="text-left">商品名称</td>
                                    <td class="text-left">型号</td>
                                    <td class="text-left">数量</td>
                                    <td class="text-right">单价</td>
                                    <td class="text-right">总计</td>
                                </tr>
                                </thead>
                                <tbody>

                                <%--循环输出购物车的商品--%>
                                <c:forEach items="${orders}" var="status">

                                <tr>

                                    <td class="text-center">
                                        <a href="">
                                            <img src="${ROOT_PATH}/static/front/images/commodtiy/<c:out value="${status.commodity.frontCommodityThumbnail}"/>" alt="" title=""
                                                 class="img-thumbnail" style="max-height: 50px"/>
                                            <c:out value="${status.commodity.frontCommodityThumbnail}"/>
                                        </a>
                                    </td>
                                    <td class="text-left">
                                        <a href=""><c:out value="${status.commodity.name}"/></a>
                                    </td>
                                    <td class="text-left"></td>
                                    <td class="text-left">
                                        <div class="input-group btn-block" style="max-width: 200px;">
                                            <input type="text" name="" value="" size="1"
                                                   class="form-control" id=""/>
                                            <span class="input-group-btn">
                            <button type="button" data-toggle="tooltip" title="更新" class="btn btn-primary button-update"
                                    data-product_id="">
                              <i class="fa fa-refresh"></i>
                            </button>
                            <button type="button" data-toggle="tooltip" title="移除" class="btn btn-danger button-remove"
                                    data-product_id="">
                              <i class="fa fa-times-circle"></i>
                            </button>
                          </span>
                                        </div>
                                    </td>
                                    <td class="text-right">￥</td>
                                    <td class="text-right">￥</td>
                                </tr>

                        </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </form>

                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-8">
                            <table class="table table-bordered">
                                <tr>
                                    <td class="text-right"><strong>商品总额:</strong>
                                    </td>
                                    <td class="text-right">￥</td>
                                </tr>
                            </table>
                        </div>
                </div>
                <div class="buttons">
                    <div class="pull-left">
                        <a href="{:url(' index\\index')}" class="btn btn-default">继续购物</a>
                        </div>
                        <div class="pull-right">
                            <a href="" class="btn btn-primary">去结账</a>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<jsp:include page="/static/front/common/footer.jsp" flush="true"/>
<jsp:include page="/static/front/common/scripts.jsp" flush="true"/>
</body>

</html>


<script>
    $(function () {
        $('button.button-update').click(function (evt) {
            var url = 'http://www.shop.cn/cart-update.html';
            var data = {
                product_id: $(this).data('product_id'),
                buy_quantity: $('#input-buy_quantity-' + $(this).data('product_id')).val()
            };

            $.post(url, data, function (resp) {
                if (resp.error) {
                    alert('更新失败');
                } else {
                    //alert('更新成功');
                    window.location.reload();  //自动刷新
                }
            }, 'json');
            evt.preventDefault();
        });
        <!--移除购物车中的产品-->
        $('button.button-remove').click(function (evt) {
            var url = 'http://www.shop.cn/cart-remove.html';
            var data = {
                product_id: $(this).data('product_id')
            };
            $.post(url, data, function (resp) {
                if (resp.error) {
                    alert('删除失败');
                } else {
                    $(this).parents('tr').remove();
                    window.location.reload();  //自动刷新
                    // alert('删除成功');


                }
            }.bind(this), 'json');
            evt.preventDefault();
        });
    });
</script>