<%--
  Created by IntelliJ IDEA.
  User: 陶扬扬
  Date: 2018/6/29
  Time: 18:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<meta charset="utf-8">
<head>
    <title>商品测试</title>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
    <style>
        .container {
            margin-top: 10%;
        }

    </style>
</head>
<body>
<div class="container">
    <h3 class="text-center">登录</h3>
    <form class="form-horizontal">
        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">商品名</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="name" placeholder="商品名">
            </div>
        </div>
        <div class="form-group">
            <label for="price" class="col-sm-2 control-label">单价</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="price" placeholder="单价" />
            </div>
        </div>
        <div class="form-group" id="commodity">
            <div class="form-group commodity_commons">
                <label class="col-sm-2 control-label">商品属性与所属值</label>
                <div class="col-sm-3">
                    <input type="text" class="form-control commodity_type" placeholder="商品所拥有的属性" />
                </div>
                <div class="col-sm-5">
                    <input type="text" class="form-control commodity_type_parameter" placeholder="商品属性值" />
                </div>
                <div class="col-sm-2 commodityTypeBtn">
                    <button type="button" class="btn btn-primary" id="addBtn"><span class="glyphicon glyphicon-plus"></span></button>
                    <button type="button" class="btn btn-danger" id="delBtn"><span class="glyphicon glyphicon-minus"></span></button>
                </div>
            </div>
        </div>


        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="button" class="btn btn-default" id="login_btn">提交</button>
            </div>
        </div>

        <%--<div class="form-group">--%>
            <%--<div class="col-sm-offset-2 col-sm-10">--%>
                <%--<button type="button" class="btn btn-default" id="huoQu">获取</button>--%>
            <%--</div>--%>
        <%--</div>--%>
    </form>
</div>
</body>
<script>
    $(function() {

        // $("#huoQu").click(function () {
        //         var btns = $('.commodity_commons');
        //     btns.each(function(i, e) {
        //         alert($(this).find(".commodity_type").val());
        //         alert($(this).find(".commodity_type_parameter").val());
        //     }, false);
        // });


        /*
        添加商品的属性与值
         */
        $("#addBtn").click(function () {
            // alert(1);
            var str = "<div class=\"form-group commodity_commons\">\n" +
                "                <label class=\"col-sm-2\"></label>\n" +
                "                <div class=\"col-sm-3 commodityType\">\n" +
                "                    <input type=\"text\" class=\"form-control commodity_type\" placeholder=\"商品所拥有的属性\" />\n" +
                "                </div>\n" +
                "                <div class=\"col-sm-5\">\n" +
                "                    <input type=\"text\" class=\"form-control commodity_type_parameter\" placeholder=\"商品属性值\" />\n" +
                "                </div>\n" +
                "            </div>";

            $("#commodity").append(str);
        });
        /*
         删除商品的属性与值
        */
        $("#delBtn").click(function () {
            // alert(2);
            $("#commodity").children("div:last-child").remove();
        });


        $("#login_btn").click(function() {
            // alert($("#name").val()+ $(".commodity_type").val() + $("#is_del").val());
            var commodity = {};//商品的对象
            var commodityTypeArr = [];//商品属性与值存放的数组

            var btns = $('.commodity_commons');//遍历这个div的class
            btns.each(function(i, e) {
                var commodityType = {};//商品样式的对象
                commodityType.frontCommodityTypeParameter = $(this).find(".commodity_type_parameter").val();//商品属性值
                commodityType.frontCommodityType = $(this).find(".commodity_type").val();//商品所拥有的属性
                commodityTypeArr.push(commodityType);//把每次获取的值，放到这个数组里

            }, false);

            commodity.name = $("#name").val();//商品的名字
            commodity.price = $("#price").val();//商品的单间
            commodity.backCommodityTypeList = commodityTypeArr;//把商品属性与值存放的数组放到这个对象

            // alert(commodity);

            $.ajax({
                url: "${ROOT_PATH}/back/commodity/addCommodity",
                data: JSON.stringify(commodity),
                dataType: "json",
                type: "POST",
                contentType: "application/json",
                success: function(res) {
                    console.log(res["error"]);
                    console.log(res["msg"]);
                    console.log(res["data"]);
                    alert();
                }

            });
        })
    })
</script>
</html>
