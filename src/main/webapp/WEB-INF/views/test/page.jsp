<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<html>
<meta charset="utf-8">

<head>
    <title>用户登录页</title>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.js"></script>
</head>
<style>
    .container {
        margin-top: 10%;
    }
</style>

<body>

<div class="container">
    <h3 class="text-center">登录</h3>
    <form class="form-horizontal">
        <div class="form-group">
            <label for="user_name" class="col-sm-2 control-label">用户名</label>
            <div class="col-sm-10">
                <input type="email" class="form-control" id="user_name" placeholder="用户名">
            </div>
        </div>
        <div class="form-group">
            <label for="password" class="col-sm-2 control-label">密码</label>
            <div class="col-sm-10">
                <input type="password" class="form-control" id="password" placeholder="密码"/>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="button" class="btn btn-default" id="login_btn">登录</button>
            </div>
        </div>
    </form>
</div>
</body>

</html>
<script>
    $(function () {

        $("#login_btn").click(function () {
            //     $.post("http://localhost:8080/back/admin/login",
            //         {account: $("#user_name").val(), password: $("#password").val()},
            //         function (res) {
            //             if (res.error == "false") {
            //                 alert("成功！");
            //             }
            //         }, 'json');
            //
            // });
            $("#login_btn").click(function () {
                var user = {};
                user.account = $("#user_name").val();
                user.password = $("#password").val();

                $.ajax({
                    url: "http://localhost:8080/back/admin/login",
                    data: JSON.stringify(user),
                    dataType: "json",
                    type: "post",
                    contentType: "application/json",
                    success: function (res) {
                        alert("成功！");

                    }

                });
            });
        })

</script>