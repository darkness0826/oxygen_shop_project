<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- /.container-fluid-->
<!-- /.content-wrapper-->
<footer class="sticky-footer">
    <div class="container">
        <div class="text-center">
            <small>Copyright ©  OXYGEN 2018</small>
        </div>
    </div>
</footer>
<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fa fa-angle-up"></i>
</a>
<!-- Logout Modal-->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">如果您准备好结束当前会话，请选择下面的“退出登录”</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">取消</button>
                <a href="javascript:;" class="btn btn-primary" id="quit_login" >退出登录</a>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        $("#quit_login").click(function () {
            //退出操作
            let url = "${ROOT_PATH}"+"/back/admin/quit";
            $.post(url,function (res) {
                if(res.error===false){
                    alert("退出成功~");
                    location.href="${ROOT_PATH}"+"/back/admin/login";
                }else{
                    alert("出错了");
                }

            },"json");
        }) ;
    });
</script>