
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- Bootstrap core CSS-->
<link href="${ROOT_PATH}/static/back/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="${ROOT_PATH}/static/back/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="${ROOT_PATH}/static/back/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<link href="${ROOT_PATH}/static/back/css/sb-admin.css" rel="stylesheet">

<%--bootstrap-table--%>

<link href="${ROOT_PATH}/static/back/css/bootstrap-table.css" rel="stylesheet">
<link href="${ROOT_PATH}/static/back/css/open-iconic.min.css" rel="stylesheet">
<link href="${ROOT_PATH}/static/back/css/open-iconic-bootstrap.min.css" rel="stylesheet">

