<%--
  Created by IntelliJ IDEA.
  User: 陶扬扬
  Date: 2018/6/28
  Time: 17:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- Bootstrap core JavaScript-->
<script src="${ROOT_PATH}/static/back/vendor/jquery/jquery.min.js"></script>
<script src="${ROOT_PATH}/static/back/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<%--bootstrap-table--%>
<script src="${ROOT_PATH}/static/back/js/bootstrap-table.min.js"></script>
<script src="${ROOT_PATH}/static/back/js/bootstrap-table-zh-CN.min.js"></script>


<!-- Core plugin JavaScript-->
<script src="${ROOT_PATH}/static/back/vendor/jquery-easing/jquery.easing.min.js"></script>
<!-- Page level plugin JavaScript-->
<script src="${ROOT_PATH}/static/back/vendor/datatables/jquery.dataTables.js"></script>
<script src="${ROOT_PATH}/static/back/vendor/datatables/dataTables.bootstrap4.js"></script>
<!-- Custom scripts for all pages-->
<script src="${ROOT_PATH}/static/back/js/sb-admin.min.js"></script>
<!-- Custom scripts for this page-->
<script src="${ROOT_PATH}/static/back/js/sb-admin-datatables.min.js"></script>