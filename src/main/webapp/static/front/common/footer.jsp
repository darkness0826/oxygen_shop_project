<%@ page contentType="text/html;charset=UTF-8" language="java" %>
 <div class="footer_bg">
    </div>
    <div class="footer">
        <div class="container">
            <div class="col-md-3 f_grid1">
                <h3>About</h3>
                <a href="#"><img src="${ROOT_PATH}/static/front/images/logo.png" alt="" /></a>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
            </div>
            <div class="col-md-3 f_grid1 f_grid2">
                <h3>Follow Us</h3>
                <ul class="social">
                    <li>
                        <a href=""> <i class="fb"> </i>
                            <p class="m_3">Facebook</p>
                            <div class="clearfix"> </div>
                        </a>
                    </li>
                    <li><a href=""><i class="tw"> </i><p class="m_3">Twittter</p><div class="clearfix"> </div></a></li>
                    <li><a href=""><i class="google"> </i><p class="m_3">Google</p><div class="clearfix"> </div></a></li>
                    <li><a href=""><i class="instagram"> </i><p class="m_3">Instagram</p><div class="clearfix"> </div></a></li>
                </ul>
            </div>
            <div class="col-md-6 f_grid3">
                <h3>Contact Info</h3>
                <ul class="list">
                    <li>
                        <p>Phone : 1.800.254.5487</p>
                    </li>
                    <li>
                        <p>Fax : 1.800.254.2548</p>
                    </li>
                    <li>
                        <p>Email : <a href="mailto:info(at)fashionpress.com"> info(at)fashionpress.com</a></p>
                    </li>
                </ul>
                <ul class="list1">
                    <li>
                        <p>Aliquam augue a bibendum ipsum diam, semper porttitor libero elit egestas gravida, ut quam, nunc taciti</p>
                    </li>
                </ul>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
    <div class="footer_bottom">
        <div class="container">
            <div class="cssmenu">
                <ul>
                    <li class="active"><a href="login.html">隐私政策</a></li> .
                    <li><a href="checkout.html">服务条款</a></li> .
                    <li><a href="checkout.html">创意权利政策</a></li> .
                    <li><a href="login.html">联系我们</a></li> .
                    <li><a href="register.html">支持和常见问题</a></li>
                </ul>
            </div>
            <div class="copy">
                <p>Oxygen网络科技有限公司 版权所有 Copyright © 2016-2018 备案号：皖ICP备123456789</p>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>