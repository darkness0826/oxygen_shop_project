<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="header">
    <div class="header_top">
        <div class="container">
            <div class="logo">
                <a href="${ROOT_PATH}/index/index"><img src="${ROOT_PATH}/static/front/images/logo.png" alt="" /></a>
            </div>
            <ul class="shopping_grid">
                <c:set var="user" value="${sessionScope.login_user_info}"/>
                <c:if test="${user!=null}">
                    <li class="ii">
                        <img class="face" src="${ROOT_PATH}/static/front/images/face/<c:out value="${user.getFace()}"/>" alt="用户头像">
                    </li>
                    <li class="iii">
                        您好：<span style="color: indianred"><c:out value="${user.getName()}"/></span>
                        <ul class="message">
                            <a href="${ROOT_PATH}/user/personal"><li>个人中心</li></a>
                            <a href="${ROOT_PATH}/user/quit"><li>退出登陆</li></a>
                        </ul>
                    </li>
                    <script type="text/javascript">
                        var timer;
                        $(".iii,.ii").mouseover(function () {
                            clearTimeout(timer);
                            $(".message").fadeIn(200);
                        });
                        $(".iii,.ii").mouseleave(function () {
                            timer = setTimeout(function () {
                                $(".message").fadeOut(200);
                                console.log("我还在")
                            }, 00);
                        });

                    </script>
                </c:if>
                <c:if test="${user==null}">
                    <a href="${ROOT_PATH}/user/login">
                        <li>登录</li>
                    </a>
                    <a href="${ROOT_PATH}/user/register">
                        <li>注册</li>
                    </a>
                </c:if>
                <a href="${ROOT_PATH}/order/single">
                    <li><span class="m_1">Shopping Bag</span>&nbsp;&nbsp;(0) &nbsp;<img src="${ROOT_PATH}/static/front/images/bag.png" alt="" /></li>
                </a>
                <div class="clearfix"> </div>
            </ul>
            <div class="clearfix"> </div>
        </div>
    </div>
    <div class="h_menu4">
        <!-- start h_menu4 -->
        <div class="container">
            <a class="toggleMenu" href="#" style="display: none;">Menu</a>
            <ul class="nav">
                <li class=""><a href="index.html" data-hover="Home">Home</a></li>
                <li class="active"><a href="about.html" data-hover="About Us">About Us</a></li>
                <li><a href="careers.html" data-hover="Careers">Careers</a></li>
                <li><a href="contact.html" data-hover="Contact Us">Contact Us</a></li>
                <li><a href="404.html" data-hover="Company Profile">Company Profile</a></li>
                <li><a href="register.html" data-hover="Company Registration">Company Registration</a></li>
                <li><a href="wishlist.html" data-hover="Wish List">Wish List</a></li>
            </ul>
            <script type="text/javascript" src="js/nav.js"></script>
        </div>
        <!-- end h_menu4 -->
    </div>
</div>