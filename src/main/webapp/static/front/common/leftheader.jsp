<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<div class="uc-aside">
    <div class="uc-menu">
        <div class="tit">订单中心</div>
        <ul class="sublist">
            <li><a href="${ROOT_PATH}/user/order">我的订单</a></li>
            <li><a href="${ROOT_PATH}/user/fav">我的收藏</a></li>
            <li><a href="${ROOT_PATH}/user/footprint">浏览历史</a></li>
        </ul>
        <div class="tit">客户服务</div>
        <ul class="sublist">
            <li><a href="#">取消订单记录</a></li>
            <li><a href="${ROOT_PATH}/user/refund">退款/退货</a></li>
        </ul>
        <div class="tit">账户中心</div>
        <ul class="sublist">
            <li><a href="${ROOT_PATH}/user/personal">账户信息</a></li>
            <li><a href="${ROOT_PATH}/user/safe">账户安全</a></li>
            <li><a href="#">消费记录</a></li>
            <li><a href="${ROOT_PATH}/user/address">收货地址</a></li>
        </ul>
        <div class="tit">消息中心</div>
        <ul class="sublist">
            <li><a href="${ROOT_PATH}/user/evaluate">商品评价</a></li>
            <li><a href="${ROOT_PATH}/user/msg">站内消息</a></li>
        </ul>
    </div>
</div>