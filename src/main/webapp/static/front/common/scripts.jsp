
<script type="text/javascript" src="${ROOT_PATH}/static/front/js/easyResponsiveTabs.js"></script>
<script type="text/javascript" src="${ROOT_PATH}/static/front/js/hover_pack.js"></script>
<script type="text/javascript" src="${ROOT_PATH}/static/front/js/jquery.etalage.min.js"></script>
<script type="text/javascript" src="${ROOT_PATH}/static/front/js/nav.js"></script>
<script type="text/javascript" src="${ROOT_PATH}/static/front/js/responsiveslides.min.js"></script>
<script type="text/javascript" src="${ROOT_PATH}/static/front/js/stat.js"></script>
<link rel="stylesheet" href="${ROOT_PATH}/static/front/js/icheck/style.css"/>
<script src="${ROOT_PATH}/static/front/js/icheck/icheck.min.js"></script>
<script src="${ROOT_PATH}/static/front/js/laydate/laydate.js"></script>
<script src="${ROOT_PATH}/static/front/js/layer/layer.js"></script>
<script src="${ROOT_PATH}/static/front/js/global.js"></script>

<script type="application/x-javascript">
    addEventListener("load", function() {
        setTimeout(hideURLbar, 0);
    }, false);

    function hideURLbar() {
        window.scrollTo(0, 1);
    }
</script>
<script type="text/javascript">
    jQuery(document).ready(function($) {

        $('#etalage').etalage({
            thumb_image_width: 300,
            thumb_image_height: 400,
            source_image_width: 900,
            source_image_height: 1200,
            show_hint: true,
            click_callback: function(image_anchor, instance_id) {
                alert('Callback example:\nYou clicked on an image with the anchor: "' + image_anchor + '"\n(in Etalage instance: "' + instance_id + '")');
            }
        });

    });
    $(document).ready(function() {
        $('#horizontalTab').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion           
            width: 'auto', //auto or any width like 600px
            fit: true // 100% fit in a container
        });
    });
    // 左侧导航栏

    $(function() {
        var menu_ul = $('.menu > li > ul'),
            menu_a  = $('.menu > li > a');
        menu_ul.hide();
        menu_a.click(function(e) {
            e.preventDefault();
            if(!$(this).hasClass('active')) {
                menu_a.removeClass('active');
                menu_ul.filter(':visible').slideUp('normal');
                $(this).addClass('active').next().stop(true,true).slideDown('normal');
            } else {
                $(this).removeClass('active');
                $(this).next().stop(true,true).slideUp('normal');
            }
        });

    });
    //搜索
    $(".search").eq(1).click(function () {
        var name = $("#search").val();
        if (name == null || name == "") {
            return;
        } else {
            window.location.href = "${ROOT_PATH}"+"/commodity/search/"+name;
        }


    });
</script>
</script>